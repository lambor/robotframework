*** Variables ***
${icon-sync-center}    //*[@id='sync']
${btn-save-config}    //*[@id='btn-save-config']
${btn-save-config-disable}    //*[@id='btn-save-config'and contains(@class,'disabled')]
${lst-branch-inventory-sync}    //*[@id='inventory-sync']//*
${ord}            ${EMPTY}
${desc-kv}        //*[@id='opt-des-kv']
${btn-syn-manual}    //*[@id='tooltip-btn-sync']
${branch-orders-sync}    //*[@id='inventory-sync']
${ord}            ${EMPTY}
${desc-kv}        //*[@id='opt-des-kv']
${surchages_checkbox}    //*[@id='checkbox-1']
${all-branch}     //*[@id='checkbox-2']
${message-error}    //*[@id='message-error']
${toastmessage-suscess}    //*[@class='toasted-container top-right']//*[contains(@class,'success')]
${surchage-sync}    //*[@id='surcharge-sync']//input
${surchage-listbox}    //*[contains(@id,'__listbox') and not(@style)]
${surchage-option}    //*[contains(@id,'__listbox') and not(@style)]//*[contains(@id,'__option')]
# ${surchage-option}    //*[@id='vs5__option-0']
${branch-order-sync-input}    //*[@id='inventory-sync']//input
${lst-branch-inventory-sync}    //*[@id='inventory-sync']
${cb-branch-order-sync}    //*[@id='branch-order-syn']
${cb-price-sync}    //*[@id='price-sync']
${cb-price-sync-selected}    //*[@id='price-sync']/..//span//span[@role='textbox']
${cb-promotion-sync}    //*[@id='promotion-sync']
${cb-promotion-sync-selected}    //*[@id='promotion-sync']/..//span//span[@role='textbox']
${tab-sync-product}    //*[@class='tab-pane active']//*[@id='sync-config']
${search-product}    //*[@id='mk-search-sync']
${default-category}    //*[@id='category-list-sync']//*[contains(text(),'Category Auto Test')]
${product-sync}    //*[@id='product-item-sync']//*[contains(text(),'Sản phẩm auto test')]
${search-category}    //*[@id='mk-search-category']
${list-product-sync}    //*[@id='product-item-sync']
${list-category-sync}    //*[@id='category-list-sync']
${dropdown-trademark}    //*[@id='mk-dropdown-brand__BV_toggle_']
${list-product-sync}    //*[@id='product-item-sync']
${list-category-sync}    //*[@id='category-list-sync']
${trademark-dropdown-item}    //a[contains(text(),'{0}')]
${trademark-filter-result}    //tbody//tr[1]//td[@class='cell-brand']
${dropdown-sync-status}    //*[@id="mk-dropdown-status__BV_toggle_"]
${dropdown-sync-status-synced}    //*[@id='mk-dropdown-status']//ul//li//a[contains(text(),'Đã đồng bộ')]
${dropdown-sync-status-not-synced}    //*[@id='mk-dropdown-status']//ul//li//a[contains(text(),'Chưa đồng bộ')]
${product-sync-status-synced}    //*[@id="product-item-sync"]//*[*[contains(text(), 'Sản phẩm auto test 2 - không xóa')]]//input
${product-sync-status-not-synced}    //*[@id="product-item-sync"]//*[*[contains(text(), 'Sản phẩm auto test - không xóa')]]//input
${category-auto-test}    //*[@id='mk-category-item-4']//*[@class='mk-custom-checkbox category-checkbox custom-control custom-checkbox checked-false']
