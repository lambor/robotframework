*** Variables ***
#Màn hình Template
${mytemplate_btn_delete_unpublished_tmpl}    //*[@id='my-templates']//*[@class='mk-template-content' and not(strong)]/parent::*//*[contains(@id,'delete')]
${mytemplate_unpublished_template}    //*[@id='my-templates']//*[@class='mk-template-content' and not(strong)]/parent::*
${mytemplate_btn_cofirm_delete}    //*[@id='modal-confirm-delete___BV_modal_content_']//*[contains(text(),'Đồng ý')]
${mytemplate_txt_undeleteable}    //*[@id='modal-custom-template___BV_modal_body_']//*[contains(text(),'Bạn không thể xóa giao diện đang tùy biến')]
${mytemplate_btn_open_unpublished_tmpl}    //*[@id='my-templates']//*[@class='mk-template-content' and not(strong)]/parent::*//*[contains(@id,'open')]
${mytemplate_unpublished_template_name}    //*[@id='my-templates']//*[@class='mk-template-content' and not(strong)]/parent::*//*[@class='mk-template-name']
${mytemplate_btn_open_published_tmpl}    //*[@id='my-templates']//*[@class='mk-template-content' and strong]/parent::*//*[contains(@id,'open')]
${mytemplate_published_template_name}    //*[@id='my-templates']//*[@class='mk-template-content' and strong]/parent::*//*[@class='mk-template-name']
