*** Variables ***
${store_template}    //*[@id='store-templates']
${theme_template}    //*[@class='mk-template-thumb aspect-ratio-16-9']//*[@class='mk-template-ctas']
${apply_theme}    //*[@id='apply-theme']
${modal_confirm_apply}    //*[@id='modal-confirm-apply___BV_modal_content_']
${modal_confirm_apply_btn_Cancel}    //*[@id='modal-confirm-apply___BV_modal_content_']//button
${modal_confirm_apply_btn_Accept}    (//*[@id='modal-confirm-apply___BV_modal_content_']//button)[2]
${surchage_checkbox}    //*[@id="checkbox-1"]
