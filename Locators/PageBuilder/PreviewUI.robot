*** Variables ***
${frame_preview}    //iframe[@title='preview-template']
${link_home}      //img[@alt='Logo']
${btn_exit_preview}    //div[@id='mk-topbar-edit']//button[contains(@class,'mk-btn mk-btn-secondary')]
${txt_name_preview}    //div[@class='mk-name mb0 fw-bold']
# hàng hoá
${link_hanghoa}    //*[@class='mk-grid']//*[contains(@class,'mk-product-main')]//a
${btn_muangay}    //div[@class='mk-content-page']//button[@class='btn btn-secondary']
${btn_mua}        //button[@class='btn btn-secondary']
${btn_quickcart}    //div[@class='preview-category-section'][1]//div[@class='mk-product-item'][1]//button[@class='btn-cart hover-cart-btn']
${btn_closequickcart}    //button[@aria-label='Close']
${tilte_checkout}    //h4[contains(text(),'Đặt hàng')]
${btn_dathang}    //div[@class='card card-toggle']//button
${link_danhmuchh}    //div[@class='preview-category-section'][1]//a[@class='preview-category-more']
${lbl_name_category}    //div[@class='preview-category-section'][1]//h3[@class='preview-category-name']
${lbl_product_code}    //div[@class='product-info']//div[@class='product-sku']
# bài viết
${lbl_tenbaiviet}    //div[@class='mk-Blog-list']//div[contains(@class,'blog-item inline')][1]//h5
${txt_tenbaiviet}    ${EMPTY}
${lbl_danhmucbaiviet}    //div[@class='mk-Blog-list']//div[contains(@class,'blog-item inline')][1]//div[2]//a
${lbl_title_category_blog}    //div[@class='mk-content-page']//h5[@class='blog-title']
