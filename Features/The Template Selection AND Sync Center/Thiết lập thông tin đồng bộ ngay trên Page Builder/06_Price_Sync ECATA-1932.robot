*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot

*** Test cases ***
Kiểm tra hoạt động của thiết lập "Chọn bảng giá dùng làm giá bán"
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    When Thấy thiết lập "Chọn bảng giá dùng làm giá bán" có chọn "Bảng giá chung"
    And Thấy nút "Lưu" ở trạng thái không hoạt động
    And Cập nhật bảng giá bán từ "Bảng giá chung" sang "Bảng giá lẻ"
    And Thấy cập nhật thiết lập giá bán thành bảng giá "Bảng giá lẻ" thành công

Kiểm tra hoạt động của thiết lập "Chọn bảng giá dùng làm giá khuyến mại"
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    When Thấy thiết lập "Chọn bảng giá dùng làm giá khuyến mại" không chọn giá trị nào
    And Thấy nút "Lưu" ở trạng thái không hoạt động
    And Cập nhật bảng giá khuyến mại là Bảng giá "Tri ân khách hàng"
    And Thấy cập nhật thiết lập giá khuyến mại thành bảng giá "Tri ân khách hàng" thành công
