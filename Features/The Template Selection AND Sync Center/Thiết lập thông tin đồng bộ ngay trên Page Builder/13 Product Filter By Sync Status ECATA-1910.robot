*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot

*** Test Cases ***
Kiểm tra khả năng lọc sản phẩm "Đã đồng bộ"
    [Tags]    all
    Thiết lập đồng bộ bật/tắt cho Category Auto Test    1
    Given gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    And Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    And Thấy công cụ lọc "Trạng thái đồng bộ" trong "Danh sách sản phẩm"
    When Bấm vào công cụ lọc "Trạng thái đồng bộ" chọn giá trị "Đã đồng bộ"
    Then Thấy màn hình hiển thị sản phẩm "X" có trạng thái "Đã đồng bộ"

Kiểm tra lọc sản phẩm "Chưa đồng bộ"
    [Tags]    all
    Thiết lập đồng bộ OFF cho sản phẩm test
    Given gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    And Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    And Thấy công cụ lọc "Trạng thái đồng bộ" trong "Danh sách sản phẩm"
    When Bấm vào công cụ lọc "Trạng thái đồng bộ" chọn giá trị "Chưa đồng bộ"
    Then Thấy màn hình hiển thị sản phẩm "Y" có trạng thái "Chưa đồng bộ"
