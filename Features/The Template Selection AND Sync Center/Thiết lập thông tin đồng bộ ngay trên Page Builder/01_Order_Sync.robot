*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot

*** Test cases ***
Kiểm tra hoạt động của thiết lập "Chọn chi nhánh đồng bộ đơn hàng"
    [Tags]    all
    Given gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    When Thấy thiết lập "Chọn chi nhánh đồng bộ đơn hàng" có chọn "Chi nhánh trung tâm"
    And Thấy nút "Lưu" ở trạng thái không hoạt động
    And Bấm vào thiết lập để chọn "Chi nhánh Yết Kiêu" thay cho "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ đơn đặt hàng"
    Then Thấy nút "Lưu" ở trạng thái hoạt động
    And Bấm được vào nút "Lưu"
    And Thấy thông báo "Lưu thành công"
    And Thấy lưu thiết lập thành công
