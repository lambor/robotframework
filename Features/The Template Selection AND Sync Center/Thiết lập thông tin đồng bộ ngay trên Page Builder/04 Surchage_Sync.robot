*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot

*** Test Cases ***
Kiểm tra hoạt động của thiết lập "Chọn loại Thu khác"
    [Tags]    all    regression
    Bỏ chọn Thu khác
    Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    When Thấy thiết lập "Chọn loại Thu khác" không có loại Thu khác nào được chọn
    And Thấy nút "Lưu" ở trạng thái không hoạt động
    And Bấm vào thiết lập để chọn giá trị Thu khác
    Then Thấy nút "Lưu" ở trạng thái hoạt động
    And Bấm được vào nút "Lưu"
    And Thấy thông báo "Lưu thành công"

Kiểm tra hoạt động của nút "Chọn tất cả" trong thiết lập "Chọn loại Thu khác"/Chọn
    [Tags]    all
    Given Chọn tất cả Thu khác
    Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Then Thấy hiển thị tất cả các loại Thu khác trong danh sách thiết lập "Chọn loại Thu khác"

Kiểm tra hoạt động của nút "Chọn tất cả" trong thiết lập "Chọn loại Thu khác"/Bỏ chọn
    [Tags]    all
    Given Bỏ chọn Thu khác
    Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Then Thấy thiết lập "Chọn loại Thu khác" không có loại Thu khác nào được chọn
