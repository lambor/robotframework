*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../../Actions/StoreFront/DetailProductPage.robot

*** Test Cases ***
Kiểm tra khả năng tìm kiếm theo tên sản phẩm
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    And Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    And Thấy thanh tìm kiếm trong "Danh sách sản phẩm"
    When Bấm vào thanh tìm kiếm nhập giá trị "sản phẩm auto"
    Then Thấy màn hình danh sách sản phẩm hiển thị sản phẩm "sản phẩm auto test"
