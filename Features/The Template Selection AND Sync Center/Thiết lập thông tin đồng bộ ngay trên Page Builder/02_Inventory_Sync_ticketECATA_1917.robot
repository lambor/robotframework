*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot

*** Test cases ***
Kiểm tra lưu thiết lập "Chọn chi nhánh đồng bộ tồn kho" thành công
    [Tags]    all    regression
    Lưu thiết lập "Chi nhánh trung tâm" thành công
    When Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    And Thấy "Chi nhánh trung tâm" đã được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"

Kiểm tra lưu thiết lập "Chọn chi nhánh đồng bộ tồn kho" không thành công
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    When Thấy "Chi nhánh trung tâm" đã được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    And Bỏ chọn "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    And Thấy cảnh báo "Bạn chưa chọn chi nhánh"
    And Thấy nút Lưu ở trạng thái không hoạt động

Kiểm tra hoạt động của nút "Chọn tất cả" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho" (Trường hợp chọn)
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    And Thấy "Chi nhánh trung tâm" đã được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    And Thấy nút "Chọn tất cả" ở trạng thái chưa chọn
    When Bấm nút "Chọn tất cả" ở thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Then Thấy hiển thị tất cả các chi nhánh trong danh sách thiết lập "Chọn chi nhánh đồng bộ tồn kho"

Kiểm tra hoạt động của nút "Chọn tất cả" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho" (Trường hợp bỏ chọn)
    [Tags]    all
    Lưu thiết lập "Chọn tất cả chi nhánh" thành công
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    And Thấy nút "Chọn tất cả" ở trạng thái đã chọn
    When Bấm nút "Chọn tất cả" ở thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Then Thấy cảnh báo "Bạn chưa chọn chi nhánh"
    And Thấy nút Lưu ở trạng thái không hoạt động
