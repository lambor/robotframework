*** Settings ***
Suite Setup       Before Test
Suite Teardown    After each test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/StoreFront/DetailProductPage.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-KV/api_hanghoa_kv.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../Actions/PageBuilder/ProductSetting.robot

*** Test cases ***
Đồng bộ tồn kho khi giá trị tồn kho hết hàng
    [Tags]    all    regression
    # Test 1237
    #    Get BearerToken from Public API
    #    Cập nhật tồn kho và giá sản phẩm thông qua Public API thành    0    66000
    # Kiểm tra hoạt động của thiết lập "Chọn chi nhánh đồng bộ đơn hàng"
    #    Given Thiết lập để chọn "Chi nhánh Yết Kiêu" thay cho "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    #    When Gian hàng "Testautomykiot" đăng nhập thành công
    #    And Người dùng truy cập màn hình "Page Builder"
    #    And Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    #    Then "Chi nhánh Yết Kiêu" được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Thiết lập đồng bộ bật/tắt cho Category Auto Test    1
    Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    Given Run Keyword If    '${env}' != 'live'    Cập nhật tồn kho và giá cho sản phẩm test thành    0    50000
    Given Run Keyword If    '${env}' == 'live'    Cập nhật tồn kho và giá sản phẩm thông qua Public API thành    0    50000
    When Bấm đồng bộ thủ công
    Then Sản phẩm test hiển thị hết hàng

Đồng bộ tồn kho khi giá trị tồn kho còn hàng
    [Tags]    all    regression
    Thiết lập đồng bộ bật/tắt cho Category Auto Test    1
    Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    Given Run Keyword If    '${env}' == 'live'    Cập nhật tồn kho và giá sản phẩm thông qua Public API thành    5    50000
    Given Run Keyword If    '${env}' != 'live'    Cập nhật tồn kho và giá cho sản phẩm test thành    5    50000
    When Bấm đồng bộ thủ công
    Then Sản phẩm test hiển thị còn hàng
    # Kiểm tra thiết lập đồng bộ
    #    [Template]    Cập nhật thiết lập đồng bộ
    # 11749    12041    0    91575    7461    Mykiot
    # Kiểm tra thiết lập đồng bộ
    #    [Template]    Cập nhật thiết lập đồng bộ
    #    ${branch_id1}    ${branch_id1}    ${sale_price_id}    ${promotion_price_id}    ${surchage_id1}    kiotviet

Kiểm tra đồng bộ khi thêm mới và xóa sản phẩm
    [Tags]    all    regression    hotfix
    [Setup]    Tạo mã hàng hóa
    Given Thêm mới sản phẩm Hoa hồng nhung ở KiotViet
    When Bấm đồng bộ thủ công
    Then Sản phẩm Hoa hồng nhung đã đồng bộ sang mykiot
    And Xóa sản phẩm Hoa hồng nhung ở KiotViet
    When Bấm đồng bộ thủ công
    Then Sản phẩm Hoa hồng nhung không còn hiển thị ở Mykiot
    # Get pricebook from api configs
    #    Get information pricebook from api configs with $.priceBook    #    # Get branch order from api configs    #
    # ...    # Get information branch order id from api configs with $.branch.branchOrder    #    # Lấy thông tin mô tả    #    Get information description from api configs with $.generalSetting.description    #
    # ...    # Test update price    #    Cập nhật tồn kho và giá cho sản phẩm test thành    5    60000    # So sánh tổng sản phẩm ở màn hình Thiết lập đồng bộ sản phẩm của Mykiot với màn hình Danh mục Hàng hóa Kiotviet
    # ...    #    Bấm đồng bộ thủ công    #    ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}
    # ...    # ${EMPTY}    ${EMPTY}    ${EMPTY}    # ${EMPTY}
    #    ${mk_total_product}    Lấy tổng sản phẩm từ KiotViet
    #    Should Be Equal As Numbers    ${myk_total_product}    ${mk_total_product}
    [Teardown]    after test

ECATA-1913-Lọc sản phẩm đồng bộ theo mã sản phẩm
    [Tags]    all    regression    hotfix
    ${productID}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${product_code}    ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    # ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    SP001618    ${EMPTY}    ${EMPTY}
    Should Contain    ${productID}    ${product_code}

ECATA-1912-Lọc sản phẩm đồng bộ theo tên sản phẩm
    [Tags]    all    regression
    ${productID}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    Sản phẩm auto test - không xóa    ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    # ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    SP001618    ${EMPTY}    ${EMPTY}
    Should Contain    ${productID}    ${product_code}

Lọc sản phẩm đồng bộ theo danh mục
    [Tags]    all    regression
    ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}    ${cateIDtestKV}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Should Be Equal As Numbers    ${myk_total_product}    2    # Lọc sản phẩm theo thương hiệu trong Category Auto Test    #    ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    # ${EMPTY}    ${cateIDtestKV}    Hảo hảo    ${EMPTY}    # ${EMPTY}
    #    Should Be Equal As Numbers    ${myk_total_product}    1

ECATA-1910-Lọc sản phẩm theo Đồng bộ TẮT trong Category Auto Test
    [Tags]    all    regression
    Thiết lập đồng bộ bật/tắt cho Category Auto Test    0
    Thiết lập đồng bộ bật/tắt cho Category Auto Test    0
    ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}    ${cateIDtestKV}    ${EMPTY}    ${EMPTY}    0
    Should Be Equal As Numbers    ${myk_total_product}    2

ECATA-1910-Lọc sản phẩm theo Đồng bộ BẬT trong Category Auto Test
    [Tags]    all    regression
    ${myk_total_product}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}    ${cateIDtestKV}    ${EMPTY}    ${EMPTY}    1
    Should Be Equal As Numbers    ${myk_total_product}    0

ECATA-1909-Lọc sản phẩm theo CÓ hình ảnh trong Category Auto Test
    [Tags]    all    regression
    ${productID}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}    ${cateIDtestKV}    ${EMPTY}    1    ${EMPTY}
    Should Not Contain    ${productID}    ${product_code}

ECATA-1909-Lọc sản phẩm theo KHÔNG hình ảnh trong Category Auto Test
    [Tags]    all    regression
    ${productID}    Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot    ${EMPTY}    ${cateIDtestKV}    ${EMPTY}    0    ${EMPTY}
    Should Contain    ${productID}    ${product_code}
    # test
    #    [Template]
    #    Tạo store
