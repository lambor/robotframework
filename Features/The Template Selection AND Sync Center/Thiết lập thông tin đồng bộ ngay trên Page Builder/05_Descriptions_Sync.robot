*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot

*** Test Cases ***
Cập nhật thiết lập đồng bộ
    [Tags]    all
    Cập nhật thiết lập đồng bộ    ${branch_id1}    0    0    91575    ${surchage_id1}    mykiot
    Kiểm tra cấu hình đồng bộ chứa: "generalSetting":{"description":"mykiot"}
    # Log    ${description}
    # Should Be Equal As Strings    ${description}    mykiot
