*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_menu_pb.robot
Resource          ../../../Actions/PageBuilder/Menu.robot

*** Test Cases ***
Kiểm tra khả năng lọc sản phẩm theo tiêu chí thương hiệu
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    And Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    #And Thấy công cụ lọc "Thương hiệu" trong "Danh sách sản phẩm"
    When Bấm vào công cụ lọc "Thương hiệu" chọn giá trị "Hảo hảo"
    Then Thấy màn hình hiển thị sản phẩm "X" có thương hiệu "Hảo hảo"

Kiểm tra khả năng lọc sản phẩm theo tiêu chí nhiều thương hiệu cùng lúc
    [Tags]    all
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    And Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    #And Thấy công cụ lọc "Thương hiệu" trong "Danh sách sản phẩm"
    When Bấm vào công cụ lọc "Thương hiệu" chọn giá trị "Hảo hảo"
    Then Thấy màn hình hiển thị sản phẩm "X" có thương hiệu "Hảo hảo"
    And Chọn tiếp lọc theo thương hiệu "Vifon"
    And Thấy màn hình hiển thị sản phẩm "Y" có thương hiệu "Vifon"
