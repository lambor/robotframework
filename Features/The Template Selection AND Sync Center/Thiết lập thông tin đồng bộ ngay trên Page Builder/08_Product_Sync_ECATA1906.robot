*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../../Actions/StoreFront/DetailProductPage.robot

*** Test cases ***
Kiểm tra hoạt động của thao tác bật đồng bộ từng sản phẩm
    [Tags]    all    regression    hotfix
    Given Thiết lập đồng bộ ON cho sản phẩm test
    And Trạng thái Đồng bộ của sản phẩm test ON
    And Cập nhật tồn kho và giá của sản phẩm test
    And Bấm đồng bộ thủ công
    When Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    Then Sản phẩm test hiển thị giá trên PageBuilder chính xác
    And Sản phẩm test hiển thị đúng giá bán ngoài StoreFront

Kiểm tra hoạt động của thao tác tắt đồng bộ từng sản phẩm
    [Tags]    all    regression    hotfix
    Given Thiết lập đồng bộ OFF cho sản phẩm test
    And Trạng thái Đồng bộ của sản phẩm test OFF
    And Cập nhật tồn kho và giá của sản phẩm test
    And Bấm đồng bộ thủ công
    When Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    Then Sản phẩm test hiển thị giá trên PageBuilder chính xác
    And Sản phẩm test không hiển thị ngoài StoreFront

Kiểm tra hoạt động của thao tác TẮT đồng bộ sản phẩm bằng thao tác nhanh
    [Tags]    all    regression
    Given Thiết lập đồng bộ bật/tắt cho Category Auto Test    0
    And Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    Then Trạng thái Đồng bộ của sản phẩm trong Category Auto test ON/OFF    0

Kiểm tra hoạt động của thao tác BẬT đồng bộ sản phẩm bằng thao tác nhanh
    [Tags]    all    regression
    Given Thiết lập đồng bộ bật/tắt cho Category Auto Test    1
    And Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    Then Trạng thái Đồng bộ của sản phẩm trong Category Auto test ON/OFF    1
    # And Cập nhật tồn kho và giá cho sản phẩm test thành    5    55000
    And Run Keyword If    '${env}' != 'live'    Cập nhật tồn kho và giá cho sản phẩm test thành    5    55000
    And Run Keyword If    '${env}' == 'live'    Cập nhật tồn kho và giá sản phẩm thông qua Public API thành    5    55000
    And Bấm đồng bộ thủ công
    Sản phẩm test hiển thị thông tin trên StoreFront
