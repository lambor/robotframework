*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Config/envi.robot
Resource          ../../../Actions/PageBuilder/Mytemplate.robot
Resource          ../../../Actions/PageBuilder/SidebarMenu.robot
Resource          ../../../Actions/PageBuilder/StoreTemplate.robot
Resource          ../../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../../Actions/PageBuilder/PageBuilder.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Locators/CMS.robot
Resource          ../../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../../Locators/PageBuilder/StoreTemplate.robot
Resource          ../../../Locators/PageBuilder/TopbarMenu.robot
Resource          ../../../Locators/PageBuilder/PageBuilder.robot
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/API/API-MYK/api_theme.robot

*** Test Cases ***
Kiểm tra áp dụng giao diện mới thành công khi không chỉnh sửa giao diện có sẵn
    [Tags]    all    regression
    # Đang mở giao diện B
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Người dùng không chỉnh sửa giao diện "A" trên màn hình "Page Builder"
    When Người dùng truy cập màn hình Templates
    And Hệ thống hiển thị danh sách giao diện có sẵn trong trang "Store Templates"
    And Áp dụng giao diện eMobile trong danh sách Store Templates
    Then Người dùng thay giao diện eMobile hiển thị thành công trên màn hình "Page Builder"
    # Giao diện hiển thị thành công, đang dựa vào ảnh logo eMobile

Kiểm tra áp dụng giao diện mới thành công khi đang chỉnh sửa giao diện có sẵn
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Người dùng thay đổi phông chữ của giao diện thành Lora
    # Người dùng thay đổi phông chữ của giao diện B và lưu thành công
    # And Người dùng đang chỉnh sửa giao diện "A" trên màn hình "Page Builder"
    And Bấm nút Lưu trên thanh công cụ TopbarMenu
    And Bấm nút Tiếp tục sau khi Lưu thành công
    # And Người dùng lưu giao diện "A" vào màn hình "My Templates" thành công
    When Người dùng truy cập màn hình Templates
    And Hệ thống hiển thị danh sách giao diện có sẵn trong trang "Store Templates"
    And Áp dụng giao diện Miss.A trong danh sách Store Templates
    Then Người dùng thay giao diện missA hiển thị thành công trên màn hình "Page Builder"
    # And Giao diện B lưu thành công phông chữ Lora trong config

Kiểm tra áp dụng giao diện mới thành công khi đang chỉnh sửa giao diện có sẵn nhưng không lưu
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Người dùng thay đổi phông chữ của giao diện thành Montserrat
    # And Người dùng đang chỉnh sửa giao diện "A" trên màn hình "Page Builder"
    And Người dùng không bấm vào nút "Lưu" giao diện đang mở vào màn hình "My Templates"
    When Người dùng truy cập màn hình Templates
    And Hệ thống hiển thị danh sách giao diện có sẵn trong trang "Store Templates"
    And Áp dụng giao diện Miss.A trong danh sách Store Templates
    Then Hệ thống hiển thị yêu cầu xác nhận có lưu những thay đổi không
    And Bấm Hủy Bỏ lưu thay đổi
    And Người dùng thay giao diện missA hiển thị thành công trên màn hình "Page Builder"

Kiểm tra áp dụng giao diện mới thành công khi đang chỉnh sửa giao diện có sẵn nhưng không lưu đè
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    And Người dùng truy cập màn hình "Page Builder"
    And Người dùng thay đổi phông chữ của giao diện thành Anton
    # And Người dùng đang chỉnh sửa giao diện "A" trên màn hình "Page Builder"
    And Người dùng không bấm vào nút "Lưu đè" giao diện đang mở vào màn hình "My Templates"
    When Người dùng truy cập màn hình Templates
    And Hệ thống hiển thị danh sách giao diện có sẵn trong trang "Store Templates"
    And Áp dụng giao diện eMobile trong danh sách Store Templates
    Then Hệ thống hiển thị yêu cầu xác nhận có lưu những thay đổi không
    And Bấm Đồng Ý lưu thay đổi
    And Người dùng thay giao diện eMobile hiển thị thành công trên màn hình "Page Builder"
