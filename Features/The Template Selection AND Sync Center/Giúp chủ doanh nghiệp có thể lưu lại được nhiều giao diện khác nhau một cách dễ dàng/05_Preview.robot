*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Config/envi.robot
Resource          ../../../Actions/PageBuilder/Mytemplate.robot
Resource          ../../../Actions/PageBuilder/SidebarMenu.robot
Resource          ../../../Actions/PageBuilder/StoreTemplate.robot
Resource          ../../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../../Actions/PageBuilder/Preview.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/Common.robot
Resource          ../../../Locators/StoreFront/HomePage.robot

*** Test Cases ***
Kiểm tra áp dụng giao diện mới thành công khi không chỉnh sửa giao diện có sẵn
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    #And Người dùng truy cập màn hình "Page Builder"
    Then Người dùng thấy nút "Preview" giao diện "A" trên thanh công cụ của màn hình "Page Builder"
    When Người dùng bấm vào nút "Preview"
    Then Hệ thống mở ra màn hình hiển thị giao diện "A" như ở "StoreFront" ngay tại màn hình "Page Builder"
    And Người dùng tương tác ở màn hình xem trước của giao diện "A" giống như trải nghiệm ở "StoreFront"

Kiểm tra trạng thái hoạt động của nút "Đặt hàng" trong trang Check-out
    [Tags]    all
    Given Người dùng đang ở "trang chi tiết sản phẩm A" của màn hình xem trước giao diện "A"
    When Người dùng bấm vào nút "Mua ngay" ở "trang chi tiết sản phẩm A" của màn hình xem trước giao diện "A"
    Then Người dùng thấy màn hình xem trước mở ra trang "Check out"
    And Thấy nút "Đặt hàng" ở cuối trang "Check out" ở trạng thái không thể bấm được

Kiểm tra khả năng mở lại trang vừa xem trước khi nhấn Refressh (F5)
    [Tags]    all
    Given Người dùng đang ở "trang chi tiết sản phẩm A" của màn hình xem trước giao diện "A"
    When Người dùng nhấn tải lại trang F5 (Refressh)
    Then Người dùng thấy màn hình xem trước mở lại trang "Home" sau khi tải trang

Kiểm tra khả năng quay trở lại được màn "Edit Template" khi đang Xem trước
    [Tags]    all
    Given Người dùng đang chỉnh sửa giao diện "A" ở màn hình "Page Builder"
    When Người dùng bấm vào nút "Preview" để mở trang xem trước giao diện "A"
    Then Người dùng thấy nút thoát khỏi màn hình xem trước giao diện "A" hiển thị
    When Người dùng nhấn nút thoát khỏi màn hình xem trước giao diện "A" và tiếp tục chỉnh sửa giao diện "A" ở màn hình "Page Builder"
