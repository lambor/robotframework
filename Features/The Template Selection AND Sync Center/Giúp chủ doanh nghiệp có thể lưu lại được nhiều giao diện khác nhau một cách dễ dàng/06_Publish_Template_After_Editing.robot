*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../../Actions/PageBuilder/SidebarMenu.robot
Resource          ../../../Actions/PageBuilder/Mytemplate.robot
Resource          ../../../Actions/PageBuilder/StoreTemplate.robot
Resource          ../../../Actions/API/API-MYK/api_theme.robot

*** Test cases ***
Kiểm tra phát hành thành công giao diện hiện có trên màn hình Page Builder nhưng không chỉnh sửa
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    #And Người dùng truy cập màn hình "Page Builder"
    And Người dùng truy cập màn hình Templates
    And Mở giao diện B trong màn hình "My Templates"
    And Người dùng không chỉnh sửa các thiết lập trong giao diện đang mở
    When Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Then Người dùng phát hành giao diện thành công
    And Hệ thống hiển thị thông báo "Phát hành thành công"
    And So sánh config của giao diện đang phát hành và B trong My Templates giống nhau

Kiểm tra phát hành không thành công khi áp dụng giao diện mới lên giao diện hiện có nhưng không lưu
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    #And Người dùng truy cập màn hình "Page Builder"
    And Người dùng truy cập màn hình Templates
    And Áp dụng giao diện eMobile trong danh sách Store Templates
    And Người dùng không bấm vào nút "Save as" để lưu giao diện vào màn hình "My Templates"
    When Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Then Người dùng phát hành giao diện không thành công
    # And Hệ thống hiển thị thông báo "Bạn cần lưu lại những tùy biến vừa thiết lập trên giao diện hiện tại trước khi “Phát hành”"

Kiểm tra phát hành thành công khi áp dụng giao diện mới lên giao diện hiện có nhưng không chỉnh sửa
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    #And Người dùng truy cập màn hình "Page Builder"
    And Người dùng truy cập màn hình Templates
    And Áp dụng giao diện eMobile trong danh sách Store Templates
    And Người dùng bấm vào nút "Save As" trên thanh công cụ tại màn hình "Page Builder"
    And Người dùng thay template E cho template D và lưu lại
    And Giao diện lưu đè thành công
    When Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Then Người dùng phát hành giao diện thành công
    And Hệ thống hiển thị thông báo "Phát hành thành công"

Kiểm tra phát hành không thành công khi áp dụng giao diện đã lưu lên giao diện hiện có nhưng có chỉnh sửa
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    #And Người dùng truy cập màn hình "Page Builder"
    And Người dùng truy cập màn hình Templates
    And Mở giao diện Giao diện mặc định trong màn hình "My Templates"
    And Người dùng thay đổi phông chữ của giao diện thành Gotu
    # Người dùng đang chỉnh sửa giao diện trên màn hình "Page Builder"
    When Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Then Người dùng phát hành giao diện không thành công
    # And Hệ thống hiển thị thông báo "Bạn cần lưu lại những tùy biến vừa thiết lập trên giao diện hiện tại trước khi “Phát hành”"
