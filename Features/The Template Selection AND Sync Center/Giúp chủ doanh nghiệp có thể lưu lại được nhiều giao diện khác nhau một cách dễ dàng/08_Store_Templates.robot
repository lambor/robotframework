*** Settings ***
Test Setup        Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../../Actions/Common.robot
Resource          ../../../Actions/Share/utils.robot
Resource          ../../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../../Actions/StoreFront/HomePage.robot
Resource          ../../../Actions/PageBuilder/SidebarMenu.robot
Resource          ../../../Actions/PageBuilder/Mytemplate.robot
Resource          ../../../Actions/PageBuilder/StoreTemplate.robot
Resource          ../../../Actions/API/API-MYK/api_theme.robot
Resource          ../../../Actions/Share/Computation.robot
Resource          ../../../Locators/StoreFront/HomePage.robot

*** Test cases ***
Vegan theme
    [Tags]    all    theme
    Tạo theme mới theo config: vegan_config.json
    Kiểm tra theme Vegan xuất hiện trong My Templates
    Mở giao diện Vegan trong màn hình "My Templates"
    Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Hệ thống hiển thị thông báo "Phát hành thành công"
    Chụp ảnh ${header} với tên vegan-header-actual.jpg
    Chụp ảnh ${main-banner} với tên vegan-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên vegan-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên vegan-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên vegan-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên vegan-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên vegan-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên vegan-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên vegan-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên vegan-content-testimonial-actual.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên vegan-title-blog-expected.jpg
    # Chụp ảnh ${section-blog-content} với tên vegan-content-blog-expected.jpg
    Chụp ảnh ${footer} với tên vegan-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên vegan-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên vegan-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên vegan-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên vegan-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên vegan-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên vegan-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên vegan-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên vegan-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên vegan-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên vegan-content-testimonial-actual-mobile.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên vegan-title-blog-expected.jpg
    # Chụp ảnh ${section-blog-content} với tên vegan-content-blog-expected.jpg
    Chụp ảnh ${footer} với tên vegan-footer-expected-mobile.jpg
    Close Browser
    Người dùng đang ở màn hình Templates
    Mở giao diện Giao diện mặc định trong màn hình "My Templates"
    Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Sleep    5s
    Bấm nút "Tiếp tục" trên modal phát hành thành công
    Người dùng truy cập màn hình Templates
    Xóa giao diện Vegan
    Compare Two Image    vegan-header-actual.jpg    vegan-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-banner-actual.jpg    vegan-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-service-actual.jpg    vegan-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-content-service-actual.jpg    vegan-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-custom1-actual.jpg    vegan-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-custom2-actual.jpg    vegan-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-default-actual.jpg    vegan-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-default-content-actual.jpg    vegan-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-testimonial-actual.jpg    vegan-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-content-testimonial-actual.jpg    vegan-content-testimonial-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    vegan-title-blog-actual.jpg    vegan-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    vegan-content-blog-actual.jpg    vegan-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-footer-actual.jpg    vegan-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-header-actual-mobile.jpg    vegan-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-service-actual-mobile.jpg    vegan-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-content-service-actual-mobile.jpg    vegan-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-custom1-actual-mobile.jpg    vegan-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-custom2-actual-mobile.jpg    vegan-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-default-actual-mobile.jpg    vegan-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-default-content-actual-mobile.jpg    vegan-default-content-expected-mobilejpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-title-testimonial-actual-mobile.jpg    vegan-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-content-testimonial-actual-mobile.jpg    vegan-content-testimonial-expected-mobile.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    vegan-title-blog-actual.jpg    vegan-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    vegan-content-blog-actual.jpg    vegan-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    vegan-footer-actual-mobile.jpg    vegan-footer-expected-mobile.jpg

eMobile theme
    [Tags]    all    theme
    Update Giao diện mặc định theo config: eMobile_config.json
    Người dùng đang ở màn hình Templates
    Mở giao diện Giao diện mặc định trong màn hình "My Templates"
    Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Hệ thống hiển thị thông báo "Phát hành thành công"
    Chụp ảnh ${header} với tên eMobile-header-actual.jpg
    Chụp ảnh ${main-banner} với tên eMobile-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên eMobile-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên eMobile-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên eMobile-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên eMobile-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên eMobile-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên eMobile-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên eMobile-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên eMobile-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên eMobile-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên eMobile-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên eMobile-footer-actual.jpg
    Close Browser
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên eMobile-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên eMobile-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên eMobile-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên eMobile-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên eMobile-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên eMobile-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên eMobile-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên eMobile-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên eMobile-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên eMobile-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên eMobile-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên eMobile-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên eMobile-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-header-actual.jpg    eMobile-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-banner-actual.jpg    eMobile-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-service-actual.jpg    eMobile-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-service-actual.jpg    eMobile-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-custom1-actual.jpg    eMobile-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-custom2-actual.jpg    eMobile-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-default-actual.jpg    eMobile-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-default-content-actual.jpg    eMobile-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-testimonial-actual.jpg    eMobile-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-testimonial-actual.jpg    eMobile-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-blog-actual.jpg    eMobile-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-blog-actual.jpg    eMobile-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-footer-actual.jpg    eMobile-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-header-actual-mobile.jpg    eMobile-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-banner-actual-mobile.jpg    eMobile-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-service-actual-mobile.jpg    eMobile-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-service-actual-mobile.jpg    eMobile-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-custom1-actual-mobile.jpg    eMobile-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-custom2-actual-mobile.jpg    eMobile-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-default-actual-mobile.jpg    eMobile-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-default-content-actual-mobile.jpg    eMobile-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-testimonial-actual-mobile.jpg    eMobile-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-testimonial-actual-mobile.jpg    eMobile-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-title-blog-actual-mobile.jpg    eMobile-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-content-blog-actual-mobile.jpg    eMobile-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    eMobile-footer-actual-mobile.jpg    eMobile-footer-expected-mobile.jpg

aMotor theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: aMotor_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên aMotor-header-actual.jpg
    Chụp ảnh ${main-banner} với tên aMotor-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên aMotor-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên aMotor-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên aMotor-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên aMotor-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên aMotor-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên aMotor-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên aMotor-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên aMotor-content-testimonial-actual.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên aMotor-title-blog-expected.jpg
    # Chụp ảnh ${section-blog-content} với tên aMotor-content-blog-expected.jpg
    Chụp ảnh ${footer} với tên aMotor-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên aMotor-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên aMotor-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên aMotor-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên aMotor-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên aMotor-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên aMotor-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên aMotor-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên aMotor-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên aMotor-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên aMotor-content-testimonial-actual-mobile.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên aMotor-title-blog-expected.jpg
    # Chụp ảnh ${section-blog-content} với tên aMotor-content-blog-expected.jpg
    Chụp ảnh ${footer} với tên aMotor-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-header-actual.jpg    aMotor-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-banner-actual.jpg    aMotor-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-service-actual.jpg    aMotor-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-service-actual.jpg    aMotor-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-custom1-actual.jpg    aMotor-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-custom2-actual.jpg    aMotor-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-default-actual.jpg    aMotor-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-default-content-actual.jpg    aMotor-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-testimonial-actual.jpg    aMotor-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-testimonial-actual.jpg    aMotor-content-testimonial-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-blog-actual.jpg    aMotor-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-blog-actual.jpg    aMotor-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-footer-actual.jpg    aMotor-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-header-actual-mobile.jpg    aMotor-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-banner-actual-mobile.jpg    aMotor-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-service-actual-mobile.jpg    aMotor-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-service-actual-mobile.jpg    aMotor-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-custom1-actual-mobile.jpg    aMotor-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-custom2-actual-mobile.jpg    aMotor-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-default-actual-mobile.jpg    aMotor-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-default-content-actual-mobile.jpg    aMotor-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-testimonial-actual-mobile.jpg    aMotor-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-testimonial-actual-mobile.jpg    aMotor-content-testimonial-expected-mobile.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    aMotor-title-blog-actual.jpg    aMotor-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    aMotor-content-blog-actual.jpg    aMotor-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    aMotor-footer-actual-mobile.jpg    aMotor-footer-expected-mobile.jpg

Embeau theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: embeau_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên embeau-header-actual.jpg
    Chụp ảnh ${main-banner} với tên embeau-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên embeau-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên embeau-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên embeau-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên embeau-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên embeau-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên embeau-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên embeau-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên embeau-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên embeau-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên embeau-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên embeau-footer-actual.jpg
    Close Browser
    # mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên embeau-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên embeau-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên embeau-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên embeau-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên embeau-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên embeau-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên embeau-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên embeau-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên embeau-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên embeau-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên embeau-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên embeau-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên embeau-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-header-actual.jpg    embeau-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-banner-actual.jpg    embeau-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-service-actual.jpg    embeau-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-service-actual.jpg    embeau-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-custom1-actual.jpg    embeau-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-custom2-actual.jpg    embeau-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-default-actual.jpg    embeau-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-default-content-actual.jpg    embeau-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-testimonial-actual.jpg    embeau-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-testimonial-actual.jpg    embeau-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-blog-actual.jpg    embeau-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-blog-actual.jpg    embeau-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-footer-actual.jpg    embeau-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-header-actual-mobile.jpg    embeau-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-banner-actual-mobile.jpg    embeau-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-service-actual-mobile.jpg    embeau-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-service-actual-mobile.jpg    embeau-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-custom1-actual-mobile.jpg    embeau-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-custom2-actual-mobile.jpg    embeau-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-default-actual-mobile.jpg    embeau-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-default-content-actual-mobile.jpg    embeau-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-testimonial-actual-mobile.jpg    embeau-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-testimonial-actual-mobile.jpg    embeau-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-title-blog-actual-mobile.jpg    embeau-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-content-blog-actual-mobile.jpg    embeau-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    embeau-footer-actual-mobile.jpg    embeau-footer-expected-mobile.jpg

Grocerin theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: grocerin_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên grocerin-header-actual.jpg
    Chụp ảnh ${main-banner} với tên grocerin-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên grocerin-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên grocerin-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên grocerin-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên grocerin-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên grocerin-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên grocerin-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên grocerin-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên grocerin-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên grocerin-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên grocerin-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên grocerin-footer-actual.jpg
    Close Browser
    # mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên grocerin-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên grocerin-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên grocerin-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên grocerin-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên grocerin-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên grocerin-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên grocerin-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên grocerin-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên grocerin-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên grocerin-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên grocerin-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên grocerin-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên grocerin-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-header-actual.jpg    grocerin-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-banner-actual.jpg    grocerin-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-service-actual.jpg    grocerin-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-service-actual.jpg    grocerin-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-custom1-actual.jpg    grocerin-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-custom2-actual.jpg    grocerin-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-default-actual.jpg    grocerin-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-default-content-actual.jpg    grocerin-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-testimonial-actual.jpg    grocerin-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-testimonial-actual.jpg    grocerin-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-blog-actual.jpg    grocerin-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-blog-actual.jpg    grocerin-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-footer-actual.jpg    grocerin-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-header-actual-mobile.jpg    grocerin-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-banner-actual-mobile.jpg    grocerin-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-service-actual-mobile.jpg    grocerin-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-service-actual-mobile.jpg    grocerin-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-custom1-actual-mobile.jpg    grocerin-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-custom2-actual-mobile.jpg    grocerin-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-default-actual-mobile.jpg    grocerin-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-default-content-actual-mobile.jpg    grocerin-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-testimonial-actual-mobile.jpg    grocerin-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-testimonial-actual-mobile.jpg    grocerin-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-title-blog-actual-mobile.jpg    grocerin-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-content-blog-actual-mobile.jpg    grocerin-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    grocerin-footer-actual-mobile.jpg    grocerin-footer-expected-mobile.jpg

Miss.A theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Miss.A_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Miss.A-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Miss.A-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Miss.A-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Miss.A-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Miss.A-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Miss.A-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Miss.A-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Miss.A-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Miss.A-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Miss.A-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Miss.A-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên Miss.A-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Miss.A-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Miss.A-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Miss.A-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Miss.A-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Miss.A-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Miss.A-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Miss.A-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Miss.A-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Miss.A-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Miss.A-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Miss.A-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Miss.A-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên Miss.A-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Miss.A-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-header-actual.jpg    Miss.A-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-banner-actual.jpg    Miss.A-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-service-actual.jpg    Miss.A-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-service-actual.jpg    Miss.A-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-custom1-actual.jpg    Miss.A-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-custom2-actual.jpg    Miss.A-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-default-actual.jpg    Miss.A-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-default-content-actual.jpg    Miss.A-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-testimonial-actual.jpg    Miss.A-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-testimonial-actual.jpg    Miss.A-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-blog-actual.jpg    Miss.A-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-blog-actual.jpg    Miss.A-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-footer-actual.jpg    Miss.A-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-header-actual-mobile.jpg    Miss.A-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-banner-actual-mobile.jpg    Miss.A-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-service-actual-mobile.jpg    Miss.A-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-service-actual-mobile.jpg    Miss.A-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-custom1-actual-mobile.jpg    Miss.A-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-custom2-actual-mobile.jpg    Miss.A-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-default-actual-mobile.jpg    Miss.A-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-default-content-actual-mobile.jpg    Miss.A-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-testimonial-actual-mobile.jpg    Miss.A-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-testimonial-actual-mobile.jpg    Miss.A-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-title-blog-actual-mobile.jpg    Miss.A-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-content-blog-actual-mobile.jpg    Miss.A-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Miss.A-footer-actual-mobile.jpg    Miss.A-footer-expected-mobile.jpg

Uniqueen theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Uniqueen_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Uniqueen-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Uniqueen-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Uniqueen-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Uniqueen-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Uniqueen-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Uniqueen-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Uniqueen-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Uniqueen-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Uniqueen-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Uniqueen-content-testimonial-actual.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên Uniqueen-title-blog-actual.jpg
    # Chụp ảnh ${section-blog-content} với tên Uniqueen-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Uniqueen-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Uniqueen-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Uniqueen-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Uniqueen-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Uniqueen-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Uniqueen-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Uniqueen-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Uniqueen-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Uniqueen-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Uniqueen-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Uniqueen-content-testimonial-actual-mobile.jpg
    # Không có blog
    # Chụp ảnh ${section-title-blog} với tên Uniqueen-title-blog-actual.jpg
    # Chụp ảnh ${section-blog-content} với tên Uniqueen-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Uniqueen-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-header-actual.jpg    Uniqueen-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-banner-actual.jpg    Uniqueen-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-service-actual.jpg    Uniqueen-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-service-actual.jpg    Uniqueen-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-custom1-actual.jpg    Uniqueen-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-custom2-actual.jpg    Uniqueen-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-default-actual.jpg    Uniqueen-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-default-content-actual.jpg    Uniqueen-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-testimonial-actual.jpg    Uniqueen-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-testimonial-actual.jpg    Uniqueen-content-testimonial-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-blog-actual.jpg    Uniqueen-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-blog-actual.jpg    Uniqueen-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-footer-actual.jpg    Uniqueen-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-header-actual-mobile.jpg    Uniqueen-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-banner-actual-mobile.jpg    Uniqueen-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-service-actual-mobile.jpg    Uniqueen-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-service-actual-mobile.jpg    Uniqueen-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-custom1-actual-mobile.jpg    Uniqueen-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-custom2-actual-mobile.jpg    Uniqueen-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-default-actual-mobile.jpg    Uniqueen-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-default-content-actual-mobile.jpg    Uniqueen-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-testimonial-actual-mobile.jpg    Uniqueen-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-testimonial-actual-mobile.jpg    Uniqueen-content-testimonial-expected-mobile.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-title-blog-actual.jpg    Uniqueen-title-blog-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-content-blog-actual.jpg    Uniqueen-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Uniqueen-footer-actual-mobile.jpg    Uniqueen-footer-expected-mobile.jpg

Homelux theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: homelux_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên homelux-header-actual.jpg
    Chụp ảnh ${main-banner} với tên homelux-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên homelux-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên homelux-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên homelux-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên homelux-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên homelux-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên homelux-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên homelux-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên homelux-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên homelux-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên homelux-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên homelux-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên homelux-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên homelux-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên homelux-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên homelux-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên homelux-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên homelux-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên homelux-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên homelux-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên homelux-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên homelux-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên homelux-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên homelux-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên homelux-footer-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    homelux-header-actual.jpg    homelux-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-header-actual.jpg    homelux-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-banner-actual.jpg    homelux-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-service-actual.jpg    homelux-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-service-actual.jpg    homelux-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-custom1-actual.jpg    homelux-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-custom2-actual.jpg    homelux-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-default-actual.jpg    homelux-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-default-content-actual.jpg    homelux-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-testimonial-actual.jpg    homelux-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-testimonial-actual.jpg    homelux-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-blog-actual.jpg    homelux-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-blog-actual.jpg    homelux-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-footer-actual.jpg    homelux-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-header-actual-mobile.jpg    homelux-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-header-actual-mobile.jpg    homelux-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-banner-actual-mobile.jpg    homelux-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-service-actual-mobile.jpg    homelux-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-service-actual-mobile.jpg    homelux-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-custom1-actual-mobile.jpg    homelux-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-custom2-actual-mobile.jpg    homelux-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-default-actual-mobile.jpg    homelux-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-default-content-actual-mobile.jpg    homelux-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-testimonial-actual-mobile.jpg    homelux-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-testimonial-actual-mobile.jpg    homelux-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-title-blog-actual-mobile.jpg    homelux-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-content-blog-actual-mobile.jpg    homelux-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    homelux-footer-actual-mobile.jpg    homelux-footer-expected-mobile.jpg

Olives theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Olives_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Olives-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Olives-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Olives-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Olives-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Olives-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Olives-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Olives-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Olives-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Olives-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Olives-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Olives-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên Olives-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Olives-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Olives-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Olives-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Olives-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Olives-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Olives-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Olives-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Olives-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Olives-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Olives-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Olives-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Olives-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên Olives-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Olives-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-header-actual.jpg    Olives-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-banner-actual.jpg    Olives-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-title-service-actual.jpg    Olives-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-content-service-actual.jpg    Olives-content-service-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Olives-title-custom1-actual.jpg    Olives-title-custom1-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-custom2-actual.jpg    Olives-title-custom2-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-default-actual.jpg    Olives-title-default-expected.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Olives-default-content-actual.jpg    Olives-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-title-testimonial-actual.jpg    Olives-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-content-testimonial-actual.jpg    Olives-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-blog-actual.jpg    Olives-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-content-blog-actual.jpg    Olives-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-footer-actual.jpg    Olives-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-header-actual-mobile.jpg    Olives-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-banner-actual-mobile.jpg    Olives-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-title-service-actual.jpg    Olives-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-content-service-actual-mobile.jpg    Olives-content-service-expected-mobile.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Olives-title-custom1-actual-mobile.jpg    Olives-title-custom1-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-custom2-actual-mobile.jpg    Olives-title-custom2-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-default-actual-mobile.jpg    Olives-title-default-expected-mobile.jpg
    # Run Keyword And Continue On Failure    Compare Two Image    Olives-default-content-actual-mobile.jpg    Olives-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-title-testimonial-actual-mobile.jpg    Olives-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-content-testimonial-actual-mobile.jpg    Olives-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-title-blog-actual-mobile.jpg    Olives-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Olives-content-blog-actual-mobile.jpg    Olives-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Olives-footer-actual-mobile.jpg    Olives-footer-expected-mobile.jpg

Kidora theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Kidora_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Kidora-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Kidora-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Kidora-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Kidora-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Kidora-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Kidora-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Kidora-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Kidora-default-content-actual.jpg
    #Chụp ảnh ${section-title-testimonial} với tên Kidora-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Kidora-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Kidora-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Kidora-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Kidora-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Kidora-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Kidora-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Kidora-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Kidora-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Kidora-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Kidora-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Kidora-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Kidora-default-content-actual-mobile.jpg
    #Chụp ảnh ${section-title-testimonial} với tên Kidora-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Kidora-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Kidora-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Kidora-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Kidora-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-header-actual.jpg    Kidora-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-banner-actual.jpg    Kidora-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-service-actual.jpg    Kidora-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-service-actual.jpg    Kidora-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-custom1-actual.jpg    Kidora-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-custom2-actual.jpg    Kidora-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-default-actual.jpg    Kidora-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-default-content-actual.jpg    Kidora-default-content-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-testimonial-actual.jpg    Kidora-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-testimonial-actual.jpg    Kidora-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-blog-actual.jpg    Kidora-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-blog-actual.jpg    Kidora-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-footer-actual.jpg    Kidora-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-header-actual-mobile.jpg    Kidora-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-banner-actual-mobile.jpg    Kidora-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-service-actual-mobile.jpg    Kidora-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-service-actual-mobile.jpg    Kidora-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-custom1-actual-mobile.jpg    Kidora-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-custom2-actual-mobile.jpg    Kidora-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-default-actual-mobile.jpg    Kidora-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-default-content-actual-mobile.jpg    Kidora-default-content-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-testimonial-actual-mobile.jpg    Kidora-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-testimonial-actual-mobile.jpg    Kidora-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-title-blog-actual-mobile.jpg    Kidora-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Kidora-content-blog-actual-mobile.jpg    Kidora-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Kidora-footer-actual-mobile.jpg    Kidora-footer-expected-mobile.jpg

BioPharmacy theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: BioPharmacy_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên BioPharmacy-header-actual.jpg
    Chụp ảnh ${main-banner} với tên BioPharmacy-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên BioPharmacy-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên BioPharmacy-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên BioPharmacy-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên BioPharmacy-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên BioPharmacy-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên BioPharmacy-default-content-actual.jpg
    #Chụp ảnh ${section-title-testimonial} với tên BioPharmacy-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên BioPharmacy-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên BioPharmacy-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên BioPharmacy-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên BioPharmacy-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên BioPharmacy-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên BioPharmacy-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên BioPharmacy-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên BioPharmacy-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên BioPharmacy-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên BioPharmacy-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên BioPharmacy-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên BioPharmacy-default-content-actual-mobile.jpg
    #Chụp ảnh ${section-title-testimonial} với tên BioPharmacy-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên BioPharmacy-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên BioPharmacy-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên BioPharmacy-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên BioPharmacy-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-header-actual.jpg    BioPharmacy-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-banner-actual.jpg    BioPharmacy-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-service-actual.jpg    BioPharmacy-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-service-actual.jpg    BioPharmacy-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-custom1-actual.jpg    BioPharmacy-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-custom2-actual.jpg    BioPharmacy-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-default-actual.jpg    BioPharmacy-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-default-content-actual.jpg    BioPharmacy-default-content-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-testimonial-actual.jpg    BioPharmacy-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-testimonial-actual.jpg    BioPharmacy-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-blog-actual.jpg    BioPharmacy-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-blog-actual.jpg    BioPharmacy-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-footer-actual.jpg    BioPharmacy-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-header-actual-mobile.jpg    BioPharmacy-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-banner-actual-mobile.jpg    BioPharmacy-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-service-actual-mobile.jpg    BioPharmacy-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-service-actual-mobile.jpg    BioPharmacy-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-custom1-actual-mobile.jpg    BioPharmacy-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-custom2-actual-mobile.jpg    BioPharmacy-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-default-actual-mobile.jpg    BioPharmacy-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-default-content-actual-mobile.jpg    BioPharmacy-default-content-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-testimonial-actual-mobile.jpg    BioPharmacy-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-testimonial-actual-mobile.jpg    BioPharmacy-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-title-blog-actual-mobile.jpg    BioPharmacy-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-content-blog-actual-mobile.jpg    BioPharmacy-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    BioPharmacy-footer-actual-mobile.jpg    BioPharmacy-footer-expected-mobile.jpg

Buildmore theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Buildmore_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Buildmore-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Buildmore-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Buildmore-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Buildmore-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Buildmore-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Buildmore-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Buildmore-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Buildmore-default-content-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Buildmore-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Buildmore-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Buildmore-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Buildmore-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Buildmore-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Buildmore-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Buildmore-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Buildmore-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Buildmore-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Buildmore-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Buildmore-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Buildmore-default-content-actual-mobile.jpg
    #Chụp ảnh ${section-title-testimonial} với tên Buildmore-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Buildmore-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Buildmore-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Buildmore-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Buildmore-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-header-actual.jpg    Buildmore-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-banner-actual.jpg    Buildmore-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-service-actual.jpg    Buildmore-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-content-service-actual.jpg    Buildmore-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-custom1-actual.jpg    Buildmore-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-custom2-actual.jpg    Buildmore-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-default-actual.jpg    Buildmore-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-default-content-actual.jpg    Buildmore-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-content-testimonial-actual.jpg    Buildmore-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-blog-actual.jpg    Buildmore-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Buildmore-content-blog-actual.jpg    Buildmore-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-footer-actual.jpg    Buildmore-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-header-actual-mobile.jpg    Buildmore-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-banner-actual-mobile.jpg    Buildmore-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-service-actual-mobile.jpg    Buildmore-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-content-service-actual-mobile.jpg    Buildmore-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-custom1-actual-mobile.jpg    Buildmore-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-custom2-actual-mobile.jpg    Buildmore-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-default-actual-mobile.jpg    Buildmore-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-default-content-actual-mobile.jpg    Buildmore-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-content-testimonial-actual-mobile.jpg    Buildmore-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Buildmore-title-blog-actual-mobile.jpg    Buildmore-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmorecontent-blog-actual-mobile.jpg    Buildmore-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Buildmore-footer-actual-mobile.jpg    Buildmore-footer-expected-mobile.jpg

Everyday theme
    [Tags]    all    theme
    Update Giao diện mặc định theo config: Everyday_config.json
    Người dùng đang ở màn hình Templates
    Mở giao diện Giao diện mặc định trong màn hình "My Templates"
    Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Hệ thống hiển thị thông báo "Phát hành thành công"
    Chụp ảnh ${header} với tên Everyday-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Everyday-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Everyday-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Everyday-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Everyday-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Everyday-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Everyday-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Everyday-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Everyday-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Everyday-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Everyday-title-blog-actual.jpg
    Chụp ảnh ${section-blog-content} với tên Everyday-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Everyday-footer-actual.jpg
    Close Browser
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Everyday-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Everyday-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Everyday-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Everyday-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Everyday-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Everyday-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Everyday-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Everyday-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Everyday-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Everyday-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Everyday-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên Everyday-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Everyday-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-header-actual.jpg    Everyday-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-banner-actual.jpg    Everyday-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-service-actual.jpg    Everyday-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-service-actual.jpg    Everyday-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-custom1-actual.jpg    Everyday-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-custom2-actual.jpg    Everyday-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-default-actual.jpg    Everyday-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-default-content-actual.jpg    Everyday-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-testimonial-actual.jpg    Everyday-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-testimonial-actual.jpg    Everyday-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-blog-actual.jpg    Everyday-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-blog-actual.jpg    Everyday-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-footer-actual.jpg    Everyday-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-header-actual-mobile.jpg    Everyday-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-banner-actual-mobile.jpg    Everyday-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-service-actual-mobile.jpg    Everyday-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-service-actual-mobile.jpg    Everyday-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-custom1-actual-mobile.jpg    Everyday-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-custom2-actual-mobile.jpg    Everyday-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-default-actual-mobile.jpg    Everyday-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-default-content-actual-mobile.jpg    Everyday-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-testimonial-actual-mobile.jpg    Everyday-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-testimonial-actual-mobile.jpg    Everyday-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-title-blog-actual-mobile.jpg    Everyday-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-content-blog-actual-mobile.jpg    Everyday-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Everyday-footer-actual-mobile.jpg    Everyday-footer-expected-mobile.jpg

MiniMark theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: MiniMark_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên MiniMark-header-actual.jpg
    Chụp ảnh ${main-banner} với tên MiniMark-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên MiniMark-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên MiniMark-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên MiniMark-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên MiniMark-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên MiniMark-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên MiniMark-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên MiniMark-title-testimonial-actual.jpg
    #Chụp ảnh ${section-testimonial-content} với tên MiniMark-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên MiniMark-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên MiniMark-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên MiniMark-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên MiniMark-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên MiniMark-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên MiniMark-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên MiniMark-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên MiniMark-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên MiniMark-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên MiniMark-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên MiniMark-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên MiniMark-title-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-testimonial-content} với tên MiniMark-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên MiniMark-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên MiniMark-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên MiniMark-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-header-actual.jpg    MiniMark-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-banner-actual.jpg    MiniMark-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-service-actual.jpg    MiniMark-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-service-actual.jpg    MiniMark-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-custom1-actual.jpg    MiniMark-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-custom2-actual.jpg    MiniMark-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-default-actual.jpg    MiniMark-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-default-content-actual.jpg    MiniMark-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-testimonial-actual.jpg    MiniMark-title-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-testimonial-actual.jpg    MiniMark-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-blog-actual.jpg    MiniMark-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-blog-actual.jpg    MiniMark-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-footer-actual.jpg    MiniMark-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-header-actual-mobile.jpg    MiniMark-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-banner-actual-mobile.jpg    MiniMark-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-service-actual-mobile.jpg    MiniMark-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-service-actual-mobile.jpg    MiniMark-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-custom1-actual-mobile.jpg    MiniMark-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-custom2-actual-mobile.jpg    MiniMark-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-default-actual-mobile.jpg    MiniMark-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-default-content-actual-mobile.jpg    MiniMark-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-testimonial-actual-mobile.jpg    MiniMark-title-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-testimonial-actual-mobile.jpg    MiniMark-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-title-blog-actual-mobile.jpg    MiniMark-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    MiniMark-content-blog-actual-mobile.jpg    MiniMark-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MiniMark-footer-actual-mobile.jpg    MiniMark-footer-expected-mobile.jpg

Minshop theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Minshop_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Minshop-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Minshop-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Minshop-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Minshop-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Minshop-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Minshop-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Minshop-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Minshop-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Minshop-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Minshop-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Minshop-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Minshop-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Minshop-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Minshop-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Minshop-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Minshop-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Minshop-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Minshop-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Minshop-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Minshop-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Minshop-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Minshop-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Minshop-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Minshop-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Minshop-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Minshop-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-header-actual.jpg    Minshop-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-banner-actual.jpg    Minshop-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-service-actual.jpg    Minshop-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-service-actual.jpg    Minshop-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-custom1-actual.jpg    Minshop-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-custom2-actual.jpg    Minshop-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-default-actual.jpg    Minshop-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-default-content-actual.jpg    Minshop-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-testimonial-actual.jpg    Minshop-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-testimonial-actual.jpg    Minshop-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-blog-actual.jpg    Minshop-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-blog-actual.jpg    Minshop-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-footer-actual.jpg    Minshop-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-header-actual-mobile.jpg    Minshop-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-banner-actual-mobile.jpg    Minshop-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-service-actual-mobile.jpg    Minshop-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-service-actual-mobile.jpg    Minshop-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-custom1-actual-mobile.jpg    Minshop-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-custom2-actual-mobile.jpg    Minshop-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-default-actual-mobile.jpg    Minshop-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-default-content-actual-mobile.jpg    Minshop-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-testimonial-actual-mobile.jpg    Minshop-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-testimonial-actual-mobile.jpg    Minshop-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Minshop-title-blog-actual-mobile.jpg    Minshop-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Minshop-content-blog-actual-mobile.jpg    Minshop-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Minshop-footer-actual-mobile.jpg    Minshop-footer-expected-mobile.jpg

Memo theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Memo_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Memo-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Memo-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Memo-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Memo-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Memo-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Memo-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Memo-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Memo-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Memo-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Memo-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Memo-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Memo-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Memo-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Memo-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Memo-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Memo-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Memo-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Memo-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Memo-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Memo-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Memo-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Memo-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Memo-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Memo-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Memo-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Memo-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-header-actual.jpg    Memo-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-banner-actual.jpg    Memo-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-service-actual.jpg    Memo-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-content-service-actual.jpg    Memo-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-custom1-actual.jpg    Memo-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-custom2-actual.jpg    Memo-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-default-actual.jpg    Memo-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-default-content-actual.jpg    Memo-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-testimonial-actual.jpg    Memo-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-content-testimonial-actual.jpg    Memo-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Memo-title-blog-actual.jpg    Memo-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Memo-content-blog-actual.jpg    Memo-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-footer-actual.jpg    Memo-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-header-actual-mobile.jpg    Memo-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-banner-actual-mobile.jpg    Memo-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-service-actual-mobile.jpg    Memo-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-content-service-actual-mobile.jpg    Memo-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-custom1-actual-mobile.jpg    Memo-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-custom2-actual-mobile.jpg    Memo-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-default-actual-mobile.jpg    Memo-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-default-content-actual-mobile.jpg    Memo-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-title-testimonial-actual-mobile.jpg    Memo-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-content-testimonial-actual-mobile.jpg    Memo-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Memo-title-blog-actual-mobile.jpg    Memo-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Memo-content-blog-actual-mobile.jpg    Memo-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Memo-footer-actual-mobile.jpg    Memo-footer-expected-mobile.jpg

Shara theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Shara_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Shara-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Shara-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Shara-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Shara-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Shara-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Shara-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Shara-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Shara-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Shara-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Shara-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Shara-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Shara-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Shara-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Shara-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Shara-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Shara-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Shara-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Shara-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Shara-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Shara-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Shara-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Shara-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Shara-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Shara-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Shara-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Shara-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-header-actual.jpg    Shara-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-banner-actual.jpg    Shara-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-service-actual.jpg    Shara-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-content-service-actual.jpg    Shara-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-custom1-actual.jpg    Shara-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-custom2-actual.jpg    Shara-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-default-actual.jpg    Shara-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-default-content-actual.jpg    Shara-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-testimonial-actual.jpg    Shara-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-content-testimonial-actual.jpg    Shara-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shara-title-blog-actual.jpg    Shara-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shara-content-blog-actual.jpg    Shara-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-footer-actual.jpg    Shara-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-header-actual-mobile.jpg    Shara-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-banner-actual-mobile.jpg    Shara-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-service-actual-mobile.jpg    Shara-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-content-service-actual-mobile.jpg    Shara-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-custom1-actual-mobile.jpg    Shara-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-custom2-actual-mobile.jpg    Shara-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-default-actual-mobile.jpg    Shara-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-default-content-actual-mobile.jpg    Shara-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-title-testimonial-actual-mobile.jpg    Shara-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-content-testimonial-actual-mobile.jpg    Shara-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shara-title-blog-actual-mobile.jpg    Shara-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shara-content-blog-actual-mobile.jpg    Shara-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shara-footer-actual-mobile.jpg    Shara-footer-expected-mobile.jpg

Comes theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Comes_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Comes-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Comes-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Comes-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Comes-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Comes-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Comes-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Comes-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Comes-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Comes-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Comes-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Comes-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Comes-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Comes-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Comes-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Comes-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Comes-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Comes-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Comes-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Comes-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Comes-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Comes-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Comes-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Comes-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Comes-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Comes-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Comes-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-header-actual.jpg    Comes-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-banner-actual.jpg    Comes-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-service-actual.jpg    Comes-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-content-service-actual.jpg    Comes-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-custom1-actual.jpg    Comes-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-custom2-actual.jpg    Comes-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-default-actual.jpg    Comes-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-default-content-actual.jpg    Comes-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-testimonial-actual.jpg    Comes-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-content-testimonial-actual.jpg    Comes-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Comes-title-blog-actual.jpg    Comes-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Comes-content-blog-actual.jpg    Comes-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-footer-actual.jpg    Comes-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-header-actual-mobile.jpg    Comes-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-banner-actual-mobile.jpg    Comes-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-service-actual-mobile.jpg    Comes-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-content-service-actual-mobile.jpg    Comes-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-custom1-actual-mobile.jpg    Comes-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-custom2-actual-mobile.jpg    Comes-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-default-actual-mobile.jpg    Comes-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-default-content-actual-mobile.jpg    Comes-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-title-testimonial-actual-mobile.jpg    Comes-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-content-testimonial-actual-mobile.jpg    Comes-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Comes-title-blog-actual-mobile.jpg    Comes-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Comes-content-blog-actual-mobile.jpg    Comes-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Comes-footer-actual-mobile.jpg    Comes-footer-expected-mobile.jpg

Butterfly theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Butterfly_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Butterfly-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Butterfly-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Butterfly-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Butterfly-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Butterfly-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Butterfly-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Butterfly-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Butterfly-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Butterfly-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Butterfly-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Butterfly-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Butterfly-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Butterfly-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Butterfly-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Butterfly-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Butterfly-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Butterfly-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Butterfly-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Butterfly-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Butterfly-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Butterfly-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Butterfly-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Butterfly-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Butterfly-title-blog-actual-mobile.jpg
    #Chụp ảnh ${section-blog-content} với tên Butterfly-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Butterfly-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-header-actual.jpg    Butterfly-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-banner-actual.jpg    Butterfly-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-service-actual.jpg    Butterfly-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-service-actual.jpg    Butterfly-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-custom1-actual.jpg    Butterfly-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-custom2-actual.jpg    Butterfly-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-default-actual.jpg    Butterfly-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-default-content-actual.jpg    Butterfly-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-testimonial-actual.jpg    Butterfly-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-testimonial-actual.jpg    Butterfly-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-blog-actual.jpg    Butterfly-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-blog-actual.jpg    Butterfly-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-footer-actual.jpg    Butterfly-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-header-actual-mobile.jpg    Butterfly-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-banner-actual-mobile.jpg    Butterfly-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-service-actual-mobile.jpg    Butterfly-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-service-actual-mobile.jpg    Butterfly-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-custom1-actual-mobile.jpg    Butterfly-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-custom2-actual-mobile.jpg    Butterfly-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-default-actual-mobile.jpg    Butterfly-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-default-content-actual-mobile.jpg    Butterfly-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-testimonial-actual-mobile.jpg    Butterfly-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-testimonial-actual-mobile.jpg    Butterfly-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Butterfly-title-blog-actual-mobile.jpg    Butterfly-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Butterfly-content-blog-actual-mobile.jpg    Butterfly-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Butterfly-footer-actual-mobile.jpg    Butterfly-footer-expected-mobile.jpg

Brain theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Brain_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Brain-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Brain-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Brain-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Brain-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Brain-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Brain-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Brain-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Brain-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Brain-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Brain-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Brain-title-blog-actual.jpg
    #Chụp ảnh ${section-blog-content} với tên Brain-content-blog-actual.jpg
    Chụp ảnh ${footer} với tên Brain-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Brain-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Brain-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Brain-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Brain-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Brain-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Brain-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Brain-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Brain-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Brain-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Brain-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Brain-title-blog-actual-mobile.jpg
    Chụp ảnh ${section-blog-content} với tên Brain-content-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Brain-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-header-actual.jpg    Brain-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-banner-actual.jpg    Brain-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-service-actual.jpg    Brain-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-content-service-actual.jpg    Brain-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-custom1-actual.jpg    Brain-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-custom2-actual.jpg    Brain-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-default-actual.jpg    Brain-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-default-content-actual.jpg    Brain-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-testimonial-actual.jpg    Brain-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-content-testimonial-actual.jpg    Brain-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-blog-actual.jpg    Brain-title-blog-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Brain-content-blog-actual.jpg    Brain-content-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-footer-actual.jpg    Brain-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-header-actual-mobile.jpg    Brain-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-banner-actual-mobile.jpg    Brain-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-service-actual-mobile.jpg    Brain-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-content-service-actual-mobile.jpg    Brain-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-custom1-actual-mobile.jpg    Brain-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-custom2-actual-mobile.jpg    Brain-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-default-actual-mobile.jpg    Brain-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-default-content-actual-mobile.jpg    Brain-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-testimonial-actual-mobile.jpg    Brain-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-content-testimonial-actual-mobile.jpg    Brain-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-title-blog-actual-mobile.jpg    Brain-title-blog-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Brain-content-blog-actual-mobile.jpg    Brain-content-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Brain-footer-actual-mobile.jpg    Brain-footer-expected-mobile.jpg

Homie theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Homie_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Homie-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Homie-banner-actual.jpg
    #Chụp ảnh ${section-title-service} với tên Homie-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Homie-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Homie-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Homie-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Homie-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Homie-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Homie-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Homie-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Homie-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Homie-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Homie-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Homie-banner-actual-mobile.jpg
    #Chụp ảnh ${section-title-service} với tên Homie-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Homie-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Homie-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Homie-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Homie-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Homie-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Homie-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Homie-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Homie-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Homie-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-header-actual.jpg    Homie-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-banner-actual.jpg    Homie-banner-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Homie-title-service-actual.jpg    Homie-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-content-service-actual.jpg    Homie-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-custom1-actual.jpg    Homie-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-custom2-actual.jpg    Homie-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-default-actual.jpg    Homie-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-default-content-actual.jpg    Homie-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-testimonial-actual.jpg    Homie-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-content-testimonial-actual.jpg    Homie-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-blog-actual.jpg    Homie-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-footer-actual.jpg    Homie-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-header-actual-mobile.jpg    Homie-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-banner-actual-mobile.jpg    Homie-banner-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Homie-title-service-actual-mobile.jpg    Homie-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-content-service-actual-mobile.jpg    Homie-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-custom1-actual-mobile.jpg    Homie-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-custom2-actual-mobile.jpg    Homie-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-default-actual-mobile.jpg    Homie-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-default-content-actual-mobile.jpg    Homie-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-testimonial-actual-mobile.jpg    Homie-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-content-testimonial-actual-mobile.jpg    Homie-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-title-blog-actual-mobile.jpg    Homie-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Homie-footer-actual-mobile.jpg    Homie-footer-expected-mobile.jpg

MyKing theme
    [Tags]    all    theme
    #Publish theme Giao diện mặc định theo config: MyKing_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên MyKing-header-actual.jpg
    Chụp ảnh ${main-banner} với tên MyKing-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên MyKing-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên MyKing-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên MyKing-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên MyKing-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên MyKing-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên MyKing-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên MyKing-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên MyKing-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên MyKing-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên MyKing-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên MyKing-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên MyKing-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên MyKing-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên MyKing-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên MyKing-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên MyKing-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên MyKing-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên MyKing-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên MyKing-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên MyKing-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên MyKing-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên MyKing-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-header-actual.jpg    MyKing-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-banner-actual.jpg    MyKing-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-service-actual.jpg    MyKing-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-content-service-actual.jpg    MyKing-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-custom1-actual.jpg    MyKing-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-custom2-actual.jpg    MyKing-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-default-actual.jpg    MyKing-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-default-content-actual.jpg    MyKing-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-testimonial-actual.jpg    MyKing-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-content-testimonial-actual.jpg    MyKing-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-blog-actual.jpg    MyKing-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-footer-actual.jpg    MyKing-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-header-actual-mobile.jpg    MyKing-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-banner-actual-mobile.jpg    MyKing-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-service-actual-mobile.jpg    MyKing-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-content-service-actual-mobile.jpg    MyKing-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-custom1-actual-mobile.jpg    MyKing-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-custom2-actual-mobile.jpg    MyKing-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-default-actual-mobile.jpg    MyKing-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-default-content-actual-mobile.jpg    MyKing-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-testimonial-actual-mobile.jpg    MyKing-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-content-testimonial-actual-mobile.jpg    MyKing-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-title-blog-actual-mobile.jpg    MyKing-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    MyKing-footer-actual-mobile.jpg    MyKing-footer-expected-mobile.jpg

Beshop theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Beshop_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Beshop-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Beshop-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Beshop-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Beshop-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Beshop-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Beshop-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Beshop-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Beshop-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Beshop-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Beshop-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Beshop-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Beshop-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Beshop-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Beshop-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Beshop-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Beshop-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Beshop-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Beshop-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Beshop-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Beshop-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Beshop-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Beshop-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Beshop-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Beshop-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-header-actual.jpg    Beshop-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-banner-actual.jpg    Beshop-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-service-actual.jpg    Beshop-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-content-service-actual.jpg    Beshop-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-custom1-actual.jpg    Beshop-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-custom2-actual.jpg    Beshop-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-default-actual.jpg    Beshop-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-default-content-actual.jpg    Beshop-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-testimonial-actual.jpg    Beshop-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-content-testimonial-actual.jpg    Beshop-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-blog-actual.jpg    Beshop-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-footer-actual.jpg    Beshop-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-header-actual-mobile.jpg    Beshop-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-banner-actual-mobile.jpg    Beshop-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-service-actual-mobile.jpg    Beshop-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-content-service-actual-mobile.jpg    Beshop-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-custom1-actual-mobile.jpg    Beshop-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-custom2-actual-mobile.jpg    Beshop-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-default-actual-mobile.jpg    Beshop-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-default-content-actual-mobile.jpg    Beshop-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-testimonial-actual-mobile.jpg    Beshop-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-content-testimonial-actual-mobile.jpg    Beshop-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-title-blog-actual-mobile.jpg    Beshop-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Beshop-footer-actual-mobile.jpg    Beshop-footer-expected-mobile.jpg

Shine theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Shine_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Shine-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Shine-banner-actual.jpg
    #Chụp ảnh ${section-title-service} với tên Shine-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Shine-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Shine-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Shine-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Shine-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Shine-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Shine-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Shine-content-testimonial-actual.jpg
    #Chụp ảnh ${section-title-blog} với tên Shine-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Shine-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Shine-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Shine-banner-actual-mobile.jpg
    #Chụp ảnh ${section-title-service} với tên Shine-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Shine-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Shine-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Shine-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Shine-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Shine-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Shine-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Shine-content-testimonial-actual-mobile.jpg
    #Chụp ảnh ${section-title-blog} với tên Shine-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Shine-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-header-actual.jpg    Shine-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-banner-actual.jpg    Shine-banner-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shine-title-service-actual.jpg    Shine-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-content-service-actual.jpg    Shine-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-custom1-actual.jpg    Shine-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-custom2-actual.jpg    Shine-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-default-actual.jpg    Shine-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-default-content-actual.jpg    Shine-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-testimonial-actual.jpg    Shine-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-content-testimonial-actual.jpg    Shine-content-testimonial-expected.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shine-title-blog-actual.jpg    Shine-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-footer-actual.jpg    Shine-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-header-actual-mobile.jpg    Shine-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-banner-actual-mobile.jpg    Shine-banner-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shine-title-service-actual-mobile.jpg    Shine-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-content-service-actual-mobile.jpg    Shine-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-custom1-actual-mobile.jpg    Shine-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-custom2-actual-mobile.jpg    Shine-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-default-actual-mobile.jpg    Shine-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-default-content-actual-mobile.jpg    Shine-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-title-testimonial-actual-mobile.jpg    Shine-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-content-testimonial-actual-mobile.jpg    Shine-content-testimonial-expected-mobile.jpg
    #Run Keyword And Continue On Failure    Compare Two Image    Shine-title-blog-actual-mobile.jpg    Shine-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Shine-footer-actual-mobile.jpg    Shine-footer-expected-mobile.jpg

Agriculture theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Agriculture_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Agriculture-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Agriculture-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Agriculture-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Agriculture-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Agriculture-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Agriculture-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Agriculture-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Agriculture-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Agriculture-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Agriculture-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Agriculture-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Agriculture-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Agriculture-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Agriculture-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Agriculture-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Agriculture-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Agriculture-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Agriculture-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Agriculture-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Agriculture-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Agriculture-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Agriculture-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Agriculture-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Agriculture-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-header-actual.jpg    Agriculture-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-banner-actual.jpg    Agriculture-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-service-actual.jpg    Agriculture-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-content-service-actual.jpg    Agriculture-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-custom1-actual.jpg    Agriculture-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-custom2-actual.jpg    Agriculture-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-default-actual.jpg    Agriculture-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-default-content-actual.jpg    Agriculture-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-testimonial-actual.jpg    Agriculture-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-content-testimonial-actual.jpg    Agriculture-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-blog-actual.jpg    Agriculture-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-footer-actual.jpg    Agriculture-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-header-actual-mobile.jpg    Agriculture-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-banner-actual-mobile.jpg    Agriculture-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-service-actual-mobile.jpg    Agriculture-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-content-service-actual-mobile.jpg    Agriculture-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-custom1-actual-mobile.jpg    Agriculture-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-custom2-actual-mobile.jpg    Agriculture-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-default-actual-mobile.jpg    Agriculture-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-default-content-actual-mobile.jpg    Agriculture-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-testimonial-actual-mobile.jpg    Agriculture-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-content-testimonial-actual-mobile.jpg    Agriculture-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-title-blog-actual-mobile.jpg    Agriculture-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Agriculture-footer-actual-mobile.jpg    Agriculture-footer-expected-mobile.jpg

Growth theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Growth_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Growth-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Growth-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Growth-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Growth-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Growth-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Growth-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Growth-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Growth-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Growth-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Growth-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Growth-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Growth-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Growth-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Growth-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Growth-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Growth-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Growth-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Growth-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Growth-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Growth-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Growth-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Growth-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Growth-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Growth-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-header-actual.jpg    Growth-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-banner-actual.jpg    Growth-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-service-actual.jpg    Growth-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-content-service-actual.jpg    Growth-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-custom1-actual.jpg    Growth-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-custom2-actual.jpg    Growth-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-default-actual.jpg    Growth-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-default-content-actual.jpg    Growth-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-testimonial-actual.jpg    Growth-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-content-testimonial-actual.jpg    Growth-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-blog-actual.jpg    Growth-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-footer-actual.jpg    Growth-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-header-actual-mobile.jpg    Growth-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-banner-actual-mobile.jpg    Growth-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-service-actual-mobile.jpg    Growth-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-content-service-actual-mobile.jpg    Growth-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-custom1-actual-mobile.jpg    Growth-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-custom2-actual-mobile.jpg    Growth-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-default-actual-mobile.jpg    Growth-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-default-content-actual-mobile.jpg    Growth-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-testimonial-actual-mobile.jpg    Growth-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-content-testimonial-actual-mobile.jpg    Growth-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-title-blog-actual-mobile.jpg    Growth-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Growth-footer-actual-mobile.jpg    Growth-footer-expected-mobile.jpg

Penguin theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Penguin_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Penguin-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Penguin-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Penguin-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Penguin-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Penguin-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Penguin-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Penguin-title-default-actual.jpg
    Chụp ảnh ${default-content} với tên Penguin-default-content-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Penguin-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Penguin-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Penguin-title-blog-actual.jpg
    Chụp ảnh ${footer} với tên Penguin-footer-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Penguin-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Penguin-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Penguin-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Penguin-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Penguin-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Penguin-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Penguin-title-default-actual-mobile.jpg
    Chụp ảnh ${default-content} với tên Penguin-default-content-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Penguin-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Penguin-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Penguin-title-blog-actual-mobile.jpg
    Chụp ảnh ${footer} với tên Penguin-footer-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-header-actual.jpg    Penguin-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-banner-actual.jpg    Penguin-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-service-actual.jpg    Penguin-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-content-service-actual.jpg    Penguin-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-custom1-actual.jpg    Penguin-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-custom2-actual.jpg    Penguin-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-default-actual.jpg    Penguin-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-default-content-actual.jpg    Penguin-default-content-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-testimonial-actual.jpg    Penguin-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-content-testimonial-actual.jpg    Penguin-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-blog-actual.jpg    Penguin-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-footer-actual.jpg    Penguin-footer-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-header-actual-mobile.jpg    Penguin-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-banner-actual-mobile.jpg    Penguin-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-service-actual-mobile.jpg    Penguin-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-content-service-actual-mobile.jpg    Penguin-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-custom1-actual-mobile.jpg    Penguin-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-custom2-actual-mobile.jpg    Penguin-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-default-actual-mobile.jpg    Penguin-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-default-content-actual-mobile.jpg    Penguin-default-content-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-testimonial-actual-mobile.jpg    Penguin-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-content-testimonial-actual-mobile.jpg    Penguin-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-title-blog-actual-mobile.jpg    Penguin-title-blog-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Penguin-footer-actual-mobile.jpg    Penguin-footer-expected-mobile.jpg

D-Concept theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: D-Concept_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên D-Concept-header-actual.jpg
    Chụp ảnh ${main-banner} với tên D-Concept-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên D-Concept-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên D-Concept-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên D-Concept-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên D-Concept-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên D-Concept-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên D-Concept-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên D-Concept-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên D-Concept-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên D-Concept-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên D-Concept-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên D-Concept-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên D-Concept-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên D-Concept-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên D-Concept-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên D-Concept-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên D-Concept-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên D-Concept-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên D-Concept-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-header-actual.jpg    D-Concept-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-banner-actual.jpg    D-Concept-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-service-actual.jpg    D-Concept-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-content-service-actual.jpg    D-Concept-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-custom1-actual.jpg    D-Concept-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-custom2-actual.jpg    D-Concept-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-default-actual.jpg    D-Concept-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-testimonial-actual.jpg    D-Concept-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-content-testimonial-actual.jpg    D-Concept-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-blog-actual.jpg    D-Concept-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-header-actual-mobile.jpg    D-Concept-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-banner-actual-mobile.jpg    D-Concept-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-service-actual-mobile.jpg    D-Concept-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-content-service-actual-mobile.jpg    D-Concept-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-custom1-actual-mobile.jpg    D-Concept-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-custom2-actual-mobile.jpg    D-Concept-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-default-actual-mobile.jpg    D-Concept-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-testimonial-actual-mobile.jpg    D-Concept-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-content-testimonial-actual-mobile.jpg    D-Concept-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    D-Concept-title-blog-actual-mobile.jpg    D-Concept-title-blog-expected-mobile.jpg

M.Lips theme
    [Tags]    theme    all
    Publish theme Giao diện mặc định theo config: M.Lips_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên M.Lips-header-actual.jpg
    Chụp ảnh ${main-banner} với tên M.Lips-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên M.Lips-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên M.Lips-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên M.Lips-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên M.Lips-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên M.Lips-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên M.Lips-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên M.Lips-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên M.Lips-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên M.Lips-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên M.Lips-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên M.Lips-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên M.Lips-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên M.Lips-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên M.Lips-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên M.Lips-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên M.Lips-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên M.Lips-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên M.Lips-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-header-actual.jpg    M.Lips-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-banner-actual.jpg    M.Lips-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-service-actual.jpg    M.Lips-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-content-service-actual.jpg    M.Lips-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-custom1-actual.jpg    M.Lips-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-custom2-actual.jpg    M.Lips-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-default-actual.jpg    M.Lips-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-testimonial-actual.jpg    M.Lips-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-content-testimonial-actual.jpg    M.Lips-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-blog-actual.jpg    M.Lips-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-header-actual-mobile.jpg    M.Lips-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-banner-actual-mobile.jpg    M.Lips-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-service-actual-mobile.jpg    M.Lips-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-content-service-actual-mobile.jpg    M.Lips-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-custom1-actual-mobile.jpg    M.Lips-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-custom2-actual-mobile.jpg    M.Lips-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-default-actual-mobile.jpg    M.Lips-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-testimonial-actual-mobile.jpg    M.Lips-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-content-testimonial-actual-mobile.jpg    M.Lips-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    M.Lips-title-blog-actual-mobile.jpg    M.Lips-title-blog-expected-mobile.jpg

Tropic theme
    [Tags]    all    theme
    Publish theme Giao diện mặc định theo config: Tropic_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Tropic-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Tropic-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Tropic-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Tropic-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Tropic-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Tropic-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Tropic-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Tropic-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Tropic-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Tropic-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Tropic-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Tropic-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Tropic-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Tropic-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Tropic-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Tropic-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Tropic-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Tropic-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Tropic-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Tropic-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-header-actual.jpg    Tropic-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-banner-actual.jpg    Tropic-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-service-actual.jpg    Tropic-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-content-service-actual.jpg    Tropic-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-custom1-actual.jpg    Tropic-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-custom2-actual.jpg    Tropic-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-default-actual.jpg    Tropic-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-testimonial-actual.jpg    Tropic-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-content-testimonial-actual.jpg    Tropic-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-blog-actual.jpg    Tropic-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-header-actual-mobile.jpg    Tropic-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-banner-actual-mobile.jpg    Tropic-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-service-actual-mobile.jpg    Tropic-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-content-service-actual-mobile.jpg    Tropic-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-custom1-actual-mobile.jpg    Tropic-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-custom2-actual-mobile.jpg    Tropic-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-default-actual-mobile.jpg    Tropic-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-testimonial-actual-mobile.jpg    Tropic-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-content-testimonial-actual-mobile.jpg    Tropic-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Tropic-title-blog-actual-mobile.jpg    Tropic-title-blog-expected-mobile.jpg

Fintex theme
    [Tags]    all theme
    Publish theme Giao diện mặc định theo config: Fintex_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Fintex-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Fintex-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Fintex-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Fintex-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Fintex-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Fintex-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Fintex-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Fintex-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Fintex-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Fintex-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Fintex-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Fintex-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Fintex-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Fintex-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Fintex-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Fintex-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Fintex-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Fintex-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Fintex-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Fintex-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-header-actual.jpg    Fintex-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-banner-actual.jpg    Fintex-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-service-actual.jpg    Fintex-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-content-service-actual.jpg    Fintex-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-custom1-actual.jpg    Fintex-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-custom2-actual.jpg    Fintex-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-default-actual.jpg    Fintex-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-testimonial-actual.jpg    Fintex-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-content-testimonial-actual.jpg    Fintex-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-blog-actual.jpg    Fintex-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-header-actual-mobile.jpg    Fintex-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-banner-actual-mobile.jpg    Fintex-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-service-actual-mobile.jpg    Fintex-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-content-service-actual-mobile.jpg    Fintex-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-custom1-actual-mobile.jpg    Fintex-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-custom2-actual-mobile.jpg    Fintex-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-default-actual-mobile.jpg    Fintex-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-testimonial-actual-mobile.jpg    Fintex-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-content-testimonial-actual-mobile.jpg    Fintex-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Fintex-title-blog-actual-mobile.jpg    Fintex-title-blog-expected-mobile.jpg

Flowing theme
    Publish theme Giao diện mặc định theo config: Flowing_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Flowing-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Flowing-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Flowing-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Flowing-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Flowing-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Flowing-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Flowing-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Flowing-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Flowing-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Flowing-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Flowing-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Flowing-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Flowing-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Flowing-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Flowing-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Flowing-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Flowing-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Flowing-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Flowing-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Flowing-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-header-actual.jpg    Flowing-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-banner-actual.jpg    Flowing-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-service-actual.jpg    Flowing-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-content-service-actual.jpg    Flowing-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-custom1-actual.jpg    Flowing-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-custom2-actual.jpg    Flowing-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-default-actual.jpg    Flowing-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-testimonial-actual.jpg    Flowing-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-content-testimonial-actual.jpg    Flowing-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-blog-actual.jpg    Flowing-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-header-actual-mobile.jpg    Flowing-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-banner-actual-mobile.jpg    Flowing-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-service-actual-mobile.jpg    Flowing-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-content-service-actual-mobile.jpg    Flowing-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-custom1-actual-mobile.jpg    Flowing-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-custom2-actual-mobile.jpg    Flowing-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-default-actual-mobile.jpg    Flowing-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-testimonial-actual-mobile.jpg    Flowing-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-content-testimonial-actual-mobile.jpg    Flowing-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Flowing-title-blog-actual-mobile.jpg    Flowing-title-blog-expected-mobile.jpg

Klaire theme
    Publish theme Giao diện mặc định theo config: Klaire_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Klaire-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Klaire-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Klaire-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Klaire-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Klaire-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Klaire-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Klaire-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Klaire-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Klaire-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Klaire-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Klaire-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Klaire-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Klaire-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Klaire-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Klaire-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Klaire-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Klaire-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Klaire-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Klaire-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Klaire-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-header-actual.jpg    Klaire-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-banner-actual.jpg    Klaire-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-service-actual.jpg    Klaire-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-content-service-actual.jpg    Klaire-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-custom1-actual.jpg    Klaire-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-custom2-actual.jpg    Klaire-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-default-actual.jpg    Klaire-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-testimonial-actual.jpg    Klaire-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-content-testimonial-actual.jpg    Klaire-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-blog-actual.jpg    Klaire-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-header-actual-mobile.jpg    Klaire-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-banner-actual-mobile.jpg    Klaire-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-service-actual-mobile.jpg    Klaire-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-content-service-actual-mobile.jpg    Klaire-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-custom1-actual-mobile.jpg    Klaire-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-custom2-actual-mobile.jpg    Klaire-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-default-actual-mobile.jpg    Klaire-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-testimonial-actual-mobile.jpg    Klaire-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-content-testimonial-actual-mobile.jpg    Klaire-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Klaire-title-blog-actual-mobile.jpg    Klaire-title-blog-expected-mobile.jpg

Lippo theme
    Publish theme Giao diện mặc định theo config: Lippo_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Lippo-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Lippo-banner-actual.jpg
    Chụp ảnh ${section-title-service} với tên Lippo-title-service-actual.jpg
    Chụp ảnh ${section-service-content} với tên Lippo-content-service-actual.jpg
    Chụp ảnh ${section-title-custom1} với tên Lippo-title-custom1-actual.jpg
    Chụp ảnh ${section-title-custom2} với tên Lippo-title-custom2-actual.jpg
    Chụp ảnh ${section-title-default} với tên Lippo-title-default-actual.jpg
    Chụp ảnh ${section-title-testimonial} với tên Lippo-title-testimonial-actual.jpg
    Chụp ảnh ${section-testimonial-content} với tên Lippo-content-testimonial-actual.jpg
    Chụp ảnh ${section-title-blog} với tên Lippo-title-blog-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Lippo-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Lippo-banner-actual-mobile.jpg
    Chụp ảnh ${section-title-service} với tên Lippo-title-service-actual-mobile.jpg
    Chụp ảnh ${section-service-content} với tên Lippo-content-service-actual-mobile.jpg
    Chụp ảnh ${section-title-custom1} với tên Lippo-title-custom1-actual-mobile.jpg
    Chụp ảnh ${section-title-custom2} với tên Lippo-title-custom2-actual-mobile.jpg
    Chụp ảnh ${section-title-default} với tên Lippo-title-default-actual-mobile.jpg
    Chụp ảnh ${section-title-testimonial} với tên Lippo-title-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-testimonial-content} với tên Lippo-content-testimonial-actual-mobile.jpg
    Chụp ảnh ${section-title-blog} với tên Lippo-title-blog-actual-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-header-actual.jpg    Lippo-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-banner-actual.jpg    Lippo-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-service-actual.jpg    Lippo-title-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-content-service-actual.jpg    Lippo-content-service-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-custom1-actual.jpg    Lippo-title-custom1-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-custom2-actual.jpg    Lippo-title-custom2-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-default-actual.jpg    Lippo-title-default-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-testimonial-actual.jpg    Lippo-title-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-content-testimonial-actual.jpg    Lippo-content-testimonial-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-blog-actual.jpg    Lippo-title-blog-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-header-actual-mobile.jpg    Lippo-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-banner-actual-mobile.jpg    Lippo-banner-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-service-actual-mobile.jpg    Lippo-title-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-content-service-actual-mobile.jpg    Lippo-content-service-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-custom1-actual-mobile.jpg    Lippo-title-custom1-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-custom2-actual-mobile.jpg    Lippo-title-custom2-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-default-actual-mobile.jpg    Lippo-title-default-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-testimonial-actual-mobile.jpg    Lippo-title-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-content-testimonial-actual-mobile.jpg    Lippo-content-testimonial-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Lippo-title-blog-actual-mobile.jpg    Lippo-title-blog-expected-mobile.jpg

Power theme
    Publish theme Giao diện mặc định theo config: Power_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Power-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Power-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Power-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Power-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    Power-header-actual.jpg    Power-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Power-banner-actual.jpg    Power-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Power-header-actual-mobile.jpg    Power-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Power-banner-actual-mobile.jpg    Power-banner-expected-mobile.jpg

Bimart theme
    Publish theme Giao diện mặc định theo config: Bimart_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Bimart-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Bimart-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Bimart-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Bimart-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    Bimart-header-actual.jpg    Bimart-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Bimart-banner-actual.jpg    Bimart-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Bimart-header-actual-mobile.jpg    Bimart-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Bimart-banner-actual-mobile.jpg    Bimart-banner-expected-mobile.jpg

Simpleaf theme
    Publish theme Giao diện mặc định theo config: Simpleaf_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Simpleaf-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Simpleaf-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Simpleaf-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Simpleaf-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    Simpleaf-header-actual.jpg    Simpleaf-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Simpleaf-banner-actual.jpg    Simpleaf-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Simpleaf-header-actual-mobile.jpg    Simpleaf-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Simpleaf-banner-actual-mobile.jpg    Simpleaf-banner-expected-mobile.jpg

Furnic theme
    Publish theme Giao diện mặc định theo config: Furnic_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Furnic-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Furnic-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Furnic-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Furnic-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    Furnic-header-actual.jpg    Furnic-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Furnic-banner-actual.jpg    Furnic-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Furnic-header-actual-mobile.jpg    Furnic-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Furnic-banner-actual-mobile.jpg    Furnic-banner-expected-mobile.jpg

HerSHOP theme
    Publish theme Giao diện mặc định theo config: HerSHOP_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên HerSHOP-header-actual.jpg
    Chụp ảnh ${main-banner} với tên HerSHOP-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên HerSHOP-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên HerSHOP-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    HerSHOP-header-actual.jpg    HerSHOP-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    HerSHOP-banner-actual.jpg    HerSHOP-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    HerSHOP-header-actual-mobile.jpg    HerSHOP-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    HerSHOP-banner-actual-mobile.jpg    HerSHOP-banner-expected-mobile.jpg

Morning theme
    Publish theme Giao diện mặc định theo config: Morning_config.json
    Truy cập StoreFront
    Chụp ảnh ${header} với tên Morning-header-actual.jpg
    Chụp ảnh ${main-banner} với tên Morning-banner-actual.jpg
    Close Browser
    # Mobile
    Truy cập StoreFront ở màn hình Mobile
    Chụp ảnh ${header} với tên Morning-header-actual-mobile.jpg
    Chụp ảnh ${main-banner} với tên Morning-banner-actual-mobile.jpg
    Close Browser
    Run Keyword And Continue On Failure    Compare Two Image    Morning-header-actual.jpg    Morning-header-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Morning-banner-actual.jpg    Morning-banner-expected.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Morning-header-actual-mobile.jpg    Morning-header-expected-mobile.jpg
    Run Keyword And Continue On Failure    Compare Two Image    Morning-banner-actual-mobile.jpg    Morning-banner-expected-mobile.jpg
