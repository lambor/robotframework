*** Settings ***
Suite Setup       Before Test
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/API/API-MYK/api_onboarding.robot

*** Test cases ***
Kiểm tra gian hàng Mykiot chưa được khởi tạo
    [Tags]    all
    Xóa gian hàng mykiot thành công
    Truy cập màn hình Quản lý KiotViet của gian hàng ${onboarding_retailer_name} user=${onboarding_email} password=${onboarding_password}
    Kiểm tra MHQL Kiotviet chứa button Kích hoạt Mykiot

Kiểm tra gian hàng Mykiot khởi tạo thành công
    [Tags]    all
    # Tạo gian hàng mykiot thành công
    Truy cập màn hình Quản lý KiotViet của gian hàng ${onboarding_retailer_name} user=${onboarding_email} password=${onboarding_password}
    Kiểm tra MHQL Kiotviet chứa button dẫn sang trang quản trị/web Mykiot
