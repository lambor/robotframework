*** Settings ***
Suite Setup       Before Test
Suite Teardown    After Test
Test Teardown
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/Thiết lập Section.robot
Resource          ../../Actions/PageBuilder/ComponentAdvance.robot
Library           BuiltIn

*** Test Cases ***
Lưu 1 Section thành công vào thư viện
    Given Lưu Section Banner với tên "Section ABC"
    When Gian hàng "Testautomykiot" đăng nhập thành công
    Then Lưu thành công "Section ABC" vào thư viện Your Section

Sử dụng 1 Section đã lưu trong thư viện
    Given Đang có "Section ABC" trong thư viện Your section
    When Thêm "Section ABC" từ thư viện Your Section vào màn hình PageBuilder
    Then Thêm mới "Section ABC" thành công vào màn hình PageBuilder

Sửa thông tin 1 Section đã lưu trong thư viện
    Given Đang có "Section ABC" trong thư viện Your Section
    When Sửa tên "Section ABC" sang thành "Section XYZ"
    Then Sửa tên "Section ABC" sang thành "Section XYZ" thành công

Xóa 1 Section đã lưu trong thư viện
    Given Đang có "Section XYZ" trong thư viện Your Section
    When Xóa "Section XYZ" trong thư viện Your Section
    Then Xóa "Section XYZ" thành công
