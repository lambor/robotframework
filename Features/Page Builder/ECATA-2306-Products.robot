*** Settings ***
Suite Setup       Before Test
Test Setup
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/API/API-MYK/api_product_pb.robot
Resource          ../../Actions/PageBuilder/ProductSetting.robot
Resource          ../../Actions/API/API-MYK/api_category_pb.robot

*** Test Cases ***
Xem chi tiết 1 Sản phẩm trong danh sách Sản phẩm
    [Tags]    all    regression    hotfix
    GIVEN Có "Sản phẩm Auto Test" trong danh sách Sản phẩm của gian hàng
    # WHEN Chọn “Chỉnh sửa” của “Sản phẩm Auto Test” trong danh sách Sản phẩm
    THEN Vào màn chi tiết “Sản phẩm Auto Test” thành công

Bật/Tắt hiển thị 1 Sản phẩm trong danh sách Sản phẩm
    [Tags]    all    regression    hotfix
    GIVEN Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    WHEN Sửa "Sản phẩm Auto Test" sang trạng thái Tắt hiển thị
    THEN Trạng thái “Sản phẩm Auto Test" được Tắt hiển thị thành công

Xem 1 Sản phẩm trong danh sách Sản phẩm
    [Tags]    all    regression
    GIVEN Có "Sản phẩm Auto Test" trong danh sách Sản phẩm của gian hàng
    WHEN Chọn “Xem hiển thị” của “Sản phẩm Auto Test” trong danh sách Sản phẩm
    THEN Mở tab mới hiển thị trang chi tiết của “Sản phẩm Auto Test”

Gắn Label cho 1 Sản phẩm trong danh sách Sản phẩm
    [Tags]    all    regression
    GIVEN Có "Sản phẩm Auto Test" chưa gắn Label “New” trong danh sách Sản phẩm của gian hàng
    WHEN Chọn Label “New” của “Sản phẩm Auto Test” trong danh sách Sản phẩm
    THEN Gắn Label “New” thành công cho “Sản phẩm Auto Test”

Lọc danh sách Sản phẩm theo Trạng thái
    [Tags]    regression    all
    GIVEN Có Sản phẩm ở cả 2 trạng thái Bật hiển thị hoặc Tắt hiển thị trong danh sách Sản phẩm của gian hàng
    THEN Hiển thị các Sản phẩm ở trạng thái “Bật hiển thị” khi chọn Lọc sản phẩm theo trạng thái “Bật hiển thị” trên màn danh sách Sản phẩm
    # THEN Màn danh sách Sản phẩm chỉ hiển thị các Sản phẩm ở trạng thái “Bật hiển thị”
    API get detail product

Thao tác nhiều Sản phẩm cùng 1 lúc trong danh sách Sản phẩm
    [Tags]    all
    GIVEN Có nhiều Sản phẩm trong danh sách Sản phẩm của gian hàng chưa được bật
    WHEN Chọn nhiều Sản phẩm trong danh sách gắn Label “New”
    THEN Tất cả các Sản phẩm được chọn đều được gắn Label “New” thành công

Tìm kiếm Sản phẩm trong danh sách Sản phẩm
    [Tags]    all
    GIVEN Có "Sản phẩm Auto Test" trong danh sách Sản phẩm của gian hàng
    # WHEN Nhập nội dung tìm kiếm "SP001618" trên màn danh sách Sản phẩm
    THEN Có hiển thị “Sản phẩm Auto Test" trong danh sách kết quả tìm kiếm

Chọn số lượng Sản phẩm hiển thị trên 1 trang trong danh sách Sản phẩm
    [Tags]    all
    # GIVEN Có hơn 500 Sản phẩm trong danh sách Sản phẩm của gian hàng
    # WHEN Chọn hiển thị 10 Sản phẩm trên 1 trang
    THEN Danh sách Sản phẩm sẽ có số trang = tổng sản phẩm chia 10 + 1
