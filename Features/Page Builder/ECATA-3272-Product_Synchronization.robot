*** Settings ***
Suite Setup       Before test
Suite Teardown    After test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/PageBuilder/Product_Synchronization.robot
Resource          ../../Actions/Common.robot

*** Test Cases ***
Chọn sản phẩm đồng bộ trong User Onboarding
    GIVEN Có sản phẩm SP000001 trong danh sách sản phẩm trong màn hình đồng bộ ở luồng Onboarding
    WHEN Tích chọn sản phẩm SP000001 trong danh sách trong màn hình đồng bộ ở luồng Onboarding
    THEN Đồng bộ sản phẩm SP000001 thành công từ KV lên MyKiot
    [Teardown]    Xóa gian hàng mykiot thành công

Chọn sản phẩm để đồng bộ trong Synchronization Center
    Given Có sản phẩm DC028 trong danh sách sản phẩm trong màn hình Thiết lập đồng bộ sản phẩm ở Trung tâm đồng bộ
    WHEN Tích chọn sản phẩm DC028 trong danh sách trong màn hình Thiết lập đồng bộ sản phẩm ở Trung tâm đồng bộ
    THEN Đồng bộ sản phẩm DC028 thành công lên MyKiot
