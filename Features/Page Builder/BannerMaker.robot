*** Settings ***
Suite Setup       Before Test
Test Teardown     After test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/PageBuilder/BannerMaker.robot
Resource          ../../Locators/PageBuilder/BannerMaker.robot

*** Test Cases ***
Xuất ảnh vào trong Media Manager từ Banner Maker Pro
    [Tags]    regression    all    hotfix
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Kiểm tra tính năng Rotate của plug-in
    Có thể xuất ảnh vào trong Media Manager từ Banner Maker Pro

Có thể tải ảnh xuống Computer
    [Tags]    regression    all    hotfix
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Tải ảnh về máy

Kiểm tra tính năng Rotate của plug-in BannerMaker
    [Tags]    regression    all    hotfix
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Sleep    10s
    Kiểm tra tính năng Rotate của plug-in

Kiểm tra chức năng xóa của plug-in
    [Tags]    regression    all
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Sleep    10s
    thêm sticker vào banner
    Kiểm tra chức năng xóa của plug-in

Kiểm tra chức năng xóa tất cả của plug-in
    [Tags]    all
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Sleep    10s
    thêm nhiều sticker vào banner
    Kiểm tra chức năng xóa tất cả của plug-in

Kiểm tra chức năng add text của plug-in
    [Tags]    all
    Given Người dùng truy cập màn hình "Tiện Ích"
    When Mở tiện ích Banner Maker
    Then Tải ảnh lên Banner Maker thành công
    Sleep    10s
    Kiểm tra chức năng add text của plug-in
