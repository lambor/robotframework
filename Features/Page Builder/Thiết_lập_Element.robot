*** Settings ***
Suite Setup       Before Test
Test Teardown     After test    # After test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/Thiết lập Element.robot
Library           StringFormat
Library           String

*** Test Cases ***
1. Thêm mới 1 Element thành công
    Thiết lập Element.Có Section trống trong phần Body ở màn hình Page Builder
    Thiết lập Element.Chọn button “+” trong Section bên phần Preview
    Chọn Element Heading trong danh sách Element
    Thêm mới Element Image thành công ở trong Section

2. Di chuyển 1 Element lên trên 1 Thành phàn khác trong Section thành công
    GIVEN Có Element Text ở dưới Element Heading trong cùng 1 Column của Section
    WHEN Chọn button “Move Up” trong More Actions của Menu ở Element Text bên phần Preview
    THEN Di chuyển Element Text lên trên Element Heading thành công

3. Di chuyển 1 Element xuống dưới 1 Thành phàn khác trong Section thành công
    GIVEN Có Element Text ở dưới Element Heading trong cùng 1 Column của Section
    WHEN Chọn button “Move Down” trong More Actions của Menu ở Element Heading bên phần Preview
    THEN Di chuyển Element Heading xuống dưới Element Text thành công

4. Ẩn hiển thị 1 Element thành công
    GIVEN Có Element Image đang hiển thị trong Section
    WHEN Chọn button “Hidden Element” trong More Actions của Menu ở Element Image bên phần Preview
    THEN Ẩn hiển thị Element Image thành công

5. Nhân đôi 1 Element thành công
    GIVEN Có Element Image đang hiển thị trong Section
    WHEN Chọn button “Duplicate Element” trong More Actions của Menu ở Element Image bên phần Preview
    THEN Nhân đôi Element Image mới nằm ở dưới Element Image gốc thành công

6. Xóa 1 Element thành công
    GIVEN Có Element Image trong Section
    WHEN Chọn button “Delete Element” trong More Actions của Menu ở Element Image bên phần Preview
    THEN Xóa Element Image thành công

Thêm mới nhiều Sections thành công
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    Then Người dùng truy cập màn hình "Page Builder"
    WHEN Thêm mới nhiều Sections thành công ở dưới Section Banner
