*** Settings ***
Suite Setup       Before Test
Suite Teardown    After test
Test Setup
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/PageBuilder/Category.robot
Resource          ../../Actions/Common.robot

*** Test Cases ***
Xem chi tiết 1 Danh mục trong danh sách Danh mục sản phẩm
    [Tags]    all    regression    hotfix
    GIVEN Có "Danh mục A" trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Chọn “Chỉnh sửa” của “Danh mục A” trong danh sách Danh mục sản phẩm
    THEN Vào màn chi tiết “Danh mục A” thành công

Bật/Tắt hiển thị 1 Danh mục trong danh sách Danh mục sản phẩm
    [Tags]    all    regression
    GIVEN Có "Danh mục A" đang “Bật hiển thị” trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Sửa "Danh mục A" sang trạng thái Tắt hiển thị
    THEN Trạng thái “Danh mục A" được Tắt hiển thị thành công
    [Teardown]    Bật hiển thị danh mục

Xem 1 Danh mục trong danh sách Danh mục sản phẩm
    [Tags]    all
    GIVEN Có "Danh mục A" trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Chọn “Xem hiển thị” của “Danh mục A” trong danh sách Danh mục sản phẩm
    THEN Mở tab mới hiển thị trang chi tiết của “Danh mục A”
