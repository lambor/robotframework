*** Settings ***
Suite Setup       Before Test
Suite Teardown    After test
Test Setup
Test Teardown
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/API/API-MYK/api_category_pb.robot
Resource          ../../Actions/PageBuilder/Category.robot
Resource          ../../Actions/Common.robot

*** Test Cases ***
Thiết lập thông tin Danh mục
    [Tags]    all    regression    hotfix
    GIVEN Có "Phần Lan" trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Sửa Slug từ "phan-lan" sang thành "phan-lan-moi" ở màn hình chỉnh sửa Danh mục sản phẩm
    THEN Slug của “Phần Lan" được sửa thành tên slug mới thành công

Thiết lập thông tin SEO
    [Tags]    all
    GIVEN Có "Danh mục A" trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Sửa “Nội dung Meta miêu tả” sang thành “Danh mục A giá rẻ mới” ở màn hình chỉnh sửa Danh mục sản phẩm
    THEN “Nội dung Meta miêu tả” được sửa thành “Danh mục A giá rẻ mới” thành công

Thiết lập thông tin thêm
    [Tags]    all
    GIVEN Có "Danh mục B" đang “Bật hiển thị” trong danh sách Danh mục sản phẩm của gian hàng
    When Chọn “Tắt hiển thị” cho “Danh mục B” ở màn hình chỉnh sửa Danh mục sản phẩm
    THEN “Danh mục B” được Tắt hiển thị thành công
    [Teardown]    Bật hiển thị danh mục

Thiết lập banner danh mục
    [Tags]    all
    [Setup]    Lấy ảnh sản phẩm trước khi cập nhật    Danh mục A
    GIVEN Có "Danh mục A" trong danh sách Danh mục sản phẩm của gian hàng
    WHEN Tải lên hình ảnh banner cho “Danh mục A”
    THEN “Danh mục A” được gắn hình ảnh banner thành công
