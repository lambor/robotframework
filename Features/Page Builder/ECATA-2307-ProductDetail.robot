*** Settings ***
Suite Setup       Before Test
Test Setup
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/API/API-MYK/api_product_pb.robot
Resource          ../../Actions/PageBuilder/ProductSetting.robot

*** Test Cases ***
Thiết lập thông tin Sản phẩm
    [Tags]    all    regression    hotfix
    # GIVEN Có "Sản phẩm Auto Test" trong danh sách Sản phẩm của gian hàng
    WHEN Sửa Slug từ "san-pham-auto-test-2-khong-xoa" sang thành "san-pham-auto-test-2-khong-xoa2" ở màn hình chỉnh sửa Sản phẩm
    THEN Slug của “Sản phẩm Auto Test" được sửa thành "san-pham-auto-test-2-khong-xoa2" thành công
    [Teardown]    API update product    update_value=slug    slug=san-pham-auto-test-2-khong-xoa

Thiết lập Sản phẩm thuộc tính
    [Tags]    all
    GIVEN Có Sản phẩm thuộc tính của "Sản phẩm Auto Test" đang Bật hiển thị
    WHEN Thiết lập Tắt hiển thị Sản phẩm thuộc tính của “Sản phẩm Auto Test” ở màn hình chỉnh sửa Sản phẩm
    THEN Tắt hiển thị Sản phẩm thuộc tính của “Sản phẩm Auto Test” thành công

Thiết lập bảng thông số Sản phẩm và thông tin SEO
    [Tags]    all
    GIVEN Có Sản phẩm thuộc tính của "Sản phẩm Auto Test" đang Bật hiển thị
    WHEN Thiết lập “Thông số” cho “Sản phẩm Auto Test” là “Abc”, “Chi tiết thông số” là “123” và “Nội dung Meta miêu tả” sang thành “Sản phẩm A giá rẻ” ở màn hình chỉnh sửa Sản phẩm
    THEN Thiết lập thông số và “Nội dung Meta miêu tả” cho “Sản phẩm Auto Test” thành công
    # [teardown]    API update product    update_value=cleardata
