*** Settings ***
Library    Collections
Library    RequestsLibrary
Library     OperatingSystem
Library     SeleniumLibrary
#Suite Setup    Create Session
#Library  Selenium2Library
#Library  ExtendedRequestLibrary
*** Variables ***
${base_url}  https://reqres.in/
${user}        page=3
${response}

*** Test Cases ***
Get_respone
    Create session  myssion     ${base_url}
    ${response}=    get on session   myssion     api/users?/${user}
#    log to console  ${response.status_code}
#    log to console  ${response.content}


    ${status_code} =  convert to string  ${response.status_code}
    should be equal         ${status_code}  200