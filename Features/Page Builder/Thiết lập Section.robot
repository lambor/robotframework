*** Settings ***
Suite Setup       Before Test
Test Teardown     #After test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/Thiết lập Section.robot

*** Test Cases ***
Thêm mới 1 Section thành công
    [Tags]    all    regression    hotfix
    GIVEN Có Section Service trong phần Body ở màn hình Page Builder
    WHEN Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview
    THEN Thêm mới Section thành công ở dưới Section Banner

Kéo thả element vào column
    [Tags]    all    regression
    Given Gian hàng "Testautomykiot" đăng nhập thành công
    When Người dùng truy cập màn hình "Page Builder"
    And Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview
    Then Kéo thả thành phần

Di chuyển một section thành công
    [Tags]    all    regression
    Given Có Section Banner trong phần Body ở màn hình Page Builder
    When Chọn button “Move Down” trong More Actions của Menu ở Section Banner bên phần Preview
    Then Di chuyển Section Banner xuống dưới thành công
    And Chọn button “Move Up” trong More Actions của Menu ở Section Banner bên phần Preview
    And Di chuyển Section Banner lên trên thành công
    [Teardown]    After Test

Nhân đôi 1 Section và Xóa 1 section thành công
    [Tags]    all    regression
    Given Có Section Banner trong phần Body ở màn hình Page Builder
    When Chọn button “Duplicate Section” trong More Actions của Menu ở Section Banner bên phần Preview
    Then Nhân đôi Section Banner mới nằm ở dưới Section Banner gốc thành công
    And Chọn button “Delete Section” trong More Actions của Menu ở Section Banner bên phần Preview
    And Xóa Section Banner thành công
    [Teardown]    After Test

Kiểm tra tổng 50 sections trên màn Home
    [Tags]    all    regression
    GIVEN Gian hàng "Testautomykiot" đăng nhập thành công
    THEN Người dùng truy cập màn hình "Page Builder"
    WHEN Thêm mới nhiều sections
