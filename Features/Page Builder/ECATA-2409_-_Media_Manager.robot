*** Settings ***
Suite Setup       Before Test
Suite Teardown    After test
Library           SeleniumLibrary
Library           RequestsLibrary
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/MediaManager.robot
Resource          ../../Actions/API/API-MYK/api_media_manager.robot

*** Test Cases ***
Tải 1 ảnh lên thành công
    [Tags]    all    regression    hotfix
    GIVEN Vào popup Media Manager thành công
    WHEN Tải lên mới 1 ảnh “g21” ở popup Media Manager
    THEN Ảnh “g21” được tải lên thành công và hiển thị trong danh sách ở tab Your Data của popup Media Manager
    [Teardown]    delete an image    ${id_anh_vt}

Chỉnh sửa thông tin 1 ảnh thành công
    [Tags]    all    regression
    GIVEN Có 1 ảnh “IMG_1631.jpg” với Title là “IMG_1631” ở tab Your Data của popup Media Manager
    WHEN Sửa Title của ảnh “IMG_1631.jpg” sang thành “Xyz” ở tab Your Data của popup Media Manager
    THEN Ảnh “IMG_1631.jpg” được sửa Title sang thành “Xyz” thành công
    [Teardown]    Update image title    ${image_id_dc}    IMG_1631

Bật Image Editor từ 1 ảnh thành công
    [Tags]    all    regression
    GIVEN Có ảnh “Nguyễn Thu” ở tab Your Data của popup Media Manager
    WHEN Chuyển ảnh “Nguyễn Thu” sang màn Banner Maker Pro
    THEN Ảnh “Nguyễn Thu” được chuyển sang màn Banner Maker Pro thành công để chỉnh sửa

Tìm kiếm 1 ảnh thành công
    [Tags]    all    regression
    GIVEN Có ảnh “Nguyễn Thu.jpg” ở tab Your Data của popup Media Manager
    WHEN Tìm kiếm “Nguyễn Thu” ở tab Your Data của popup Media Manager
    THEN Hiển thị kết quả có ảnh “Nguyễn Thu.jpg” ở tab Your Data của popup Media Manager

Sắp xếp theo 1 lựa chọn thành công
    [Tags]    all    regression
    GIVEN Có nhiều ảnh ở tab Your Data của popup Media Manager
    WHEN Chọn sắp xếp theo “Thời gian mới nhất” ở tab Your Data của popup Media Manager
    THEN Hiển thị kết quả có thứ tự sắp xếp các ảnh tải lên mới nhất đến cũ nhất ở tab Your Data của popup Media Manager

Sử dụng 1 ảnh tải lên thành công (chọn 1 chức năng để sử dụng bất kỳ)
    [Tags]    all
    GIVEN Có ảnh “Nguyễn Thu” ở tab Your Data của popup Media Manager
    WHEN Chọn ảnh “Nguyễn Thu” cho 1 Image Element trong Banner Component trên PB
    THEN Ảnh “Nguyễn Thu” hiển thị thành công là 1 ảnh Banner trên PB

Sử dụng 1 ảnh từ thư viện thành công (chọn 1 chức năng để sử dụng bất kỳ)
    [Tags]    all    regression
    GIVEN Có ảnh “image_1” ở tab Data Library của popup Media Manager
    WHEN Chọn ảnh “image_1” từ thư viện dữ liệu cho 1 Image Element trong Banner Component trên PB
    THEN Ảnh “image_1” từ library hiển thị thành công là 1 ảnh Banner trên PB

Xóa 1 ảnh tải lên thành công
    [Tags]    all    regression
    GIVEN Có ảnh “dearmychen” ở tab Your Data của popup Media Manager
    When Xóa ảnh “dearmychen” ở tab Your Data của popup Media Manager
    THEN Xóa ảnh “dearmychen” thành công ở tab Your Data của popup Media Manager
    [Teardown]    Thêm ảnh "dearmychen.jpg" vào Media Manager
