*** Settings ***
Suite Setup       Before Test
Test Setup
Test Teardown     After Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/SyncCenter.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../Actions/API/API-MYK/api_menu_pb.robot
Resource          ../../Actions/PageBuilder/Menu.robot
Resource          ../../Locators/PageBuilder/Menu.robot
Resource          ../../Actions/API/API-MYK/api_theme.robot

*** Test Cases ***
Thêm mới 1 Menu trong danh sách Menu và xóa Menu đó
    [Tags]    all    regression    hotfix
    [Setup]    Tạo tên menu
    Given Chưa có ${tên menu} trong danh sách menu đã lưu
    When Thêm mới "${tên menu}" vào danh sách Menu ở màn hình thiết lập Menu trong System
    Then "${tên menu}" được lưu thành công vào danh sách Menu đã lưu
    And Xóa "${tên Menu}" trong danh sách Menu đã lưu ở màn hình thiết lập Menu
    Then "${tên Menu}" được xóa thành công

Gán Menu vào vị trí trên màn Home
    [Tags]    all    regression
    GIVEN Có "Menu 1" trong danh sách Menu đã lưu
    WHEN Thiết lập "Menu 1" vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    THEN "Menu 1" được gán vào vị trí "Menu Ngang" thành công và "Menu 1" được hiển thị ngoài StoreFront

Gỡ Menu khỏi vị trí được gán trên màn Home
    [Tags]    all    regression
    # GIVEN Có "Menu 1" đang được gán ở vị trí "Menu Ngang"
    WHEN Gỡ "Menu 1" khỏi vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    THEN "Menu 1" được gỡ khỏi vị trí "Menu Ngang" thành công và "Menu 1" sẽ không hiển thị ngoài StoreFront
    AND Xóa Menu 1

Sửa tên 1 Menu trong danh sách Menu
    [Tags]    all
    Given Có "Menu A" trong danh sách Menu đã lưu
    When Sửa tên "Menu A" sang thành "Menu B" ở màn hình thiết lập Menu trong System
    Then "Menu A" được sửa tên thành "Menu B" thành công
    AND Xóa Menu B

04. Kiểm tra Data ở cột Content Tab
    [Tags]    all    regression
    GIVEN Thiết lập Menu có sẵn vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    WHEN Vào Content Tab của Menu Ngang Component
    THEN Data của Content Tab hiển thị đúng data được cấu hình của "Menu 1"

05. Kiểm tra Data ở cột StyleTab
    [Tags]    all
    GIVEN Thiết lập "Menu 1" vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    WHEN Vào Style Tab của Menu Ngang Component
    Thiết lập hiển thị kiểu menu theo config: MenuSetting.json
    THEN Thiết lập các Style cho Menu Ngang Component thành công và hiển thị đúng như đã thiết lập ở ngoài StoreFront

06. Kiểm tra Data ở cột Advance Tab
    [Tags]    all
    GIVEN Thiết lập "Menu 1" vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    WHEN Thiết lập Margin là "10,10,10,10" và Padding là "5,5,5,5"
    #And Vào Advance Tab của Menu Ngang Component
    Then Thiết lập các Advand cho Menu Ngang Component thành công và hiển thị đúng như đã thiết lập ở ngoài StoreFront
