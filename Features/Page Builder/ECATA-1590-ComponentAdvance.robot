*** Settings ***
Suite Setup       Before Test
Suite Teardown    After Test
Test Teardown
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Actions/Common.robot
Resource          ../../Actions/Share/utils.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/Thiết lập Section.robot
Resource          ../../Actions/PageBuilder/ComponentAdvance.robot
Library           BuiltIn

*** Test Cases ***
Thêm mới 1 Component thành công
    Given Có section trống trong phần Body ở màn hình Pagebuilder
    When Chọn button “+” trong Section bên phần Preview
    And Chọn Component Banner trong danh sách Component
    Then Thêm mới Component Banner thành công ở trong Section
    [Teardown]    After Test

Di chuyển 1 Component trong Section thành công
    Given Có Component Service ở dưới Component Banner trong cùng 1 Column của Section
    When Chọn Component Service
    Then Di chuyển Component Service lên trên thành công
    And Di chuyển Component Service xuống dưới thành công
    [Teardown]    After Test

Bật/Tắt hiển thị 1 Component thành công
    Given Có Component Banner đang hiển thị trong Section
    When Chọn button “Hidden Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Then Tắt hiển thị Component Banner thành công
    And Chọn button "Show Component” trong More Actions của Menu ở Component Banner bên phần Preview
    And Bật hiển thị Component Banner thành công
    [Teardown]    After Test

Nhân đôi 1 Component thành công
    Given Có Component Banner đang hiển thị trong Section
    When Chọn button “Duplicate Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Then Nhân đôi Component Banner mới nằm ở dưới Component Banner gốc thành công
    [Teardown]    After Test

Xóa 1 Component thành công
    Given Có Component Banner đang hiển thị trong Section
    When Chọn button “Delete Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Then Xóa Component Banner thành công
    [Teardown]    After Test
