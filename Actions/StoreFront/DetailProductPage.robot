*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../Share/utils.robot
Resource          ../PageBuilder/StoreTemplate.robot
Resource          ../../Locators/StoreFront/HomePage.robot
Resource          ../../Locators/StoreFront/DetailProductPage.robot
Library           ../Share/image-comparison/ImageComparison.py
Resource          ../../Actions/Share/Computation.robot
Resource          ../../../Config/envi.robot

*** Keywords ***
Sản phẩm test hiển thị còn hàng
    ${detail-product-page-url}=    Set Variable    ${storefront_url}${productIDtesturl}
    Log    ${detail-product-page-url}
    Open Browser    ${detail-product-page-url}    gc
    Maximize Browser Window
    Reload Page
    Reload Page
    Reload Page
    Sleep    2s
    ${currentURL}=    Get Location
    Should Be Equal    ${currentURL}    ${detail-product-page-url}
    Element Should Not Be Visible    ${txt_out_of_stock}

Sản phẩm test hiển thị hết hàng
    ${detail-product-page-url}=    Set Variable    ${storefront_url}${productIDtesturl}
    Log    ${detail-product-page-url}
    Open Browser    ${detail-product-page-url}    gc
    Maximize Browser Window
    Reload Page
    Reload Page
    Reload Page
    Sleep    2s
    ${currentURL}=    Get Location
    Should Be Equal    ${currentURL}    ${detail-product-page-url}
    Element Should Be Visible    ${txt_out_of_stock}

Truy cập chi tiết sản phẩm test
    ${detail-product-page-url}=    Set Variable    ${storefront_url}${productIDtesturl}
    Log    ${detail-product-page-url}
    Open Browser    ${detail-product-page-url}    gc
    Maximize Browser Window
    Sleep    2

Sản phẩm test hiển thị thông tin trên StoreFront
    ${detail-product-page-url}=    Set Variable    ${storefront_url}${productIDtesturl}
    Log    ${detail-product-page-url}
    Open Browser    ${detail-product-page-url}    gc
    Maximize Browser Window
    Reload Page
    Element Should Be Visible    //*[contains(text(),'55,000đ')]
