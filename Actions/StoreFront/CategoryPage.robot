*** Settings ***
Test Setup        Before Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../Share/utils.robot
Resource          ../PageBuilder/StoreTemplate.robot
Resource          ../../Locators/StoreFront/HomePage.robot
Resource          ../../Locators/StoreFront/DetailProductPage.robot
Library           ../Share/image-comparison/ImageComparison.py
Resource          ../../Actions/Share/Computation.robot
Resource          ../../../Config/envi.robot

*** Keywords ***
Sản phẩm test không hiển thị trong danh mục
    ${category-page-url}=   Set Variable    ${storefront_url}${categorytesturl}
    Log    ${category-page-url}
    Open Browser    ${category-page-url}    gc
    Maximize Browser Window
    Sleep    2
    Page Should Not Contain Element    //h6[contains(text(),'Sản phẩm auto test - không xóa')]
