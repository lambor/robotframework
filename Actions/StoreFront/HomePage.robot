*** Settings ***
Test Setup        Before Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../Share/utils.robot
Resource          ../PageBuilder/StoreTemplate.robot
Resource          ../../Locators/StoreFront/HomePage.robot
Resource          ../../Locators/StoreFront/DetailProductPage.robot
Library           ../Share/image-comparison/ImageComparison.py
Resource          ../../Actions/Share/Computation.robot
Resource          ../../../Config/envi.robot

*** Keywords ***
Chụp ảnh ${element} với tên ${img_name}
    Sleep    2s
    Scroll Element Into View    ${element}
    Sleep    1s
    Capture Element Screenshot    ${element}    ${EXECDIR}${/}Actions${/}Share${/}image-comparison${/}actual${/}${img_name}
