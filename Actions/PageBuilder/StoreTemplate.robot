*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Locators/PageBuilder/TopbarMenu.robot
Resource          ../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../Locators/PageBuilder/StoreTemplate.robot
Resource          ../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Actions/Share/Computation.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/TopbarMenu.robot

*** Keywords ***
Áp dụng giao diện ${template_in_store} trong danh sách Store Templates
    # //*[contains(text(),'eMobile')]//parent::*//*[contains(@id,'apply-theme')]
    ${sub_xpath1}=    Set Variable    //*[@id='store-templates']//*[contains(text(),'
    ${sub_xpath2}=    Set Variable    ')]//parent::*//*[contains(@id,'apply-theme')]
    # ${sub_xpath2}=    Set Variable    ')]//parent::*//parent::*//a[contains(@id,'apply')]
    ${xpath}    Set Variable    ${sub_xpath1}${template_in_store}${sub_xpath2}
    Sleep    1s
    Mouse Over    ${xpath}
    Sleep    1s
    Click To Element    ${xpath}
    Sleep    3s
    # Áp dụng giao diện eMobile trong danh sách Store Templates
    #    ${xpath}=    Set Variable    //*[contains(text(),'eMobile')]//parent::*//*[contains(text(),'Áp dụng giao diện')]
    #    Mouse Over    ${xpath}
    #    Click To Element    ${xpath}
    #    Sleep    3s

Người dùng không chỉnh sửa giao diện "A" trên màn hình "Page Builder"
    Người dùng thấy nút "Save" ở trạng thái không hoạt động

Hệ thống hiển thị danh sách giao diện có sẵn trong trang "Store Templates"
    Element Should Be Visible    ${store_template}

Người dùng bấm vào nút "Áp dụng" trong giao diện "B"
    mouse over    ${theme_template}
    click to element    ${apply_theme}

Hệ thống hiển thị yêu cầu xác nhận có lưu những thay đổi không
    Element Should Be Visible    ${modal_confirm_apply}

Bấm Hủy Bỏ lưu thay đổi
    Click To Element    ${modal_confirm_apply_btn_Cancel}

Bấm Đồng Ý lưu thay đổi
    Click To Element    ${modal_confirm_apply_btn_Accept}
