*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Resource          ../../Locators/PageBuilder/TopbarMenu.robot
Resource          ../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Locators/PageBuilder/PreviewUI.robot
Resource          ../../Actions/Share/Computation.robot
Resource          ../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../Actions/Common.robot
Resource          ../../../Locators/StoreFront/HomePage.robot

*** Keywords ***
Hệ thống mở ra màn hình hiển thị giao diện "A" như ở "StoreFront" ngay tại màn hình "Page Builder"
    Sleep    5s
    ${name_preview}    get text    ${txt_name_preview}
    should be equal as strings    ${theme_name}    ${name_preview}

Người dùng tương tác ở màn hình xem trước của giao diện "A" giống như trải nghiệm ở "StoreFront"
    select frame    ${frame_preview}
    #Execute JavaScript    window.scrollTo(0,500)
    Scroll Element Into View    ${section-title-default}
    Sleep    2s
    Scroll Element Into View    //*[@class='mk-grid']
    Sleep    1s
    scroll element into view    ${link_hanghoa}
    Sleep    2s
    ${product_name}    get text    ${link_hanghoa}
    Người dùng bấm vào sản phẩm A để mở trang chi tiết sản phẩm A    ${product_name}
    click to element    ${link_home}
    Reload Page
    unselect frame
    Người dùng bấm vào biểu tượng giỏ hàng ở sản phẩm A để mở trang xem nhanh sản phẩm A    ${product_name}
    ${category_name}    get text    ${lbl_name_category}
    người dùng bấm vào danh mục B để mở trang danh sách sản phẩm của danh mục B    ${category_name}
    unselect frame
    # Bấm vào bài viết C để mở trang chi tiết bài viết C
    # unselect frame
    # Bấm vào danh mục bài viết D để mở trang danh sách bài viết của danh mục D

Người dùng đang ở "trang chi tiết sản phẩm A" của màn hình xem trước giao diện "A"
    Gian hàng "Testautomykiot" đăng nhập thành công
    #Người dùng truy cập màn hình "Page Builder"
    Người dùng thấy nút "Preview" giao diện "A" trên thanh công cụ của màn hình "Page Builder"
    Người dùng bấm vào nút "Preview"
    Hệ thống mở ra màn hình hiển thị giao diện "A" như ở "StoreFront" ngay tại màn hình "Page Builder"
    select frame    ${frame_preview}
    # Execute JavaScript    window.scrollTo(0,500)
    # ${product_name}    get text    ${link_hanghoa}
    Scroll Element Into View    ${section-title-default}
    Sleep    2s
    Scroll Element Into View    //*[@class='mk-grid']
    Sleep    1s
    scroll element into view    ${link_hanghoa}
    Sleep    2s
    ${product_name}    get text    ${link_hanghoa}
    Người dùng bấm vào sản phẩm A để mở trang chi tiết sản phẩm A    ${product_name}

Người dùng bấm vào nút "Mua ngay" ở "trang chi tiết sản phẩm A" của màn hình xem trước giao diện "A"
    click to element    ${btn_muangay}

Người dùng thấy màn hình xem trước mở ra trang "Check out"
    unselect frame
    select frame    ${frame_preview}
    element should be visible    ${tilte_checkout}

Thấy nút "Đặt hàng" ở cuối trang "Check out" ở trạng thái không thể bấm được
    scroll element into view    ${btn_dathang}
    element should be disabled    ${btn_dathang}

Người dùng nhấn tải lại trang F5 (Refressh)
    reload page

Người dùng thấy màn hình xem trước mở lại trang "Home" sau khi tải trang
    unselect frame
    Hệ thống mở ra màn hình hiển thị giao diện "A" như ở "StoreFront" ngay tại màn hình "Page Builder"

Người dùng bấm vào sản phẩm A để mở trang chi tiết sản phẩm A
    [Arguments]    ${product_name}
    # scroll element into view    ${link_hanghoa}
    click to element    ${link_hanghoa}
    page should contain    ${product_name}
    select frame    ${frame_preview}

Người dùng đang chỉnh sửa giao diện "A" ở màn hình "Page Builder"
    Gian hàng "Testautomykiot" đăng nhập thành công
    #Người dùng truy cập màn hình "Page Builder"

Người dùng bấm vào nút "Preview" để mở trang xem trước giao diện "A"
    Người dùng thấy nút "Preview" giao diện "A" trên thanh công cụ của màn hình "Page Builder"
    Người dùng bấm vào nút "Preview"

Người dùng thấy nút thoát khỏi màn hình xem trước giao diện "A" hiển thị
    element should be visible    ${btn_exitpreview}

Người dùng nhấn nút thoát khỏi màn hình xem trước giao diện "A" và tiếp tục chỉnh sửa giao diện "A" ở màn hình "Page Builder"
    click to element    ${btn_exit_preview}

Người dùng bấm vào biểu tượng giỏ hàng ở sản phẩm A để mở trang xem nhanh sản phẩm A
    [Arguments]    ${product_name}
    select frame    ${frame_preview}
    Execute JavaScript    window.scrollTo(0,500)
    scroll element into view    ${btn_quickcart}
    click to element    ${btn_quickcart}
    page should contain    ${product_name}
    select frame    ${frame_preview}
    click to element    ${btn_closequickcart}

người dùng bấm vào danh mục B để mở trang danh sách sản phẩm của danh mục B
    [Arguments]    ${category_namehh}
    click to element    ${link_danhmuchh}
    page should contain    ${category_namehh}
    select frame    ${frame_preview}
    click to element    ${link_home}

Bấm vào bài viết C để mở trang chi tiết bài viết C
    select frame    ${frame_preview}
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    #scroll element into view    ${lbl_tenbaiviet}
    ${blog_name}    get text    ${lbl_tenbaiviet}
    click to element    ${lbl_tenbaiviet}
    page should contain    ${blog_name}
    unselect frame
    select frame    ${frame_preview}
    click to element    ${link_home}

Bấm vào danh mục bài viết D để mở trang danh sách bài viết của danh mục D
    select frame    ${frame_preview}
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    #scroll element into view    ${lbl_danhmucbaiviet}
    ${category_blog}    get text    ${lbl_danhmucbaiviet}
    click to element    ${lbl_danhmucbaiviet}
    ${title_category_blog}    get text    ${lbl_title_category_blog}
    should be equal as strings    ${title_category_blog}    ${category_blog}
