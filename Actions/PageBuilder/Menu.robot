*** Settings ***
Resource          ../API/API-MYK/api_menu_pb.robot
Resource          ../../Actions/Common.robot
Library           StringFormat
Resource          ../../Locators/PageBuilder/Menu.robot
Library           SeleniumLibrary

*** Keywords ***
Chưa có ${tên menu} trong danh sách menu đã lưu
    ${resp.json()}    Get menu list
    ${listmenu_name}=    JSONLibrary.Get Value From Json    ${resp.json()}    $.data[:].name
    ${listname_length}=    get length    ${listmenu_name}
    # :FOR    ${i}    IN RANGE    ${listname_length}
    # \    ${menu_name}=    Evaluate    $listmenu_name[${i}]
    # \    log    ${menu_name}
    # \    Exit for loop if    '${menu_name}'=="${tên menu}"
    FOR    ${i}    IN RANGE    ${listname_length}
        ${menu_name}=    Evaluate    $listmenu_name[${i}]
        log    ${menu_name}
        Exit for loop if    '${menu_name}'=="${tên menu}"
    END
    should not be equal as strings    ${menu_name}    ${tên menu}

Thêm mới "${tên menu}" vào danh sách Menu ở màn hình thiết lập Menu trong System
    ${menu_id_vt}    Add new menu with menu name    ${tên menu}
    Set Global Variable    \${menu_id_vt}    ${menu_id_vt}

"${tên menu}" được lưu thành công vào danh sách Menu đã lưu
    ${resp.json()}    Get menu list
    ${listmenu_name}=    JSONLibrary.Get Value From Json    ${resp.json()}    $.data[:].name
    ${listname_length}=    get length    ${listmenu_name}
    # :FOR    ${i}    IN RANGE    ${listname_length}
    # \    ${menu_name}=    Evaluate    $listmenu_name[${i}]
    # \    log    ${menu_name}
    # \    Exit for loop if    '${menu_name}'=="${tên menu}"
    FOR    ${i}    IN RANGE    ${listname_length}
        ${menu_name}=    Evaluate    $listmenu_name[${i}]
        log    ${menu_name}
        Exit for loop if    '${menu_name}'=="${tên menu}"
    END
    should be equal as strings    ${menu_name}    ${tên menu}

Có "${tên menu}" trong danh sách Menu đã lưu
    ${id_menu1}=    Add new menu with menu name    ${tên menu}
    Log    ${id_menu1}
    Set Global Variable    \${id_menu1}    ${id_menu1}

Thiết lập "Menu 1" vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    Cập nhật vị trí menu ngang cho menu:    0    ${id_menu1}

Thiết lập Menu có sẵn vào vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    Cập nhật vị trí menu ngang cho menu:    0    ${id_horizontal_menu}

Gỡ "Menu 1" khỏi vị trí "Menu Ngang" ở màn hình thiết lập Menu trong System
    Cập nhật vị trí menu ngang cho menu:    ${id_menu1}    0

"Menu 1" được gán vào vị trí "Menu Ngang" thành công và "Menu 1" được hiển thị ngoài StoreFront
    Truy cập StoreFront
    Reload Page
    Sleep    2s
    ${menu_text}    Get Text    //*[@id='navmain-top']//*[contains(text(),'Trang chủ')]
    Log    ${menu_text}
    Should Contain    ${menu_text}    Trang chủ - auto test

"Menu 1" được gỡ khỏi vị trí "Menu Ngang" thành công và "Menu 1" sẽ không hiển thị ngoài StoreFront
    Truy cập StoreFront
    Reload Page
    Sleep    2s
    Element Should Not Be Visible    //*[@id='navmain-top']//*[contains(text(),'Trang chủ - auto test')]

Xóa "${tên menu}" trong danh sách Menu đã lưu ở màn hình thiết lập Menu
    Xóa menu    ${menu_id_vt}

Xóa Menu 1
    ${id_menu1}    Lấy ID của menu:    Menu 1
    Xóa menu    ${id_menu1}

Xóa Menu B
    ${id_menuB}    Lấy ID của menu:    Menu B
    Xóa menu    ${id_menuB}

"${tên menu}" được xóa thành công
    Chưa có ${tên menu} trong danh sách menu đã lưu

Tạo tên menu
    ${tên menu}    Generate code automatically    Menu
    Set global variable    \${tên menu}    ${tên menu}

When Sửa tên "Menu A" sang thành "${tên menu cập nhật}" ở màn hình thiết lập Menu trong System
    Sửa tên menu    ${id_menu1}    ${tên menu cập nhật}

"Menu A" được sửa tên thành "${tên menu cập nhật}" thành công
    ${resp.json()}    Get menu list
    ${json_path}    Format String    $.data[?(@.id=={0})].name    ${id_menu1}
    Log    ${json_path}
    ${menu_name}=    Get Value From Json    ${resp.json()}    ${json_path}
    ${menu_name}=    Evaluate    $menu_name[0] if $menu_name else 0
    should be equal as strings    ${menu_name}    ${tên menu cập nhật}

Vào Content Tab của Menu Ngang Component
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Click to Element    ${menu_ngang}

Vào Style Tab của Menu Ngang Component
    Gian hàng "Testautomykiot" đăng nhập thành công
    ##Người dùng truy cập màn hình "Page Builder"
    Click to element    ${menu_ngang}
    #Click to element    ${style_tab}

Vào Advance Tab của Menu Ngang Component
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Click to element    ${menu_ngang}
    Click to element    ${advance_tab}

Data của Content Tab hiển thị đúng data được cấu hình của "Menu 1"
    ${a1}    Chi tiết menu
    ${a2}    Menu ngang pagebuilder
    Should Be Equal    ${a1}    ${a2}
    Cập nhật vị trí menu ngang về ban đầu

Thiết lập Margin là "10,10,10,10" và Padding là "5,5,5,5"
    ${resp.json()} =    Publish theme Giao diện mặc định theo config: ThemeMacDinh_config.json
    ${format_data}=    Convert String to JSON    ${resp.json()}
    ${config_data_menu} =    Get Value From JSON    ${format_data}    $..children[?(@.label=="Horizontal Menu")].styles
    Return From Keyword    ${config_data_menu}

Thiết lập các Advand cho Menu Ngang Component thành công và hiển thị đúng như đã thiết lập ở ngoài StoreFront
    ${published_theme_str}=    Lấy config menu theme đang phát hành
    ${data}=    Thiết lập Margin là "10,10,10,10" và Padding là "5,5,5,5"
    Log    ${published_theme_str}
    Log    ${data}
    Should Be Equal As Strings    ${published_theme_str}    ${data}
    Truy cập StoreFront

Cập nhật vị trí menu ngang về ban đầu
    Cập nhật vị trí menu ngang cho menu:    ${id_menungang_new}    0
