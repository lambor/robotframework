*** Settings ***
Resource          Thiết lập Section.robot
Resource          ../../Locators/PageBuilder/Pagebuilder-Advance.robot
Library           StringFormat
Library           String

*** Keywords ***
Thiết lập Element.Có Section trống trong phần Body ở màn hình Page Builder
    Có Section Banner trong phần Body ở màn hình Page Builder
    Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview

Chọn button “+” trong Section bên phần Preview
    Click element    ${add-content}

Chọn Element ${element} trong danh sách Element
    ${type}    String.Format String    {{'{0}-Item-Element']}}    ${element}
    ${xpath}    Remove String    ${type}    {    }
    ${xpath2}    Set Variable    //*[@id=
    ${item_element}=    Set Variable    ${xpath2}${xpath}
    Click element    ${item_element}

Thêm mới Element Image thành công ở trong Section
    #Execute JavaScript    document.querySelector('[class="component-render element-main element-type-heading"]').click()
    Click element    //*[@data-section-index='4']
    Page Should Contain Element    //*[@data-section-index='4']//*[@class='layout-render flex-column row']//*[@class='col first-child-component last-child-component']

Có Element Text ở dưới Element Heading trong cùng 1 Column của Section
    Thiết lập Element.Có Section trống trong phần Body ở màn hình Page Builder
    Thiết lập Element.Chọn button “+” trong Section bên phần Preview
    Chọn Element Heading trong danh sách Element
    Thêm mới 1 element khác vào section

Thêm mới 1 element khác vào section
    Execute JavaScript    ${active-section-4}
    Click element    ${add_element}
    Chọn Element Button trong danh sách Element

Kiểm tra thứ tự element
    Wait Until Element Is Visible    //p[text()='Heading']//ancestor::div[@class='col first-child-component']

Chọn button “Move Down” trong More Actions của Menu ở Element Heading bên phần Preview
    Execute JavaScript    ${active-section-4}
    Execute JavaScript    document.querySelector('[class="component-render element-main element-type-heading"]').click()
    Click element    //*[@class='component-render element-main element-type-heading active']//*[@class='fas fa-bars']

Di chuyển Element Text lên trên Element Heading thành công
    Click element    ${element-move-up}
    Wait Until Element Is Visible    //*[@class='component-render element-main element-type-heading']//ancestor::div[@class='col last-child-component']

Di chuyển Element Heading xuống dưới Element Text thành công
    Click element    //*[@class='dropdown-item-move-down-element']
    Wait Until Element Is Visible    //*[@class='component-render element-main element-type-button']//ancestor::div[@class='col first-child-component']

Chọn button “Move Up” trong More Actions của Menu ở Element Text bên phần Preview
    Execute JavaScript    ${active-section-4}
    Execute JavaScript    document.querySelector('[class="component-render element-main element-type-button"]').click()
    Click element    //*[@class='component-render element-main element-type-button active']//*[@class='fas fa-bars']

Có Element Image đang hiển thị trong Section
    Thiết lập Element.Có Section trống trong phần Body ở màn hình Page Builder
    Thiết lập Element.Chọn button “+” trong Section bên phần Preview
    Chọn Element Image trong danh sách Element

Chọn button “Hidden Element” trong More Actions của Menu ở Element Image bên phần Preview
    Execute JavaScript    ${active-section-4}
    Click element    //*[@data-section-index='4']//*[@class='component-render element-main element-type-image']
    Click element    //*[@class='popover-actions popover-actions-element']

Ẩn hiển thị Element Image thành công
    Click element    //*[@class='dropdown-item-hidden-element']
    sleep    3s
    Execute JavaScript    ${active-section-4}
    Mouse Over    //*[@data-section-index='3']
    Wait Until Element Is Visible    //*[@class='component-render component-render-hidden element-main element-type-image']

Chọn button “Duplicate Element” trong More Actions của Menu ở Element Image bên phần Preview
    Comment    Execute JavaScript    ${active-section-4}
    Comment    Click element    //*[@data-section-index='4']//*[@class='component-render element-main element-type-image']
    Comment    Click element    ${element_action}
    Bấm vào tác vụ của element

Nhân đôi Element Image mới nằm ở dưới Element Image gốc thành công
    Click element    //*[@class='dropdown-item-duplicate-element']
    Execute JavaScript    ${active-section-4}
    Wait Until Element Is Visible    //*[@data-section-index='4']//*[@class='col last-child-component']//*[@class='component-render element-main element-type-image']

Bấm vào tác vụ của element
    Execute JavaScript    ${active-section-4}
    Click element    //*[@data-section-index='4']//*[@class='component-render element-main element-type-image']
    Click element    ${element_action}

Có Element Image trong Section
    Gian hàng "Testautomykiot" đăng nhập thành công
    Execute JavaScript    ${active-section}
    Sleep    2s
    Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview
    Thiết lập Element.Chọn button “+” trong Section bên phần Preview
    Chọn Element Image trong danh sách Element

Xóa Element Image thành công
    ${width}    ${height}    Get Element Size    ${added-section}
    ${width}    Convert To Integer    ${width}
    ${height}    Convert To Integer    ${height}
    Should Be True    ${width}    1579
    Should Be Equal As Numbers    ${height}    134

Chọn button “Delete Element” trong More Actions của Menu ở Element Image bên phần Preview
    Bấm vào tác vụ của element
    Click element    //*[@class='dropdown-item-delete-element']
    Sleep    3s
    Click button    //*[@class='mk-btn mk-btn-primary']
    Sleep    3s
    Execute JavaScript    ${active-section-4}
