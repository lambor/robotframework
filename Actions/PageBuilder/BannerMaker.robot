*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           StringFormat
Resource          ../Common.robot
Resource          ../../Actions/Share/Computation.robot
Library           Process
Resource          ../../Locators/PageBuilder/BannerMaker.robot
Resource          ../../Actions/API/API-MYK/api_media_manager.robot

*** Variables ***
${locator-bannermaker}       //*[@id='banner-maker-pro']

*** Keywords ***
Mở tiện ích Banner Maker
    Scroll Element Into View    ${locator-bannermaker}
    Click To Element    ${locator-bannermaker}

Tải ảnh lên Banner Maker thành công
    Choose File    //input[@id='input-load-image']    ${CURDIR}/TestData/test-image.jpg

Kiểm tra tính năng Rotate của plug-in
    Click To Element    //*[contains(@class,'tie-btn-rotate')]
    Click To Element    (//*[@class="tui-image-editor-button clockwise"])
    Click To Element    (//*[@class="tui-image-editor-button counterclockwise"])
    Scroll Element Into View    //*[@class="tie-rotate-range tui-image-editor-range"]
    Input Text    //*[@class="tie-rotate-range-value tui-image-editor-range-value"]    text: 50
    Get Browser Console Log Entries

thêm sticker vào banner
    Click To Element    //*[@class="banner-maker-icontop btn-icon-action" and @data-id="2"]
    Click Image   //*[@src="/_nuxt/img/Christmas Hat 04.fb9476d.svg" and @class="lazyLoad isLoaded"]
    #Click To Element    (//*[@class="close"])[2]

Kiểm tra chức năng xóa của plug-in
    Click To Element    //*[@class="tie-btn-delete tui-image-editor-item help enabled"]
    Get Browser Console Log Entries

thêm nhiều sticker vào banner
    Click To Element    //*[@class="banner-maker-icontop btn-icon-action" and @data-id="2"]
    Click Image   //*[@src="/_nuxt/img/Elf 03.249c142.svg" and @class="lazyLoad isLoaded"]
    Click Image   //*[@src="/_nuxt/img/Gift 04.122fd43.svg" and @class="lazyLoad isLoaded"]
    Scroll Element Into View    //*[@class="banner-maker-list"]
    Click Image   //*[@src="/_nuxt/img/Gingerbread 02.48e79ed.svg" and @class="lazyLoad isLoaded"]
    #Click To Element    (//*[@class="close"])[2]

Kiểm tra chức năng xóa tất cả của plug-in
    Click To Element    //*[@class="tie-btn-deleteAll tui-image-editor-item help enabled"]
    Get Browser Console Log Entries

Kiểm tra chức năng add text của plug-in
    Click To Element    //*[@tooltip-content="Văn bản" and @class="tie-btn-text tui-image-editor-item normal active"]
    Click To Element    //*[@id="dropdownFontFamily" and @class="banner-maker-font tui-image-editor-newline tui-image-editor-range-wrap text-left"]
    Scroll Element Into View    //*[@class="tie-text-range tui-image-editor-range"]
    Input Text    //*[@class="tie-text-range-value tui-image-editor-range-value"]    text: 100
    Get Browser Console Log Entries

Tải ảnh về máy
    Page Should Contain Element    //*[@id="hasImage"]
    #element should be visible    //*[@id="hasImage"]
    #${image}    Get Element Attribute    //*[@id="hasImage"]    src
    #BuiltIn.Log To Console    ${image}
    #Run Process    curl    ${CURDIR}/logo.png    ${image}
    Click element    ${btn_download}
    Click element    ${down_laptop}
    File Should Exist    ../../../../Downloads${/}test-image.jpg
    Remove Files    ../../../../Downloads${/}test-image.jpg


Có thể xuất ảnh vào trong Media Manager từ Banner Maker Pro
    Click To Element    //*[@id='btn-download-banner']
    Click To Element    //*[@id='download-option-media']
    Sleep    2s
    Click To Element    //*[@id='confirm-save-to-library']
    Sleep    3s
    ${image-id}     search image with keyword    test-image.jpg
    Should Be True     ${image-id} != 0
    delete an image    ${image-id}
