*** Settings ***
Resource          ../../Locators/PageBuilder/Pagebuilder-Advance.robot
Library           SeleniumLibrary
Library           OperatingSystem

*** Keywords ***
Thêm mới section
    Execute JavaScript    ${active-section}
    sleep    1s
    Click element    ${add-section-bottom}
    ${width}    ${height}    Get Element Size    ${added-section}
    ${width}    Convert To Integer    ${width}
    ${height}    Convert To Integer    ${height}
    Should Be True    ${width}    1579
    Should Be Equal As Numbers    ${height}    80

Kéo thả thành phần
    Click element    ${add-element}
    #Execute JavaScript    document.querySelector('[data-section-index="3" ]').click()
    Drag And Drop    ${element-from}    ${element-to}

Người dùng duplicate section thành công
    Execute JavaScript    ${active-section}
    Click Element    //div[@class='d-block']
    Click Element    //*[@class='fas fa-copy']

Thấy section vừa tạo ở dưới section trước đó
    ${style_sectione_1} =   Get Element Attribute    //section[@data-section-index='3']    style
    ${style_sectione_2} =   Get Element Attribute    //section[@data-section-index='4']    style
    ${style_sectione_1}=  Fetch From Left  ${style_sectione_1}  --section-home-
    ${style_sectione_2}=  Fetch From Left  ${style_sectione_2}  --section-home-
    Should Be Equal    ${style_sectione_1}    ${style_sectione_2}

Người dùng xóa thành công section vừa tạo
    Execute JavaScript    ${active-section-4}
    ${style_sectione} =   Get Element Attribute    //section[@data-section-index='4']    style
    Click Element    //div[@class='d-block']
    Click Element    //*[@class='fas fa-trash']
    Click Button    ${confirm-delete}
    ${style_sectione_new} =   Get Element Attribute    //section[@data-section-index='4']    style
    Should Not Be Equal    ${style_sectione}    ${style_sectione_new}

Người dùng di chuyển một section xuống dưới thành công
    Execute JavaScript    ${active-section}
    ${style_sectione} =   Get Element Attribute    //section[@data-section-index='3']    style
    Click Element    //div[@class='d-block']
    Click Element    //*[@class='fas fa-arrow-down']
    ${style_sectione_new} =   Get Element Attribute    //section[@data-section-index='4']    style
    Should Be Equal    ${style_sectione}    ${style_sectione_new}

Người dùng di chuyển một section lên trên thành công
    Execute JavaScript    ${active-section-4}
    ${style_sectione} =   Get Element Attribute    //section[@data-section-index='4']    style
    Click Element    //div[@class='d-block']
    Click Element    //*[@class='fas fa-arrow-up']
    ${style_sectione_new} =   Get Element Attribute    //section[@data-section-index='3']    style
    Should Be Equal    ${style_sectione}    ${style_sectione_new}
