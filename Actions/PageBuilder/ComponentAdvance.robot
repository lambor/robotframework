*** Settings ***
Resource          ../Common.robot
Resource          Thiết lập Section.robot
Resource          ../../Locators/PageBuilder/Pagebuilder-Advance.robot
Library           SeleniumLibrary
Library           OperatingSystem
Resource          ../Common.robot
Library           BuiltIn

*** Keywords ***
Có section trống trong phần Body ở màn hình Pagebuilder
    Gian hàng "Testautomykiot" đăng nhập thành công
    Reload Page
    Sleep    3s
    Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview

Chọn button “+” trong Section bên phần Preview
    Click Element    ${add-content-preview}

Chọn Component Banner trong danh sách Component
    Click Image    //*[@src='/_nuxt/img/Banner.7f8f195.svg']

Thêm mới Component Banner thành công ở trong Section
    Execute JavaScript    ${active-section-4}
    Element Should Be Visible    //*[@data-name='SectionWidget']//*[@class='widget-config-inner icon-draggable banner']

Thêm mới Component Baner
    Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview
    Chọn button “+” trong Section bên phần Preview
    Chọn Component Banner trong danh sách Component

Có Component Service ở dưới Component Banner trong cùng 1 Column của Section
    Gian hàng "Testautomykiot" đăng nhập thành công
    Reload Page
    Sleep    3s
    Thêm mới Component Baner
    Execute JavaScript    ${active-section-4}
    Sleep    2s
    Click Element    //*[@id='column-widget-TestimonialSection-0']//*[@class='fas fa-plus']
    Click Image    //*[@src='/_nuxt/img/Service.457bc0a.svg']

Chọn Component Banner
    Execute JavaScript    ${active-section-4}
    Click Element    ${setting-banner}

Chọn Component Service
    Execute JavaScript    ${active-section-4}
    Click Element    //*[@class='widget-config-inner icon-draggable service-box']//*[@class='fas fa-cog']

Di chuyển Component Service lên trên thành công
    ${service} =    Get Element Attribute    //*[@id='[4].children[0].children[1]']    class
    Click Element    //*[@class='col component-main last-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-arrow-up']
    ${service-new} =    Get Element Attribute    //*[@id='[4].children[0].children[1]']    class
    Should Not Be Equal    ${service}    ${service-new}

Di chuyển Component Service xuống dưới thành công
    Chọn Component Service
    ${service} =    Get Element Attribute    //*[@id='[4].children[0].children[0]']    class
    Click Element    //*[@class='col component-main first-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-arrow-down']
    ${service-new} =    Get Element Attribute    //*[@id='[4].children[0].children[0]']    class
    Should Not Be Equal    ${service}    ${service-new}

Có Component Banner đang hiển thị trong Section
    Gian hàng "Testautomykiot" đăng nhập thành công
    Reload Page
    Sleep    3s
    Thêm mới Component Baner
    Chọn Component Banner

Chọn button “Hidden Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Click Element    //*[@class='col layout-banner component-main first-child-component last-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-eye-slash']

Tắt hiển thị Component Banner thành công
    Execute JavaScript    ${active-section-4}
    Element Should Be Visible    //*[@id='column-widget-TestimonialSection-0-0']//*[@class='widget-config-name hidden-widget']

Chọn button "Show Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Chọn Component Banner
    Click Element    //*[@class='col layout-banner component-main first-child-component last-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-eye']

Bật hiển thị Component Banner thành công
    Execute JavaScript    ${active-section-4}
    Element Should Not Be Visible    //*[@id='column-widget-TestimonialSection-0-0']//*[@class='widget-config-name hidden-widget']

Chọn button “Duplicate Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Click Element    //*[@class='col layout-banner component-main first-child-component last-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-copy']

Nhân đôi Component Banner mới nằm ở dưới Component Banner gốc thành công
    Execute JavaScript    ${active-section-4}
    ${banner} =    Get Text    //*[@id='column-widget-TestimonialSection-0-0']//*[@class='widget-config-name-text']
    ${banner-dupplicate} =    Get Text    //*[@id='column-widget-TestimonialSection-0-1']//*[@class='widget-config-name-text']
    Should Be Equal    ${banner}    ${banner-dupplicate}

Chọn button “Delete Component” trong More Actions của Menu ở Component Banner bên phần Preview
    Click Element    //*[@class='col layout-banner component-main first-child-component last-child-component active']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-trash']

Xóa Component Banner thành công
    Click Element    ${confirm-delete}
    Element Should Not Be Visible    //*[@id='[4].children[0].children[0]']//*[@class='col layout-banner component-main first-child-component last-child-component']
