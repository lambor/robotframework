*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Locators/PageBuilder/TopbarMenu.robot
Resource          ../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Actions/Share/Computation.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/Mytemplate.robot

*** Variables ***
${count_element}    0
# ${count_template_A} 0    ${EMPTY}
# ${count_template_B} 0    ${EMPTY}
# ${count_unpublished_template} 0    ${EMPTY}
${unpublished_template_element_name}    ${EMPTY}
${template_name}    ${EMPTY}


*** Keywords ***
Người dùng thấy được "Save" Button trên thanh công cụ tại màn hình PageBuilder
    Element Should Be Visible    ${btn_Save_Template}

Người dùng thấy được nút "Save As" trên thanh công cụ tại màn hình "Page Builder"
    Element Should Be Visible    ${btn_Save_As_Template}

Người dùng bấm vào nút "Save As" trên thanh công cụ tại màn hình "Page Builder"
    Sleep    3s
    Click To Element    ${btn_Save_As_Template}

Given Người dùng bấm vào nút "Save As" trên thanh công cụ tại màn hình "Page Builder"
    Gian hàng "Testautomykiot" đăng nhập thành công
    #Người dùng truy cập màn hình "Page Builder"
    Người dùng bấm vào nút "Save As" trên thanh công cụ tại màn hình "Page Builder"

Giao diện "My Templates" được hiển thị dưới dạng Modal
    Element Should Be Visible    ${modal_my_template}

Người dùng bấm vào nút "Save" trên modal "My Templates"
    Sleep    3s
    Click To Element    ${modal_my_template_save}
    Sleep    3s

Giao diện "A" mới được lưu vào danh sách "My Templates"
    Click To Element    ${btn_toggle_template}
    Element Should Be Visible    ${modal_template_name_A}

Danh sách giao diện trong "My Templates" đã có 3 giao diện được lưu
    ${count_element}    Get Element Count    ${modal_list_template_items}
    Người dùng nhập D bấm vào nút "Save" trên modal "My Templates"
    # Sleep    3s
    # Click To Element    //*[@id='modal-save-success']//*[@id='modal-save-success___BV_modal_header_']//button[@class='close']
    # Sleep    3s
    Click To Element    ${btn_Save_As_Template}
    [Return]    ${count_element}

Người dùng thấy nút "Save" ở trạng thái không hoạt động
    Run Keyword If    ${count_element} == 3    Element Should Be Disabled    ${btn_Save_Template}

Người dùng bấm vào nút "Publish" trên thanh công cụ tại màn hình "Page Builder"
    Click To Element    ${btn_publish}

Bấm nút "Tiếp tục" trên modal phát hành thành công
    Click To Element    ${modal_publish_success_btn_continue}

Người dùng phát hành giao diện thành công
    Sleep    5s
    Element Should Be Visible    ${modal_publish_success}

Người dùng phát hành giao diện không thành công
    Element Should Be Visible    ${modal_publish_unsuccess}

Hệ thống hiển thị thông báo "Phát hành thành công"
    Sleep    5s
    Element Should Be Visible    ${modal_txt_publish_success_with_link}
    Click To Element    ${modal_link_publish_success}
    Sleep  3s
    # Switch Window    title:MyKiot
    Reload Page

Người dùng không bấm vào nút "Save as" để lưu giao diện vào màn hình "My Templates"
    Element Should Be Enabled    ${btn_Save_As_Template}
    Element Should Not Be Visible    ${modal_list_template_items}

Người dùng không bấm vào nút "Lưu" giao diện đang mở vào màn hình "My Templates"
    Element Should Be Visible    ${btn_Save_Template}

Người dùng không bấm vào nút "Lưu đè" giao diện đang mở vào màn hình "My Templates"
    Element Should Be Visible    ${btn_Save_As_Template}

Bấm nút Lưu trên thanh công cụ TopbarMenu
    Click To Element    ${btn_Save_Template}

Bấm nút Tiếp tục sau khi Lưu thành công
    Click To Element    ${modal-save-success-btn-continue}

Người dùng nhập ${input_template_name} bấm vào nút "Save" trên modal "My Templates"
    Sendkey To Element    ${modal_my_template_name_inp}    ${input_template_name}
    Người dùng bấm vào nút "Save" trên modal "My Templates"
    Click To Element    ${modal_btn_accept_save_success}
    Sleep    3s

Nút "Save" ở trạng thái không hoạt động trên TopbarMenu
    Element Should Be Disabled    ${btn_Save_Template}

Nút "Save" ở trạng thái hoạt động trên TopbarMenu
    Element Should Be Enabled    ${btn_Save_Template}

Người dùng thấy nút "Preview" giao diện "A" trên thanh công cụ của màn hình "Page Builder"
    ${theme_name}  Người dùng thấy tên giao diện A đang hiển thị
    element should be visible     ${btn_Preview}

Người dùng bấm vào nút "Preview"
    click to element     ${btn_Preview}

Người dùng thấy tên giao diện A đang hiển thị
    ${theme_name}  get text  ${lbl_theme_name}
    page should contain  ${theme_name}
    set global variable   \${theme_name}   ${theme_name}
    return from keyword   ${theme_name}
