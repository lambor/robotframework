*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Locators/PageBuilder/TopbarMenu.robot
Resource          ../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../Locators/PageBuilder/PageBuilder.robot
Resource          ../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Actions/Share/Computation.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../Locators/PageBuilder/PageBuilder.robot

*** Keywords ***
Người dùng thay giao diện eMobile hiển thị thành công trên màn hình "Page Builder"
    Element Should Be Visible    ${eMobile-theme-logo}

Người dùng thay giao diện missA hiển thị thành công trên màn hình "Page Builder"
    Element Should Be Visible    ${missA-theme-logo}
