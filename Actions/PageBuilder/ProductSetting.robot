*** Settings ***
Resource          ../API/API-MYK/api_product_pb.robot
Resource          ../../Actions/Common.robot
Resource          ../../Locators/PageBuilder/Products.robot
Resource          ../../Actions/Share/Computation.robot
Library           StringFormat
Library           SeleniumLibrary


*** Keywords ***
Có "Sản phẩm Auto Test" trong danh sách Sản phẩm của gian hàng
    ${productID}    API get products    getvalue=id
    Return From Keyword If    '${productID}' == 0    Fail
    Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng

Có "Sản phẩm Auto Test" chưa gắn Label “New” trong danh sách Sản phẩm của gian hàng
    ${new_tag}    API get products    getvalue=new_tag_status
    Return From Keyword If    '${new_tag}' == 1    Fail

Chọn Label “New” của “Sản phẩm Auto Test” trong danh sách Sản phẩm
    ${status}    API get products    getvalue=status    productName=Sản phẩm auto test - không xóa
    Run Keyword If    '${status}' == '0'    API set tag (hot,new,sale) and status    tag=status
    API set tag (hot,new,sale) and status    tag=new

Gắn Label “New” thành công cho “Sản phẩm Auto Test”
    ${new_tag}    API get products    getvalue=new_tag
    Return From Keyword If    '${new_tag}' == 1    Pass
    Truy cập Category auto test ở màn hình StoreFront
    Element Should Be Visible    //*[@class='product-item-label']/*[@class='new']
    [teardown]  API set tag (hot,new,sale) and status    new    ${product_test_id_myk}

Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    ${status}    API get products    getvalue=status
    Run Keyword If    '${status}' == '0'    API set tag (hot,new,sale) and status    tag=status

Sửa "Sản phẩm Auto Test" sang trạng thái Tắt hiển thị
    API set tag (hot,new,sale) and status    tag=status

Trạng thái “Sản phẩm Auto Test" được Tắt hiển thị thành công
    Truy cập Category auto test ở màn hình StoreFront
    Element Should Not Be Visible   //*[@title='Sản phẩm auto test - không xóa']

Danh sách Sản phẩm sẽ có số trang = tổng sản phẩm chia 10 + 1
    ${result}  API get products    getvalue=limit   categoryReferIds=${EMPTY}
    Return From Keyword If    '${result}' != 'true'   Fail

Chọn nhiều Sản phẩm trong danh sách gắn Label “New”
    Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    ${status}=    API get products    getvalue=status   productName=Sản phẩm auto test 2 - không xóa
    Run Keyword If    '${status}' == '0'    API set tag (hot,new,sale) and status    tag=status   id=${product_test2_id_myk}
    API set tag (hot,new,sale) for multiple products    tag=new

Tất cả các Sản phẩm được chọn đều được gắn Label “New” thành công
    ${status}   ${status1}    API get products    getvalue=multi_new_tag_on    productName=${EMPTY}
    Return From Keyword If    '${status}' == 1 and '${status1}' == 1    Pass
    # switch status of category to 1    ${cateIDtestKV}
    Truy cập Category auto test ở màn hình StoreFront
    ${count} =  Get Element Count   //*[@class='product-item-label']/*[@class='new']
    Should Be Equal As Numbers    ${count}    2

Vào màn chi tiết “Sản phẩm Auto Test” thành công
    API get detail product

Có nhiều Sản phẩm trong danh sách Sản phẩm của gian hàng chưa được bật
    ${status}   API get products    getvalue=status_new_tag
    ${status1}    API get products    getvalue=status_new_tag   productName=Sản phẩm auto test 2 - không xóa
    Run Keyword If    '${status}' == '1'    API set tag (hot,new,sale) and status    tag=new
    Run Keyword If    '${status1}' == '1'    API set tag (hot,new,sale) and status    tag=new    id=${product_test2_id_myk}

    # API set tag (hot,new,sale) and status    new    ${product_test_id_myk}
    # API set tag (hot,new,sale) and status    new    ${product_test2_id_myk}
    ${status2}   API get products    getvalue=status_new_tag
    ${status12}    API get products    getvalue=status_new_tag   productName=Sản phẩm auto test 2 - không xóa
    Should Be Equal As Strings    ${status2}    0
    Should Be Equal As Strings    ${status12}    0



Chọn “Xem hiển thị” của “Sản phẩm Auto Test” trong danh sách Sản phẩm
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Click Element    ${icon-products}
    Sleep    2s
    Click Element    //*[contains(@id,'mk-dropdown-category')]
    Sleep    5s
    Click To Element    //*[@role='menu']//*[@role='presentation']//*[contains(text(),'Category Auto Test')]
    ${locator}      Format String    //a[contains(@href,'{0}')]    ${productIDtesturl}
    Click Element    ${locator}

Có Sản phẩm ở cả 2 trạng thái Bật hiển thị hoặc Tắt hiển thị trong danh sách Sản phẩm của gian hàng
    ${status}    API get products    getvalue=status
    Run Keyword If    '${status}' == '0'    API set tag (hot,new,sale) and status    status    ${product_test_id_myk}
    ${status1}    API get products    getvalue=status    productName=Sản phẩm auto test 2 - không xóa
    Run Keyword If    '${status1}' == '1'    API set tag (hot,new,sale) and status    status    ${product_test_id_myk}

Mở tab mới hiển thị trang chi tiết của “Sản phẩm Auto Test”
    ${title}  Get Window Titles
    Should Contain    ${title}    Sản phẩm auto test - không xóa

Có hiển thị “Sản phẩm Auto Test" trong danh sách kết quả tìm kiếm
    ${id}  API get products by filters    key=${product_code}
    Should Be Equal As Numbers    ${id}    ${product_test_id_myk}


Hiển thị các Sản phẩm ở trạng thái “Bật hiển thị” khi chọn Lọc sản phẩm theo trạng thái “Bật hiển thị” trên màn danh sách Sản phẩm
    ${id}  API get products by filters    status_invisible=1
    Should Be Equal As Numbers    ${id}    ${product_test_id_myk}

Sửa Slug từ "san-pham-auto-test-2-khong-xoa" sang thành "san-pham-auto-test-2-khong-xoa2" ở màn hình chỉnh sửa Sản phẩm
    API update product    update_value=slug     slug=san-pham-auto-test-2-khong-xoa2

Slug của “Sản phẩm Auto Test" được sửa thành "san-pham-auto-test-2-khong-xoa2" thành công
    ${slug} =   API get detail product    id=${product_test2_id_myk}    getvalue=slug
    Should Be Equal As Strings    ${slug}    san-pham-auto-test-2-khong-xoa2

Có Sản phẩm thuộc tính của "Sản phẩm Auto Test" đang Bật hiển thị
    ${status} =   API get detail product    id=${product_test_id_myk}    getvalue=status
    Run Keyword If    '${status}' == '0'    API switch status    id=${product_test_id_myk}    current_status=0

Thiết lập Tắt hiển thị Sản phẩm thuộc tính của “Sản phẩm Auto Test” ở màn hình chỉnh sửa Sản phẩm
    API switch status    id=${product_test_id_myk}    current_status=1

Tắt hiển thị Sản phẩm thuộc tính của “Sản phẩm Auto Test” thành công
    ${url}    Set Variable    ${storefront_url}${productIDtesturl}
    Open Browser    ${url}    gc
    Maximize Browser Window
    ${currentURL} =   Get Location
    Should Not Contain    ${currentURL}    ${productIDtesturl}

Thiết lập “Thông số” cho “Sản phẩm Auto Test” là “Abc”, “Chi tiết thông số” là “123” và “Nội dung Meta miêu tả” sang thành “Sản phẩm A giá rẻ” ở màn hình chỉnh sửa Sản phẩm
    API update product    update_value=thongso/seo

Thiết lập thông số và “Nội dung Meta miêu tả” cho “Sản phẩm Auto Test” thành công
    ${url}    Set Variable    ${storefront_url}${productIDtesturl}
    Open Browser    ${url}    gc
    Maximize Browser Window
    Element Should Be Visible    //*[contains(@class,'product-specification')]//td[text()='Abc']
    Element Should Be Visible    //*[contains(@class,'product-specification')]//td[text()='123']
    ${description}   Get Element Attribute    //meta[@name='description']    content
    Should Be Equal As Strings    ${description}    Sản phẩm A giá rẻ


Thiết lập ON sản phẩm auto test trên StoreFront
    API set tag (hot,new,sale) and status    status    ${productId}
