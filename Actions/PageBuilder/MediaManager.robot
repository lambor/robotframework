*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           StringFormat
Library           String
Resource          ../API/API-MYK/api_media_manager.robot
Resource          ../Share/Computation.robot
Library           DateTime
Resource          SyncCenter.robot
Resource          ../../Locators/PageBuilder/Setting_PB.robot
Resource          ../../Locators/PageBuilder/PageBuilder.robot
Resource          ../../Locators/PageBuilder/Media_Manager.robot
Resource          ../API/API-MYK/api_theme.robot

*** Keywords ***
Có ảnh “${tên ảnh}” ở tab Your Data của popup Media Manager
    ${image_id_dc}    search image with keyword    ${tên ảnh}
    should not be equal as strings    ${image_id_dc}    0
    Set global variable    \${image_id_dc}    ${image_id_dc}

Xóa ảnh “${tên ảnh}” ở tab Your Data của popup Media Manager
    delete an image    ${image_id_dc}

Xóa ảnh “${tên ảnh}” thành công ở tab Your Data của popup Media Manager
    Get image detail that not exists    ${image_id_dc}
    log    Image được xóa thành công

Có 1 ảnh “${tên ảnh}” với Title là “${tên title ảnh}” ở tab Your Data của popup Media Manager
    ${image_id_dc}    search image with keyword    ${tên ảnh}
    should not be equal as strings    ${image_id_dc}    0
    Set global variable    \${image_id_dc}    ${image_id_dc}
    ${title_cu}    Get image title    ${image_id_dc}
    Should be equal as strings    ${title_cu}    ${tên title ảnh}

Sửa Title của ảnh “${tên ảnh}” sang thành “${tên mới}” ở tab Your Data của popup Media Manager
    Update image title    ${image_id_dc}    ${tên mới}

Ảnh “${tên ảnh}” được sửa Title sang thành “${tên title mới}” thành công
    ${title}    Get image title    ${image_id_dc}
    Should be equal as strings    ${title}    ${tên title mới}

Có nhiều ảnh ở tab Your Data của popup Media Manager
    ${resp.json()}    Get list image
    ${total}    Get Value From Json    ${resp.json()}    $.data.total_records
    ${total}=    Evaluate    $total[0] if $total else 0
    should be true    $total>1

Chọn sắp xếp theo “Thời gian mới nhất” ở tab Your Data của popup Media Manager
    ${resp.json()}    Get list image
    ${list_datetime}    Get Value From Json    ${resp.json()}    $.data.data[:].created_at
    Set global variable    \${list_datetime}    ${list_datetime}

Hiển thị kết quả có thứ tự sắp xếp các ảnh tải lên mới nhất đến cũ nhất ở tab Your Data của popup Media Manager
    ${length_list_datetime}=    Get length    ${list_datetime}
    FOR    ${i}    IN RANGE    ${length_list_datetime}
        ${date1}    get from list    ${list_datetime}    ${i}
        ${date1}    Convert Date    ${date1}    epoch
        ${e}    sum    ${i}    1
        Exit for loop if    $e==$length_list_datetime
        ${date2}    get from list    ${list_datetime}    ${e}
        ${date2}    Convert Date    ${date2}    epoch
        should be true    $date1>$date2 or $date1==$date2
    #Exit for loop if    $e==$length_list_datetime
    END

Tìm kiếm “${tên ảnh}” ở tab Your Data của popup Media Manager
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Click element    //a[@href="/admin/page-builder/site-settings.html"]
    Sleep    3s
    Click element    //*[@class='fa fa-upload']
    Input Text    //*[@id='search-media-yourdata']//input    ${tên ảnh}

Vào image manager
    Click Element    ${btn_thietlap}
    wait until element is visible    ${icon_thietlap}
    Click Element    ${btn_managerimage}

Hiển thị kết quả có ảnh “${tên ảnh}” ở tab Your Data của popup Media Manager
    Element Text Should Be    //*[@class="media-item"]    ${tên ảnh}

Vào popup Media Manager thành công
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    #sleep    5s
    #click element    //div[@id='modal-feedback']//button[@class='close']
    Click to element    ${logo}
    Click to element    ${btn_upload_logo}
    wait until element is visible    ${popup_mm}
    Page should contain element    ${popup_mm}

Tải lên mới 1 ảnh “${tên ảnh}” ở popup Media Manager
    Click element    ${btn_tailen}
    #Wait until element is visible    ${popup_tailen}
    #Click element    //div[@id='dropzone']
    Choose file    //input[@type='file' and @multiple='multiple']    ${EXECDIR}${/}Actions${/}Share${/}image-comparison${/}g21.jpg
    sleep    5s

Ảnh “${tên ảnh}” được tải lên thành công và hiển thị trong danh sách ở tab Your Data của popup Media Manager
    ${id_anh_vt}    search image with keyword    ${tên ảnh}
    Should not be equal as strings    ${id_anh_vt}    0
    Set global variable    \${id_anh_vt}    ${id_anh_vt}

Có ảnh “${tên ảnh}” ở tab Data Library của popup Media Manager
    ${image_id_dc}    search library image with keyword    ${tên ảnh}
    should not be equal as strings    ${image_id_dc}    0
    Set global variable    \${image_id_dc}    ${image_id_dc}

Trước khi chọn image element
    Publish theme Giao diện mặc định theo config: M.Lips_config.json

Chọn ảnh “${tên ảnh}” từ thư viện dữ liệu cho 1 Image Element trong Banner Component trên PB
    Trước khi chọn image element
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    click to element    //*[@id='template-btn']
    mouse over    //*[@id='open-2']
    click to element    //*[@id='open-2']
    sleep    3s
    #reload page
    Click to element    //*[@id='mk-banner']//img[1]
    click to element    //*[@class='fa fa-upload']
    wait until element is visible    ${popup_mm}
    Click to element    //ul[@role='tablist' and contains(@class,'nav nav-tabs flex-column')]//li[2]
    sleep    1s
    Input Text    //div[@id='search-media-library']//input    ${tên ảnh}
    ${thumbnail}    Get thumbnail from library data    ${tên ảnh}
    ${image_chosen}    Format string    //img[@src='{0}']    ${thumbnail}
    click to element    ${image_chosen}
    Click to element    //button[@class='mk-btn mk-btn-primary ml8']

Ảnh “${tên ảnh}” từ library hiển thị thành công là 1 ảnh Banner trên PB
    ${full_path}    Get full_path from library image    ${tên ảnh}
    ${src_img}    Get element attribute    //*[@id='mk-banner']//img    src
    should be equal as strings    ${full_path}    ${src_img}

Thêm ảnh "${tên ảnh}" vào Media Manager
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    #sleep    5s
    #click element    //div[@id='modal-feedback']//button[@class='close']
    Click to element    ${logo}
    Click to element    ${btn_upload_logo}
    wait until element is visible    ${popup_mm}
    Click element    ${btn_tailen}
    #Wait until element is visible    ${popup_tailen}
    #Click element    //div[@id='dropzone']
    Choose file    //input[@type='file' and @multiple='multiple']    ${EXECDIR}${/}Actions${/}Share${/}image-comparison${/}dearmychen.jpg
    sleep    5s

Chuyển ảnh “${tên ảnh}” sang màn Banner Maker Pro
    Publish theme Giao diện mặc định theo config: M.Lips_config.json
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Click element    //a[@id="cube"]
    sleep    2s
    Click element    //a[@id="banner-maker-pro"]
    sleep    2s
    Click element    //span[@class="widget-close"]//i[@class="fal fa-times"]
    click to element    //*[@id='template-btn']
    mouse over    //*[@id='open-2']
    click to element    //*[@id='open-2']
    Click element    //*[@id="mk-banner"]
    Sleep    3s
    Click element    //*[@class='fa fa-upload']
    Input Text    //*[@id='search-media-yourdata']//input    ${tên ảnh}
    Click element    //img[@alt='không sửa']
    Sleep    3s
    Click Element    //div[@class='d-flex input-group-options p-0 border-0']//i[@class='far fa-magic']
    ${handle}=    Select window    New

Ảnh “${tên ảnh}” được chuyển sang màn Banner Maker Pro thành công để chỉnh sửa
    Element Text Should Be    //p[@class="banner-maker-desc"]    ${tên ảnh}

Chọn ảnh “${tên ảnh}” cho 1 Image Element trong Banner Component trên PB
    Publish theme Giao diện mặc định theo config: M.Lips_config.json
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    click to element    //*[@id='template-btn']
    mouse over    //*[@id='open-2']
    click to element    //*[@id='open-2']
    sleep    3s
    #reload page
    Click to element    //*[@id='mk-banner']//img[1]
    Sleep    3s
    Click element    //*[@class='fa fa-upload']
    Input Text    //*[@id='search-media-yourdata']//input    ${tên ảnh}
    Click element    //img[@alt='không sửa']
    Click element    //footer[@id='modal-media-manager___BV_modal_footer_']//button[@class="mk-btn mk-btn-primary ml8"]

Ảnh “${tên ảnh}” hiển thị thành công là 1 ảnh Banner trên PB
    ${full_path}    Get full_path from your data    ${tên ảnh}
    ${src_img}    Get element attribute    //*[@id='mk-banner']//img    src
    should be equal as strings    ${full_path}    ${src_img}
