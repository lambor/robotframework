*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           StringFormat
Library           String
Resource          ../API/API-MYK/api_category_pb.robot
Resource          ../Common.robot
Resource          ../../Locators/PageBuilder/Category_PB.robot

*** Keywords ***
Sửa Slug từ "${tên danh mục}" sang thành "${tên danh mục}-moi" ở màn hình chỉnh sửa Danh mục sản phẩm
    ${slug-moi}    Generate Random String    6    [NUMBERS]abcdef
    Sửa slug của danh mục    ${slug-moi}    ${category_id_dc}
    set global variable    \${slug-moi}    ${slug-moi}

Có "${tên danh mục}" trong danh sách Danh mục sản phẩm của gian hàng
    ${category_id_dc}    Get category_id from category_name    ${tên danh mục}
    should not be equal as strings    ${category_id_dc}    0
    Set global variable    \${category_id_dc}    ${category_id_dc}

Slug của “${tên danh mục}" được sửa thành tên slug mới thành công
    ${slug}    Get slug of category    ${category_id_dc}
    ${slug_ss}    format string    phan-lan-{0}    ${slug-moi}
    should be equal as strings    ${slug_ss}    ${slug}

Sửa “Nội dung Meta miêu tả” sang thành “Danh mục A giá rẻ mới” ở màn hình chỉnh sửa Danh mục sản phẩm
    ${meta-description-moi}    Generate Random String    6    [NUMBERS]abcdef
    Sửa meta description của danh mục    ${meta-description-moi}    ${category_id_dc}
    set global variable    \${meta-description-moi}    ${meta-description-moi}

“Nội dung Meta miêu tả” được sửa thành “${meta description} mới” thành công
    ${get_meta_description}    Get meta description of category    ${category_id_dc}
    ${slug_ss}    format string    {0} {1}    ${meta description}    ${meta-description-moi}
    should be equal as strings    ${slug_ss}    ${get_meta_description}

Có "${tên danh mục}" đang “Bật hiển thị” trong danh sách Danh mục sản phẩm của gian hàng
    ${category_id_dc}    Get category_id from category_name    ${tên danh mục}
    should not be equal as strings    ${category_id_dc}    0
    ${status}    Get category status    ${category_id_dc}
    should be equal as strings    ${status}    1
    Set global variable    \${category_id_dc}    ${category_id_dc}

Chọn “Tắt hiển thị” cho “Danh mục B” ở màn hình chỉnh sửa Danh mục sản phẩm
    switch status of category to 0    ${category_id_dc}

“Danh mục B” được Tắt hiển thị thành công
    ${status}    Get category status    ${category_id_dc}
    should be equal as strings    ${status}    0

Chọn “Chỉnh sửa” của “${tên danh mục}” trong danh sách Danh mục sản phẩm
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "pagebuilder"
    Vào mục danh mục sản phẩm
    ${location_btn_sua}    Format string    ${btn_suadanhmuc}    ${tên danh mục}
    Click element    ${location_btn_sua}

Vào mục danh mục sản phẩm
    Click element    ${btn_catalog}
    wait until element is visible    ${pnl_title_hanghoa}
    #click element    //h6[contains(text(),'Danh sách sản phẩm')]
    click element    //*[@class='container-fluid']//*[@class='row align-items-center']
    wait until element is enabled    //a[@role='tab' and @aria-posinset='2']
    Click element    //a[@role='tab' and @aria-posinset='2']
    # click element    ${tab_danhmuc}
    #click element    //*[@class='container-fluid']//*[@class='row align-items-center']

Vào màn chi tiết “${tên danh mục}” thành công
    Wait until element is visible    ${lbl_tendanhmuc_chitiet}
    element text should be    ${lbl_tendanhmuc_chitiet}    ${tên danh mục}

Sửa "${tên danh mục}" sang trạng thái Tắt hiển thị
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "pagebuilder"
    Vào mục danh mục sản phẩm
    ${location_bathienthi_dm}    Format string    ${btn_bathienthi_dm}    ${tên danh mục}
    Click element    ${location_bathienthi_dm}

Trạng thái “${tên danh mục}" được Tắt hiển thị thành công
    ${location_btn_tatdanhmuc}    Format String    ${btn_tathienthi_dm}
    Sleep    3s
    page should contain element    ${location_btn_tatdanhmuc}

Bật hiển thị danh mục
    switch status of category to 1    ${category_id_dc}

Chọn “Xem hiển thị” của “${tên danh mục}” trong danh sách Danh mục sản phẩm
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "pagebuilder"
    Vào mục danh mục sản phẩm
    sleep    3s
    ${location_xemhienthi_dm}    Format string    ${btn_xemhienthi_dm}    ${tên danh mục}
    Click element    ${location_xemhienthi_dm}

Mở tab mới hiển thị trang chi tiết của “${tên danh mục}”
    ${handle}=    Select window    New
    Title should be    ${tên danh mục}

Tải lên hình ảnh banner cho “${tên danh mục}”
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "pagebuilder"
    Vào mục danh mục sản phẩm
    ${location_btn_sua}    Format string    ${btn_suadanhmuc}    ${tên danh mục}
    Click element    ${location_btn_sua}
    Vào màn chi tiết “${tên danh mục}” thành công
    Click element    ${btn_xoa_banner}
    sleep    3s
    click element    //*[@class='fa fa-upload']
    Input Text    //*[@id='search-media-yourdata']//input    Nguyễn Thu.jpg
    Click element    //img[@alt='không sửa']
    Click element    //footer[@id='modal-media-manager___BV_modal_footer_']//button[@class="mk-btn mk-btn-primary ml8"]
    #Choose file    ${btn_uploadanh_dm}    ${EXECDIR}${/}Actions${/}Share${/}image-comparison${/}d3.jpg
    wait until element is visible    ${btn_luuchitiet_dm}
    Click element    ${btn_luuchitiet_dm}

“${tên danh mục}” được gắn hình ảnh banner thành công
    Sleep    3s
    ${image}    get image of category    ${category_id_dc}
    should not be equal as strings    ${image}    0
    should be equal as strings    ${image}    ${image_cu}

Lấy ảnh sản phẩm trước khi cập nhật
    [Arguments]    ${tên danh mục}
    ${category_id}    Get category_id from category_name    ${tên danh mục}
    ${image_cu}    Get image of category    ${category_id}
    Set global variable    \${image_cu}    ${image_cu}
