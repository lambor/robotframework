*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../../Locators/PageBuilder/SyncCenter.robot
Resource          ../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../Locators/PageBuilder/StoreTemplate.robot
Resource          ../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Actions/Share/Computation.robot
Resource          ../../Actions/Common.robot
Resource          ../../Actions/PageBuilder/TopbarMenu.robot
Resource          ../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../API/API-MYK/api_core_product.robot
Resource          ../API/API-KV/api_hanghoa_kv.robot
Resource          ../StoreFront/DetailProductPage.robot
Resource          ../StoreFront/CategoryPage.robot

*** keywords ***
Bấm đồng bộ thủ công
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Click To Element    ${btn-syn-manual}

Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Click To Element    ${icon-sync-center}
    Sleep    3s

Kiểm tra "Thiết lập lựa chọn sử dụng Mô tả chi tiết" được chọn
    Element Should Be Enabled    ${desc-kv}

Thiết lập để chọn "Chi nhánh Yết Kiêu" thay cho "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    # Cập nhật thiết lập đồng bộ    ${branch_id2}    ${branch_id2}    ${sale_price_id}    ${EMPTY}    ${EMPTY}    mykiot
    Reset thiết lập đồng bộ    ${branch_id2}    ${branch_id2}    -1    0    mykiot

"Chi nhánh Yết Kiêu" được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    ${branch}=    Get Text    ${lst-branch-inventory-sync}
    Should Be Equal As Strings    ${branch}    Chi nhánh Yết Kiêu

Chọn tất cả Thu khác
    #CN_DBTK    CN_DBDH    Bang_gia_ban    Bang_gia_KM    Thu_khac    Desc
    # Cập nhật thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    0    91575    ${surchage_id1}, ${surchage_id2}    Mykiot
    Reset thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    -1    ${surchage_id1}, ${surchage_id2}    mykiot

Bỏ chọn Thu khác
    #CN_DBTK    CN_DBDH    Bang_gia_ban    Bang_gia_KM    Thu_khac    Desc
    # Cập nhật thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    0    91575    0    mykiot
    Reset thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    -1    0    mykiot

Thấy hiển thị tất cả các loại Thu khác trong danh sách thiết lập "Chọn loại Thu khác"
    sleep    2s
    Checkbox Should Be Selected    ${surchages_checkbox}

Thấy thiết lập "Chọn loại Thu khác" không có loại Thu khác nào được chọn
    Checkbox Should Not Be Selected    ${surchages_checkbox}

Bấm vào thiết lập để chọn giá trị Thu khác
    Click element    ${surchage-sync}
    Mouse Over    ${surchage-listbox}
    click element    ${surchage-option}
    # Sendkey To Element    ${surchage-sync}    Thu khác 1 (2 %)
    # Press Keys    ${surchage-sync}    ENTER

Thấy nút "Lưu" ở trạng thái hoạt động
    Element Should Be Visible    ${btn-save-config}

Bấm được vào nút "Lưu"
    Click element    ${btn-save-config}

Thấy thông báo "Lưu thành công"
    Sleep    1s
    Element should be Visible    ${toastmessage-suscess}

Thấy nút "Lưu" ở trạng thái không hoạt động
    Element Should Be Disabled    ${btn-save-config}

Lưu thiết lập "Chi nhánh trung tâm" thành công
    # Cập nhật thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    0    null    null    mykiot
    Reset thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    -1    null    mykiot

Thấy "Chi nhánh trung tâm" đã được chọn trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Set Focus To Element    ${lst-branch-inventory-sync}
    Page Should Contain Element    //*[contains(text(),'Chi nhánh trung tâm')]
    #${branchName}    Lấy tên chi nhánh qua id    ${branch_id1}
    #${name}    StringFormat.Format String    //*[contains(text(),'{0}')]    ${branchName}
    #Page Should Contain Element    ${name}

Bỏ chọn "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Click Element    //*[contains(text(),'Chọn tất cả chi nhánh')]
    Click Element    //*[contains(text(),'Chọn tất cả chi nhánh')]
    #Click Element    //*[@id='checkbox-2']//parent::div

Thấy cảnh báo "Bạn chưa chọn chi nhánh"
    wait until element is visible    ${message-error}
    Page Should Contain Element    ${message-error}

Thấy nút Lưu ở trạng thái không hoạt động
    wait until element is visible    ${btn-save-config}
    Element Should Be Visible    ${btn-save-config-disable}

Thấy nút "Chọn tất cả" ở trạng thái chưa chọn
    Checkbox Should Not Be Selected    ${all-branch}

Bấm nút "Chọn tất cả" ở thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Page Should Contain Checkbox    ${all-branch}
    Click Element    //*[contains(text(),'Chọn tất cả chi nhánh')]

Thấy hiển thị tất cả các chi nhánh trong danh sách thiết lập "Chọn chi nhánh đồng bộ tồn kho"
    Set Focus To Element    ${lst-branch-inventory-sync}
    ${list_name}    Lấy danh sách chi nhánh
    ${length_branchName}=    get length    ${list_name}
    # : FOR    ${i}    IN RANGE    ${length_branchName}
    # \    ${branchName}    get from list    ${list_name}    ${i}
    # \    ${testq}    StringFormat.Format String    //*[contains(text(),'{0}')]    ${branchName}
    # \    Page Should Contain Element    ${testq}
    # \    exit for loop if    '${i}'=='${length_branchName}'
    # : FOR    ${i}    IN RANGE    ${length_branchName}
    # \    ${branchName}    get from list    ${list_name}    ${i}
    # \    ${testq}    Format String    //*[contains(text(),'{0}')]    ${branchName}
    # \    Page Should Contain Element    ${testq}
    # \    exit for loop if    '${i}'=='${length_branchName}'
    FOR    ${i}    IN RANGE    ${length_branchName}
        ${branchName}    get from list    ${list_name}    ${i}
        ${testq}    Format String    //*[contains(text(),'{0}')]    ${branchName}
        Page Should Contain Element    ${testq}
        exit for loop if    '${i}'=='${length_branchName}'
    END

Lưu thiết lập "Chọn tất cả chi nhánh" thành công
    # Cập nhật thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    0    null    null    mykiot
    Reset thiết lập đồng bộ    ${branch_id1},${branch_id2}    ${branch_id1}    -1    null    mykiot

Thấy nút "Chọn tất cả" ở trạng thái đã chọn
    Sleep    3s
    Checkbox Should Be Selected    ${all-branch}

Thấy thiết lập "Chọn chi nhánh đồng bộ đơn hàng" có chọn "Chi nhánh trung tâm"
    Sleep    3s
    ${order-branch}=    Get Text    ${branch-orders-sync}
    Should Be Equal As Strings    ${order-branch}    Chi nhánh trung tâm

Bấm vào thiết lập để chọn "Chi nhánh Yết Kiêu" thay cho "Chi nhánh trung tâm" trong thiết lập "Chọn chi nhánh đồng bộ đơn đặt hàng"
    Sendkey To Element    ${branch-order-sync-input}    Chi nhánh Yết Kiêu
    Press Keys    ${branch-order-sync-input}    ENTER

Thấy lưu thiết lập thành công
    Reload Page
    Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Sleep    5s
    ${order-branch}=    Get Text    ${branch-orders-sync}
    Should Contain    ${order-branch}    Chi nhánh Yết Kiêu

Thấy thiết lập "Chọn bảng giá dùng làm giá bán" có chọn "${tên bảng giá}"
    wait until element is visible    ${cb-price-sync-selected}    10s
    Element text should be    ${cb-price-sync-selected}    ${tên bảng giá}

Cập nhật bảng giá bán từ "Bảng giá chung" sang "Bảng giá lẻ"
    # Cập nhật thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    ${sale_price_id}    ${promote_price_id}    ${EMPTY}    kiotviet
    Reset thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    ${sale_price_id}    ${EMPTY}    kiotviet

Thấy cập nhật thiết lập giá bán thành bảng giá "${tên bảng giá}" thành công
    reload page
    Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    Thấy thiết lập "Chọn bảng giá dùng làm giá bán" có chọn "${tên bảng giá}"

Thấy thiết lập "Chọn bảng giá dùng làm giá khuyến mại" không chọn giá trị nào
    wait until element is visible    ${cb-promotion-sync-selected}    10s
    # Element text should be    ${cb-promotion-sync-selected}    - Chọn bảng giá -
    Element Should Contain    ${cb-promotion-sync-selected}    Chọn bảng giá

Cập nhật bảng giá khuyến mại là Bảng giá "Tri ân khách hàng"
    Cập nhật thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    -1    ${promote_price_id}    ${EMPTY}    kiotviet

Thấy cập nhật thiết lập giá khuyến mại thành bảng giá "${tên bảng giá}" thành công
    reload page
    Truy cập vào màn hình "Cấu hình đồng bộ" của trang "Trung tâm đồng bộ"
    wait until element is visible    ${cb-promotion-sync-selected}    10s
    Element text should be    ${cb-promotion-sync-selected}    ${tên bảng giá}

Before test sync config
    # init test env    stagingnew
    reset thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    -1    ${EMPTY}    kiotviet

Tạo mã hàng hóa
    ${Mã sản phẩm 1}    Generate code automatically    MYKS
    Set global variable    \${Mã sản phẩm 1}    ${Mã sản phẩm 1}

Sản phẩm ${tên sản phẩm} đã đồng bộ sang mykiot
    # : FOR    ${i}    IN RANGE    10
    # \    sleep    20s
    # \    ${product_Id}    get product id through product code    ${Mã sản phẩm}
    # \    exit for loop if    '${product_Id}'!='0'
    FOR    ${i}    IN RANGE    10
        sleep    20s
        ${product_Id}    get product id through product code    ${Mã sản phẩm 1}
        exit for loop if    '${product_Id}'!='0'
        #${product_code}    ${product_name}    ${category_name}    ${stock}    ${product_baseprice}    Get product main infor from product Detail    ${product_Id}
    END
    ${product_code}    ${product_name}    ${category_name}    ${stock}    ${product_baseprice}    Get product main infor from product Detail    ${product_Id}
    should be equal as strings    ${product_code}    ${Mã sản phẩm 1}
    should be equal as strings    ${product_name}    ${tên sản phẩm}
    should be equal as strings    ${category_name}    Category Auto Test
    should be equal as numbers    ${stock}    16
    should be equal as strings    ${product_baseprice}    100000

Sản phẩm ${tên sản phẩm} không còn hiển thị ở Mykiot
    # : FOR    ${i}    IN RANGE    10
    # \    sleep    20s
    # \    ${product_Id}    get product id through product code    ${Mã sản phẩm}
    # \    exit for loop if    '${product_Id}'=='0'
    FOR    ${i}    IN RANGE    10
        sleep    20s
        ${product_Id}    get product id through product code    ${Mã sản phẩm 1}
        exit for loop if    '${product_Id}'=='0'
    END
    should be equal as strings    ${product_Id}    0

Bấm nút đồng bộ thủ công cũ
    Gian hàng "Testautomykiot" đăng nhập thành công
    #Người dùng truy cập màn hình "Page Builder"
    click to element    //*[@class='fas fa-sync']

Truy cập vào màn hình Thiết lập đồng bộ sản phẩm
    Click To Element    ${icon-sync-center}
    Sleep    3
    Click Element    //*[@class='mk-panel mk-panel-sync']
    Click Element    ${tab-sync-product}
    Sleep    3

Thiết lập đồng bộ ON cho sản phẩm test
    Đồng bộ một sản phẩm    true    ${productIDtestKV}

Trạng thái Đồng bộ của sản phẩm test ON
    ${is_sync}    Tìm kiếm sản phẩm theo product_code    ${product_code}
    Should Be Equal As Strings    ${is_sync}    1

Cập nhật tồn kho và giá của sản phẩm test
    # Cập nhật tồn kho và giá cho sản phẩm test thành    10    600000
    Run Keyword If    '${env}' != 'live'    Cập nhật tồn kho và giá cho sản phẩm test thành    10    600000
    Run Keyword If    '${env}' == 'live'    Cập nhật tồn kho và giá sản phẩm thông qua Public API thành    10    600000

Sản phẩm test hiển thị giá trên PageBuilder chính xác
    Set Selenium Speed    2
    Click Element    ${category-auto-test}
    #Scroll Element Into View    //tbody/tr[2]/td[9]
    Page Should Contain Element    //td[contains(text(),'10')]
    Page Should Contain Element    //p[contains(text(),'600,000 đ')]

Sản phẩm test hiển thị đúng giá bán ngoài StoreFront
    Truy cập chi tiết sản phẩm test
    Page Should Contain Element    //p[contains(text(),'600,000đ')]

Thiết lập đồng bộ OFF cho sản phẩm test
    Đồng bộ một sản phẩm    false    ${productIDtestKV}

Trạng thái Đồng bộ của sản phẩm test OFF
    ${is_sync}    Tìm kiếm sản phẩm theo product_code    ${product_code}
    Should Be Equal As Strings    ${is_sync}    0

Sản phẩm test không hiển thị ngoài StoreFront
    Sản phẩm test không hiển thị trong danh mục

Trạng thái Đồng bộ của sản phẩm trong Category Auto test ON/OFF
    [Arguments]    ${status}
    Click To Element    //*[contains(text(),'Category Auto Test')]
    sleep    3s
    #Scroll Element Into View    //tbody/tr[2]/td[9]
    ${is_sync}    Sản phẩm đồng bộ trong danh mục ON
    BuiltIn.Should Be Equal As Strings    ${is_sync}    ${status}

Chọn Danh mục Category Auto Test
    Click Element    ${default-category}

Danh mục Category Auto Test được chọn
    Element Should Be Visible    ${product-sync}

Thấy danh sách danh mục sản phẩm và danh sách sản phẩm trong màn hình "Thiết lập đồng bộ sản phẩm"
    Sleep    3s
    Page should contain element    ${list-product-sync}
    Page should contain element    ${list-category-sync}

Thấy thanh tìm kiếm trong "Danh sách danh mục sản phẩm"
    Page Should Contain Element    ${search-category}

Bấm vào thanh tìm kiếm nhập giá trị "Category"
    Click To Element    ${search-category}
    Input text    ${search-category}    Category

Thấy màn hình danh sách danh mục sản phẩm hiển thị danh mục sản phẩm "Category Auto Test"
    Page Should Contain Element    ${default-category}

Thấy thanh tìm kiếm trong "Danh sách sản phẩm"
    Page Should Contain Element    ${search-product}

Bấm vào thanh tìm kiếm nhập giá trị "sản phẩm auto"
    Click To Element    ${search-product}
    Input text    ${search-product}    sản phẩm auto

Thấy màn hình danh sách sản phẩm hiển thị sản phẩm "sản phẩm auto test"
    Sleep    2s
    Page Should Contain Element    ${product-sync}

Bấm vào thanh tìm kiếm nhập giá trị "SP000"
    Click To Element    ${search-product}
    Sleep    1s
    Input text    ${search-product}    ${product_code}
    # Sleep    1s
    # Input Text    ${search-product}    SP001618

Thấy màn hình danh sách sản phẩm hiển thị sản phẩm có mã sản phẩm là "SP0001"
    Sleep    1s
    Page Should Contain Element    ${product-sync}
    # Input Text    ${search-product}    SP001618
    # Sleep    3
    # Scroll Element Into View    //tbody/tr[1]/td[9]

Thấy công cụ lọc "Thương hiệu" trong "Danh sách sản phẩm"
    Element text should be    ${dropdown-trademark}    Thương hiệu

Bấm vào công cụ lọc "Thương hiệu" chọn giá trị "${tên thương hiệu}"
    Click to element    ${dropdown-trademark}
    ${trademark-dropdown--choose-item}    Format String    ${trademark-dropdown-item}    ${tên thương hiệu}
    Click to element    ${trademark-dropdown--choose-item}

Thấy màn hình hiển thị sản phẩm "X" có thương hiệu "${tên thương hiệu}"
    wait until element is visible    ${trademark-filter-result}    5s
    Element text should be    ${trademark-filter-result}    ${tên thương hiệu}

Thấy màn hình hiển thị sản phẩm "Y" có thương hiệu "${tên thương hiệu}"
    wait until element is visible    ${trademark-filter-result}    5s
    Element text should be    ${trademark-filter-result}    ${tên thương hiệu}

Chọn tiếp lọc theo thương hiệu "${tên thương hiệu}"
    ${trademark-dropdown--choose-item}    Format String    ${trademark-dropdown-item}    ${tên thương hiệu}
    Click to element    ${trademark-dropdown--choose-item}

Thấy công cụ lọc "Trạng thái đồng bộ" trong "Danh sách sản phẩm"
    Element text should be    ${dropdown-sync-status}    Trạng thái đồng bộ

Bấm vào công cụ lọc "Trạng thái đồng bộ" chọn giá trị "Đã đồng bộ"
    Click to element    ${dropdown-sync-status}
    Sleep    1s
    Click To Element    ${dropdown-sync-status-synced}

Bấm vào công cụ lọc "Trạng thái đồng bộ" chọn giá trị "Chưa đồng bộ"
    Click to element    ${dropdown-sync-status}
    Sleep    1s
    Click To Element    ${dropdown-sync-status-not-synced}

Thấy màn hình hiển thị sản phẩm "X" có trạng thái "Đã đồng bộ"
    Click To Element    ${search-product}
    Sleep    1s
    Input Text    ${search-product}    Sản phẩm auto test 2 - không xóa
    Sleep    1s
    Checkbox Should Be Selected    ${product-sync-status-synced}

Thấy màn hình hiển thị sản phẩm "Y" có trạng thái "Chưa đồng bộ"
    Click To Element    ${search-product}
    Sleep    1s
    Input Text    ${search-product}    Sản phẩm auto test - không xóa
    Sleep    1s
    Checkbox Should Not Be Selected    ${product-sync-status-not-synced}
