*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../Common.robot

*** Keywords ***
Có sản phẩm ${mã sản phẩm} trong danh sách sản phẩm trong màn hình đồng bộ ở luồng Onboarding
    open browser    ${onboarding_link}    gc
    Maximize Browser window
    wait for condition    return document.title == "Khởi tạo gian hàng"
    Click to element    //*[@class='mk-btn mk-btn-primary btn-next']
    Click to element    //*[@class='fal fa-square']
    Click to element    //*[@class='mk-btn mk-btn-primary btn-status btn-next']
    Click to element    //ul[@class='mk-selection-list']//li[1]
    Click to element    //*[@class='mk-btn mk-btn-primary btn-next']
    sleep    3s
    Scroll Element Into View    //h6[contains(text(),'M.Lips')]
    Mouse Over    //div[h6[contains(text(),'M.Lips')]]//span[1]
    Click to element    //div[h6[contains(text(),'M.Lips')]]//span[1]
    Click to element    //*[@class='mk-btn mk-btn-primary btn-next']
    wait until element is visible    //*[@class='mk-sync-setting mk-tab-content-inner']    10s
    page should contain element    //*[@class='mk-sync-setting mk-tab-content-inner']
    input type flex    //input[@id='mk-search-sync']    ${mã sản phẩm}
    sleep    3s
    ${product_input_code}    Format string    //td[contains(text(),'{0}')]    ${mã sản phẩm}
    page should contain element    ${product_input_code}

Tích chọn sản phẩm ${mã sản phẩm} trong danh sách trong màn hình đồng bộ ở luồng Onboarding
    ${product_checkbox}    Format string    //td[contains(text(),'{0}')]//preceding::td[@class='cell-selected text-center']//input    ${mã sản phẩm}
    click element js    ${product_checkbox}
    click to element    //*[@class='mk-btn mk-btn-primary btn-next']
    Wait until element is visible    //*[@id='modal-sync-data___BV_modal_header_']    5s
    click to element    //*[@class='mk-btn mk-btn-primary ml8 active disable']
    click to element    //*[@class='mk-btn mk-btn-primary mk-link-cms']

Đồng bộ sản phẩm ${mã sản phẩm} thành công từ KV lên MyKiot
    go to    ${onboarding_storefront_url}
    wait until element is visible    //div[@class='header-search hover-builder-config d-none d-xl-flex']//input    5s
    input text    //div[@class='header-search hover-builder-config d-none d-xl-flex']//input    ${mã sản phẩm}
    wait until element is visible    //*[@class='search-product-code color-text-secondary']    5s
    element text should be    //*[@class='search-product-code color-text-secondary']    ${mã sản phẩm}

Có sản phẩm ${mã sản phẩm} trong danh sách sản phẩm trong màn hình Thiết lập đồng bộ sản phẩm ở Trung tâm đồng bộ
    Gian hàng "Testautomykiot" đăng nhập thành công
    Click element    //a[@id="sync"]
    Sleep    3s
    Click element    //div[@class="mk-panel-body"]
    Click element    //a[@id="sync-config"]
    sleep    3s
    input type flex    //input[@id="mk-search-sync"]    ${mã sản phẩm}
    sleep    5s
    Element Should Contain    //table[@id="product-item-sync"]//tbody/tr[1]    ${mã sản phẩm}
    #Element Should Contain    //table[@id="product-item-sync"]//tbody/tr[1]    Hoa lan

Tích chọn sản phẩm ${mã sản phẩm} trong danh sách trong màn hình Thiết lập đồng bộ sản phẩm ở Trung tâm đồng bộ
    ${product_checkbox}    Format string    //td[contains(text(),'{0}')]//preceding::td[@class='cell-selected text-center']//input    ${mã sản phẩm}
    click element js    ${product_checkbox}
    click to element    //*[@class='fas fa-power-off']
    sleep    3s
    click element js    ${product_checkbox}
    click to element    //*[@class='fas fa-sync']

Đồng bộ sản phẩm ${mã sản phẩm} thành công lên MyKiot
    go to    ${storefront_url}
    wait until element is visible    //div[@class='header-search hover-builder-config d-none d-xl-flex']//input    5s
    input text    //div[@class='header-search hover-builder-config d-none d-xl-flex']//input    ${mã sản phẩm}
    wait until element is visible    //*[@class='search-product-code color-text-secondary']    5s
    element text should be    //*[@class='search-product-code color-text-secondary']    ${mã sản phẩm}
