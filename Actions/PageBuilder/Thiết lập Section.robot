*** Settings ***
Resource          ../../Locators/PageBuilder/Pagebuilder-Advance.robot
Resource          ../../Actions/PageBuilder/ComponentAdvance.robot
Library           SeleniumLibrary
Library           OperatingSystem
Resource          ../Common.robot
Resource          ../API/API-MYK/api_save_section.robot

*** Keywords ***
Chọn button “Add Section” ở dưới khi Active vào Section ${tên section} bên phần Preview
    Click element    ${add-section-bottom}

Chọn button “Add Section” ở dưới khi Active vào Section Banner bên phần Preview
    Execute JavaScript    ${active-section}
    Click element    ${add-section-bottom}
    Click Element    ${add-blank-section}

Kéo thả thành phần
    Click element    ${add-element}
    Click element    ${add-blank}
    #Execute JavaScript    document.querySelector('[data-section-index="3" ]').click()
    Drag And Drop    ${element-Heading}    ${element-to}

Có Section Banner trong phần Body ở màn hình Page Builder
    Gian hàng "Testautomykiot" đăng nhập thành công
    Reload Page
    Sleep    3
    Execute JavaScript    ${active-section}

Chọn button “Duplicate Section” trong More Actions của Menu ở Section Banner bên phần Preview
    Click Element    //*[@data-section-index='3']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-copy']

Nhân đôi Section Banner mới nằm ở dưới Section Banner gốc thành công
    ${style_sectione_1} =    Get Element Attribute    //section[@data-section-index='3']    style
    ${style_sectione_2} =    Get Element Attribute    //section[@data-section-index='4']    style
    ${style_sectione_1}=    Fetch From Left    ${style_sectione_1}    --section-home-
    ${style_sectione_2}=    Fetch From Left    ${style_sectione_2}    --section-home-
    Should Be Equal    ${style_sectione_1}    ${style_sectione_2}

Chọn button “Delete Section” trong More Actions của Menu ở Section Banner bên phần Preview
    Execute JavaScript    ${active-section-4}
    ${style_sectione} =    Get Element Attribute    //section[@data-section-index='4']    style
    Click Element    //*[@data-section-index='4']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-trash']

Xóa Section Banner thành công
    ${style_sectione} =    Get Element Attribute    //section[@data-section-index='4']    style
    Click Button    ${confirm-delete}
    ${style_sectione_new} =    Get Element Attribute    //section[@data-section-index='4']    style
    Should Not Be Equal    ${style_sectione}    ${style_sectione_new}

Chọn button “Move Down” trong More Actions của Menu ở Section Banner bên phần Preview
    Execute JavaScript    ${active-section}
    Click Element	    //*[@data-section-index='3']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-arrow-down']

Di chuyển Section Banner xuống dưới thành công
    ${style_sectione} =    Get Element Attribute    //section[@data-section-index='3']    style
    ${style_sectione_new} =    Get Element Attribute    //section[@data-section-index='4']    style
    Should Not Be Equal    ${style_sectione}    ${style_sectione_new}

Chọn button “Move Up” trong More Actions của Menu ở Section Banner bên phần Preview
    Execute JavaScript    ${active-section-4}
    Click Element    //*[@data-section-index='4']//*[@class='fas fa-bars']
    Click Element    //*[@class='dropdown-menu show']//*[@class='fas fa-arrow-up']

Di chuyển Section Banner lên trên thành công
    ${style_sectione} =    Get Element Attribute    //section[@data-section-index='4']    style
    ${style_sectione_new} =    Get Element Attribute    //section[@data-section-index='3']    style
    Should Not Be Equal    ${style_sectione}    ${style_sectione_new}

Có Section Service trong phần Body ở màn hình Page Builder
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    Execute JavaScript    ${active-section}
    Sleep    2s

Thêm mới Section thành công ở dưới Section ${tên section}
    ${width}    ${height}    Get Element Size    ${added-section}
    ${width}    Convert To Integer    ${width}
    ${height}    Convert To Integer    ${height}
    Should Be True    ${width}    1579
    Should Be Equal As Numbers    ${height}    100

Thêm mới nhiều Sections
    ${count-section-init}=    get element count    //section
    FOR    ${i}    IN RANGE    50 - ${count-section-init}
        Execute JavaScript    ${active-section}
        click element    ${add-section-bottom}
    END
    Execute JavaScript    ${added-section}
    Element Should Not Be Visible    ${add-section-bottom}
    ${count-section-added} =    get element count    //section    #tổng số sections sau khi add section
    should be equal as numbers    ${count-section-added}    50
    FOR    ${i}    IN RANGE    50 - ${count-section-init}
        Execute JavaScript    1
        sleep    1s
        click element    //*[@class="d-block"]
        click element    //*[@class="fas fa-trash"]
        click button    //*[@class='mk-btn mk-btn-primary']
    END
    ${count-section-deleted} =    get element count    //section
    should be equal    ${count-section-deleted}    ${count-section-init}

Lưu Section Banner với tên "Section ABC"
    Save Section: Banner.json

Lưu thành công "Section ABC" vào thư viện Your Section
    ${jsondata}=    Get Section
    ${name}=    Get Value From JSON    ${jsondata}    $.data[:1].section_name
    ${name}=    Evaluate    $name[0]
    Execute JavaScript    ${active-section}
    Click element    ${add-section-bottom}
    Click Element    ${add-template-section}
    ${section-name}=    Get Text    //*[@id='box-section-item-0']//*[@class='title fs-12 fw-medium']
    Should Be Equal    ${name}    ${section-name}    Section ABC
    Close Browser

Đang có "${section_name}" trong thư viện Your section
    ${jsondata}=    Get Section
    ${name}=    Get Value From JSON    ${jsondata}    $.data[:].section_name
    ${name}=    Evaluate    $name[0]

Thêm "Section ABC" từ thư viện Your Section vào màn hình PageBuilder
    Gian hàng "Testautomykiot" đăng nhập thành công
    Execute JavaScript    ${active-section}
    Click element    ${add-section-bottom}
    Click Element    ${add-template-section}
    Mouse Over    //*[@class='box-section-item drag-section']
    Click Element    ${add-section}

Thêm mới "Section ABC" thành công vào màn hình PageBuilder
    Page Should Contain Image    //*[@data-section-index='4']//*[@src='https://cdn.mykiot.vn/2021/12/1639365631d80c040a3fc0117f0e8161373f72d892.png']
    Page Should Contain Image    //*[@data-section-index='4']//*[@src='https://cdn.mykiot.vn/2021/12/163936562828ecaaf3fb76360a98a7e79beb323305.png']
    Page Should Contain Image    //*[@data-section-index='4']//*[@src='https://cdn.mykiot.vn/2021/12/1639365629c4f00713c1cebfa6d2d035839e2157e8.png']
    Page Should Contain Image    //*[@data-section-index='4']//*[@src='https://cdn.mykiot.vn/2021/12/16393656290199f7db98a11300182eb81be966ad01.png']
    Page Should Contain Image    //*[@data-section-index='4']//*[@src='https://cdn.mykiot.vn/2021/12/16393656303145537e906b2982b494adbc9c24850a.png']
    Close Browser

Sửa tên "Section ABC" sang thành "Section XYZ"
    ${jsondata}=    Get Section
    ${id}=    Get Value From JSON    ${jsondata}    $.data[?(@.section_name=="Section ABC")].id
    ${id}=    Evaluate    $id[0]
    Update Section    Section XYZ    ${id}

Sửa tên "Section ABC" sang thành "Section XYZ" thành công
    ${jsondata}=    Get Section
    ${name}=    Get Value From JSON    ${jsondata}    $.data[:1].section_name
    ${name}=    Evaluate    $name[0]
    Gian hàng "Testautomykiot" đăng nhập thành công
    Execute JavaScript    ${active-section}
    Click element    ${add-section-bottom}
    Click Element    ${add-template-section}
    ${section-name}=    Get Text    //*[@id='box-section-item-0']//*[@class='title fs-12 fw-medium']
    Should Be Equal    ${name}    ${section-name}    Section XYZ
    Close Browser

Xóa "Section XYZ" trong thư viện Your Section
    ${jsondata}=    Get Section
    ${id}=    Get Value From JSON    ${jsondata}    $.data[?(@.section_name=="Section XYZ")].id
    ${id}=    Evaluate    $id[0]
    Delete Section    ${id}

Xóa "Section XYZ" thành công
    Gian hàng "Testautomykiot" đăng nhập thành công
    Execute JavaScript    ${active-section}
    Click element    ${add-section-bottom}
    Click Element    ${add-template-section}
    Page Should Not Contain    //*[@id='box-section-item-0']//*[contains(text(),'Section XYZ')]
    Close Browser
