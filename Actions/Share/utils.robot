*** Settings ***
Library           SeleniumLibrary
Library           OperatingSystem
Resource          ../../Actions/API/API-MYK/api_config_sync.robot
Resource          ../../Config/envi.robot

*** Keywords ***
Before Test
    init test env    stagingnew
    init test env sync    stagingnew

After Test
    Reset thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    -1    ${EMPTY}    kiotviet
    # Thiết lập đồng bộ bật/tắt cho Category Auto Test    1
    # Cập nhật thiết lập đồng bộ    ${branch_id1}    ${branch_id1}    -1    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Close All Browsers

After each test
    Close All Browsers
