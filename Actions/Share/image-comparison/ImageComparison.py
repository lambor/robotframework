from skimage.measure import compare_ssim
from pathlib import Path
import imutils
import cv2
import os
import sys

def compare_image(img1, img2):
	dir_path = os.path.dirname(os.path.realpath(__file__))

	actual_img_name = f"{dir_path}\\actual\\{img1}"
	expected_img_name = f"{dir_path}\\expected\\{img2}"

	if not os.path.isfile(actual_img_name):
		print(f"Image not exist: {actual_img_name}")
		return 'False'

	if not os.path.isfile(expected_img_name):
		print(f"Image not exist: {expected_img_name}")
		return 'False'


	# images
	imageA = cv2.imread(actual_img_name)
	imageB = cv2.imread(expected_img_name)
	dim = (500, 500)

	imageA = cv2.resize(imageA, dim, interpolation = cv2.INTER_AREA)
	imageB = cv2.resize(imageB, dim, interpolation = cv2.INTER_AREA)
	# convert img to gray
	grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
	grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

	# Calcule the SSIM

	(score, diff) = compare_ssim(grayA, grayB, full=True)
	diff = (diff * 255).astype("uint8")
	print(img1 + " has simularity index: {}".format(score))


	# obtain the regions of the two input images that differ
	thresh = cv2.threshold(diff, 0, 255,
		cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)

	# loop over the contours
	for c in cnts:
		#draw a red rectangle around it
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
		cv2.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)

	# show imgs
	if score < 0.9 :
		# cv2.imshow("Actual Image: " + actual_img_name, imageA)
		# cv2.imshow("Expected Image: " + expected_img_name, imageB)
		# cv2.imshow("Diff: " + actual_img_name, diff)
		dir_result = f"{dir_path}\\result\\"
		cv2.imwrite(dir_result + img1 + "_" + format(score) + ".jpg", imageA)
		# cv2.imshow("Thresh", thresh)
		cv2.waitKey(0)
		#dir_actual_fail = f"{dir_path}\\actual_fail\\"
		#cv2.imwrite(dir_actual_fail + img1 + "_" + format(score) + ".jpg", img1)
		return 'False'
	else:
		print(actual_img_name + " : PASS!!!")
		return 'True'


# def main():
# 	compare_image("actual/vegan-header-actual.jpg","expected/vegan-header-expected.jpg")
# 	compare_image("actual/eMobile-header-actual.jpg","expected/eMobile-header-expected.jpg")
# 	compare_image("actual/embeau-header-actual.jpg","expected/embeau-header-expected.jpg")
#
#
#
# if __name__ == "__main__":
#     main()
