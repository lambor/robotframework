*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Resource          ../../Share/Computation.robot
Library           StringFormat
Resource          ../../Common.robot

*** Keywords ***
Get menu list
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Get Request    ali    /menus    headers=${headers1}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    Return from keyword    ${resp.json()}

Add new menu with menu name
    [Arguments]    ${tên menu}
    ${data}    Format String    {{"id":"0","name":"{0}","active_vertical":false,"active_top":false,"active_footer":false,"active_horizontal":false,"table_content":[{{"id":1,"type":"home","icon":"","name":"Trang chủ - auto test","referId":null}},{{"id":2,"type":"introduce","icon":"","name":"Giới thiệu","referId":null}},{{"id":3,"type":"contact","icon":"","name":"Liên hệ","referId":null}},{{"id":4,"type":"blog","icon":"","name":"Tất cả bài viết","referId":null}},{{"id":5,"type":"viewed","icon":"","name":"Sản phẩm đã xem","referId":null}},{{"id":6,"type":"favourites","icon":"","name":"Sản phẩm yêu thích","referId":null}}]}}    ${tên menu}
    ${headers1}=    Create Dictionary    jwttoken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Post request    ali    /menus/menu-items/update-menu-item    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    ${json}=    To Json    ${resp.content}
    ${id_json}=    Get Value From Json    ${json}    $.data.menu_root.id
    ${id} =    Evaluate    $id_json[0] if $id_json else 0
    Return From Keyword    ${id}

Lấy ID của menu:
    [Arguments]    ${menu_name}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Get Request    mysession    /menus    headers=${headers}
    Log    ${resp.json()}
    ${json}=    To Json    ${resp.content}
    # ${json_path}=    String.Format String    $.data[?(@.name=="{0}")].id    ${menu_name}
    ${json_path}    Format String    $.data[?(@.name=="{0}")].id    ${menu_name}
    Log    ${json_path}
    ${id_json}=    Get Value From Json    ${json}    ${json_path}
    ${id} =    Evaluate    $id_json[0] if $id_json else 0
    Log    ${id}
    Return From Keyword    ${id}

Cập nhật vị trí menu ngang cho menu:
    [Arguments]    ${id_old_menu}    ${id_new_menu}
    ${data}=    Create Dictionary    oldPosition[active_horizontal]=${id_old_menu}    newPosition[active_horizontal]=${id_new_menu}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Patch Request    mysession    /menus/position    headers=${headers}    data=${data}
    Log    ${resp.json()}

Xóa menu
    [Arguments]    ${menu_id}
    ${uri}    Format String    /menus/delete/{0}    ${menu_id}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Delete Request    ali    ${uri}    headers=${headers1}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    Return from keyword    ${resp.json()}

Sửa tên menu
    [Arguments]    ${menu_id}    ${menu_name}
    ${data}    Format String    {{"id":{0},"name":"{1}","table_content":[{{"id":124413,"name":"Trang chủ","refer_id":null,"icon":null,"type":"home","order":0,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"0","originName":"Trang chủ"}},{{"id":124414,"name":"Giới thiệu","refer_id":null,"icon":null,"type":"introduce","order":1,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"1","originName":"Giới thiệu"}},{{"id":124415,"name":"Liên hệ","refer_id":null,"icon":null,"type":"contact","order":2,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"2","originName":"Liên hệ"}},{{"id":124416,"name":"Tất cả bài viết","refer_id":null,"icon":null,"type":"blog","order":3,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"3","originName":"Tất cả bài viết"}},{{"id":124417,"name":"Sản phẩm đã xem","refer_id":null,"icon":null,"type":"viewed","order":4,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"4","originName":"Sản phẩm đã xem"}},{{"id":124418,"name":"Sản phẩm yêu thích","refer_id":null,"icon":null,"type":"favourites","order":5,"parent_id":124412,"active_redirect":0,"increment_id":null,"origin_name":null,"slug":null,"status":1,"deleted_at":null,"children":[],"key":"5","originName":"Sản phẩm yêu thích"}}]}}    ${menu_id}    ${menu_name}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Post request    ali    /menus/menu-items/update-menu-item    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}

Chi tiết menu
    #${headers1}=    Create Dictionary    jwttoken=eksyWTZ0S0lZbUhwRGxyQStJbjhiek0vTHI5SVRzdGZuaEFHMzN1bXRlY05LWmY4VktUSFJXQ0E4a3prRGlOTkpMeTlrUzJlQkNGNWVNVis4NUpXaUhPZ0NjOGtiQzQrOFdtNWhZRWpGQVcyUDlKQ1NFVWNheUdJanoxSm5FT0J5SjR6RWhReHNvcTAyUnhkL2Jjd2FzeXlUbk9TbTNONUwvS1d4UVBmS3JLUkx2bDROL3BvaEhTb1JFZGFtWUs5elgzRHkrSzZYUW9talZnVkRFK3E0S1FTeUZBcDBTeWF5TVJ1TWNOcVBMWTNJWTdLek9wcFFEL2VZQVNaQkpnSmFrL3BmcEU0RmJkUC9NUjIwYUtzVXlnN0doNFBTbEsyRk9YdkVrNTN4TUQ1MkIyMDFUNi9TZGFIK2dURkMwZDF6a2xoeGlXSU9HNW5YVTF0aWFGTlVQMy9JY0RWQ1llY3NUQ3ZkSzVNbEFqUjBQL3N3Z0xRRi91ZDB6QWViVjFmQ3QremJ1V3hRRUc0YnpHdUhUb0FvSW9UcEhSeDYzcDdocUhObGpmUzhjVitlU3dnY29INDVobmFRQmJXNytMQUt1M2c4MkhEN2JrNkppMDVvT0xZcEE9PQ==
    ${headers1}=    Create Dictionary    jwttoken=${jwttoken}
    ${params}    Create Dictionary    root_id=${id_menungang_new}
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Get Request    ali    /menus/menu-items/get-one    headers=${headers1}    params=${params}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    ${list_menu}    Get Value From Json    ${resp.json()}    $..name
    Return from keyword    ${list_menu}

Menu ngang pagebuilder
    ${headers} =    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    ${params}    Create Dictionary    location=horizontal    status=1
    Create Session    ali    ${api_url}    verify=True
    ${resp}=    Get Request    ali    /api/v1/menus    headers=${headers}    params=${params}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    ${pb_menu}    Get Value From Json    ${resp.json()}    $..name
    log    ${pb_menu}
    Return from keyword    ${pb_menu}

Thiết lập hiển thị kiểu menu theo config: ${json_file}
    ${file_data}=    Get Binary File    ${EXECDIR}${/}Config${/}Setting Theme${/}${json_file}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    #${headers} =    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/publish    headers=${headers}    data=${file_data}
    Log    ${resp.content}
    log    ${resp.request.body}
    ${menu}    Convert String to JSON    ${resp.request.body}
    log    ${menu}
    ${setting_arrow}    Get Value From Json    ${menu}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.arrows
    ${setting_dots}    Get Value From Json    ${menu}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.dots
    ${setting_stylemenu}    Get Value From Json    ${menu}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.menuDisplay
    Return from keyword    ${setting_arrow}    ${setting_dots}    ${setting_stylemenu}

Thiết lập các Style cho Menu Ngang Component thành công và hiển thị đúng như đã thiết lập ở ngoài StoreFront
    ${a1}    Thiết lập hiển thị kiểu menu theo config: MenuSetting.json
    ${a2}    Chi tiết theme đang phát hành
    Should Be Equal    ${a1}    ${a2}
    Truy cập StoreFront

Chi tiết theme đang phát hành
    ${headers} =    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    Create Session    ali    ${api_url}    verify=True
    ${resp}=    Get Request    ali    /api/v1/theme    headers=${headers}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}
    log    ${resp.request.body}
    ${setting_menu}    Get Value From Json    ${resp.json()}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.arrows
    ${setting_dots}    Get Value From Json    ${resp.json()}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.dots
    ${setting_stylemenu}    Get Value From Json    ${resp.json()}    $..children[?(@.label=="Horizontal Menu")].children[:]..content.menuDisplay
    Return from keyword    ${setting_menu}    ${setting_dots}    ${setting_stylemenu}

Lấy config menu theme đang phát hành
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}
    ${resp} =    Get Request    mysession    api/v1/theme    headers=${headers}
    Log    ${resp.json()}
    ${resp.json}=    Set variable    ${resp.json()}
    ${jsondata} =    Get Value From Json    ${resp.json}    $..children[?(@.label=="Horizontal Menu")].styles
    Log    ${jsondata}
    [Return]    ${jsondata}
