*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Resource          ../../Share/Computation.robot
Library           StringFormat
Resource          ../../../Actions/Common.robot


*** Keywords ***
API get products
    [Arguments]   ${getvalue}   ${productName}=Sản phẩm auto test - không xóa   ${categoryReferIds}=[${cateIDtestKV}]
    ${data}=    Create Dictionary    categoryReferIds=${categoryReferIds}   page=1    limit=10    order=newest    ignoreStatus=0    ignoreCategoryStatus=1
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/get-all    headers=${headers}    data=${data}
    Log    ${resp.json()}

    ${json}=    To Json    ${resp.content}
    # response statuses
    ${response_status}    Get Value From Json    ${json}    $.status
    ${response_status_value} =    Evaluate    $response_status[0] if $response_status else 0
    Should Not Be Equal As Strings    ${response_status_value}    fail

    # id
    ${json_path_id}    Format String    $.data.products[?(@.name=="{0}")].id    ${productName}
    ${id_json}=   Get Value From Json    ${json}    ${json_path_id}
    # new tag
    IF  '${productName}' == '${EMPTY}'
    ${json_path_new_tag}   Set Variable    $.data.products[?(@.new)].new
    ELSE
    ${json_path_new_tag}  Format String    $.data.products[?(@.name=="{0}")].new    ${productName}
    END
    ${new_tag_json}=   Get Value From Json    ${json}    ${json_path_new_tag}
    #status
    ${json_path_status}    Format String    $.data.products[?(@.name=="{0}")].status    ${productName}
    ${status_json}=   Get Value From Json    ${json}    ${json_path_status}

    #status new tag
    ${json_path_status_new_tag}    Format String    $.data.products[?(@.name=="{0}")].new    ${productName}
    ${status_new_tag_json}=   Get Value From Json    ${json}    ${json_path_status_new_tag}

    # limit
    ${total_page_json}=   Get Value From Json    ${json}    $.data.total_page
    ${total_products_json}=   Get Value From Json    ${json}    $.data.total_products

    ${id} =    Evaluate    $id_json[0] if $id_json else 0
    ${new_tag} =  Evaluate    $new_tag_json[0] if $new_tag_json else 0
    ${new_tag1} =  Run Keyword If    '${productName}' == '${EMPTY}'    Evaluate    $new_tag_json[1] if $new_tag_json else 0
    ${status} =  Evaluate    $status_json[0] if $status_json else 0
    ${total_page} =  Evaluate    $total_page_json[0] if $total_page_json else 0
    ${total_products} =  Evaluate    $total_products_json[0] if $total_products_json else 0
    ${status_new_tag} =  Evaluate    $status_new_tag_json[0] if $status_new_tag_json else 0
    Log    ${id}
    Log    ${new_tag}
    Log    ${status}
    Log    ${total_page}
    Log    ${total_products}
    ${calculation1} =  Divide    ${total_products}    10
    ${calculationAndround}    Sum    ${calculation1}    1
    ${calculation} =  Convert To Integer   ${calculationAndround}
    Return From Keyword If    '${getvalue}' == 'id'   ${id}
    Return From Keyword If    '${getvalue}' == 'status'    ${status}
    Return From Keyword If    '${getvalue}' == 'total_page'    ${total_page}
    Return From Keyword If    '${calculation}' == '${total_page}' and '${getvalue}' == 'limit'   true
    Return From Keyword If    '${getvalue}' == 'multi_new_tag_on' and '${new_tag}' == 1 and '${new_tag1}' == 1    true
    Return From Keyword If    '${getvalue}' == 'new_tag'    ${new_tag}
    Return From Keyword If    '${getvalue}' == 'status_new_tag'    ${status_new_tag}

API set tag (hot,new,sale) and status
    [Arguments]   ${tag}   ${id}=${product_test_id_myk}
    ${data}=    Create Dictionary    id=${id}    tag=${tag}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/set-tag    headers=${headers}    data=${data}
    ${json}=    To Json    ${resp.content}
    ${status} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status}    success

API set tag (hot,new,sale) for multiple products
    [Arguments]   ${tag}  ${lstproducttestIdKV}=[${productIDtestKV},${productIDtestKV2}]
    ${data}=    Create Dictionary    referIds=${lstproducttestIdKV}    tag=${tag}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/update-multiple-tag    headers=${headers}    data=${data}
    ${json}=    To Json    ${resp.content}
    ${status} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status}    success

API get detail product
    [Arguments]    ${id}=${product_test_id_myk}   ${getvalue}=${EMPTY}
    ${params}=    Create Dictionary    id=${id}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Get Request    mysession    /products/detail    headers=${headers}    params=${params}
    ${json}=    To Json    ${resp.content}
    ${status} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status}    success
    ${slug_json}   Get Value From Json    ${json}    $.data.slug
    ${slug}    Evaluate    $slug_json[0] if $slug_json else 0
    Return From Keyword If    '${getvalue}' == 'slug'    ${slug}
    ${status_json}    Get Value From Json    ${json}    $.data.status
    ${status}   Evaluate    $status_json[0] if $status_json else 2
    Return From Keyword If    '${getvalue}' == 'status'    ${status}

API get products by filters
    [Arguments]   ${key}=${EMPTY}    ${status_invisible}=${EMPTY}   ${productName}=Sản phẩm auto test - không xóa
    IF    '${status_invisible}' != '${EMPTY}'
    ${data}=    Create Dictionary    categoryReferIds=[${cateIDtestKV}]   page=1    limit=10    order=newest    ignoreStatus=0    ignoreCategoryStatus=1   status=${status_invisible}
    ELSE
    ${data}=    Create Dictionary    categoryReferIds=[${cateIDtestKV}]   page=1    limit=10    order=newest    ignoreStatus=0    ignoreCategoryStatus=1    key=${key}
    END
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/get-all    headers=${headers}    data=${data}
    ${json}=    To Json    ${resp.content}
    ${status1} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status1}    success
    ${json_path}    Format String    $.data.products[?(@.name=="{0}")].id    ${productName}
    ${id_json} =   Get Value From Json    ${json}    ${json_path}
    ${id} =  Evaluate    $id_json[0] if $id_json else 0
    Return From Keyword    ${id}

API update product
    [Arguments]   ${update_value}   ${slug}=${EMPTY}
    IF  '${update_value}' == 'slug'
    ${data}   Format String    {{"refer_id":"{0}","slug":"{1}","page_title":"Sản phẩm auto test 2 - không xóa","page_keywords":"san,pham,auto,test,2,khong,xoa","meta_description":"Sản phẩm auto test 2 - không xóa","specifications":{{"row_0":{{"":""}},"row_1":{{"":""}},"row_2":{{"":""}},"row_3":{{"":""}},"row_4":{{"":""}}}},"summary":[{{"refer_id":"{0}","summary":null}}],"description":[{{"refer_id":"{0}","description":null}}]}}    ${productIDtestKV2}   ${slug}
    ELSE IF   '${update_value}' == 'thongso/seo'
    ${data}   Format String    {{"refer_id":"{0}","slug":"san-pham-auto-test-khong-xoa","page_title":"Sản phẩm auto test - không xóa","page_keywords":"san,pham,auto,test,2,khong,xoa","meta_description":"Sản phẩm A giá rẻ","specifications":{{"row_0":{{"Abc":"123"}},"row_1":{{"":""}},"row_2":{{"":""}},"row_3":{{"":""}},"row_4":{{"":""}}}},"summary":[{{"refer_id":"{0}","summary":null}}],"description":[{{"refer_id":"{0}","description":null}}]}}   ${productIDtestKV}
    ELSE
    ${data}   Format String    {{"refer_id":"{0}","slug":"san-pham-auto-test-khong-xoa","page_title":"Sản phẩm auto test - không xóa","page_keywords":"san,pham,auto,test,2,khong,xoa","meta_description":"Sản phẩm auto test - không xóa","specifications":{{"row_0":{{"":""}},"row_1":{{"":""}},"row_2":{{"":""}},"row_3":{{"":""}},"row_4":{{"":""}}}},"summary":[{{"refer_id":"{0}","summary":null}}],"description":[{{"refer_id":"{0}","description":null}}]}}   ${productIDtestKV}
    END
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/update-product    headers=${headers}    data=${data}
    ${json}=    To Json    ${resp.content}
    ${status} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status}    success

API switch status
    [Arguments]   ${id}   ${current_status}
    ${data}=    Create Dictionary    id=${id}   current_status=${current_status}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/x-www-form-urlencoded
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Post Request    mysession    /products/detail/switch-status    headers=${headers}    data=${data}
    ${json}=    To Json    ${resp.content}
    ${status} =   Get Value From Json    ${json}    $.status
    Should Contain    ${status}    success
