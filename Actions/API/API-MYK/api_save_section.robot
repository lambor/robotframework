*** Settings ***
Library           SeleniumLibrary    # Test Setup    Before Test
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Library           StringFormat
Resource          ../../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Share/Computation.robot
Resource          ../../Share/utils.robot
Resource          ../../Common.robot
Resource          ../../PageBuilder/TopbarMenu.robot
Resource          ../../../Config/envi.robot
Resource          ../../../Config/Theme/theme_config.robot
Resource          ../../../Locators/PageBuilder/TopbarMenu.robot    # *** Test cases ***    # Kiểm tra log    #    # Tạo theme mới theo config: vegan_config.json    #    # Update Giao diện mặc định theo config: embeau_config.json    #    Publish theme Giao diện mặc định theo config: grocerin_config.json

*** Keywords ***
Save Section: ${json_file}
    ${file_data}=    Get Binary File    ${EXECDIR}${/}Config${/}Setting Theme${/}${json_file}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/save-section    headers=${headers}    data=${file_data}
    Should be equal as strings    ${resp.status_code}    200

Get Section
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Get Request    mysession    api/v1/theme/get-sections    headers=${headers}
    Log    ${resp.json()}
    Return From Keyword    ${resp.json()}

Update Section
    [Arguments]    ${name_update}    ${id}
    ${data}=    Format String    {{"section_name":"{0}","section_id":"{1}"}}    ${name_update}    ${id}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/update-section    headers=${headers}    data=${data}
    Should be equal as strings    ${resp.status_code}    200
    Log    ${resp.json()}

Delete Section
    [Arguments]    ${id}
    ${data}=    Format String    {{"section_id":"{0}"}}    ${id}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/delete-section    headers=${headers}    data=${data}
    Should be equal as strings    ${resp.status_code}    200
    Log    ${resp.json()}
