*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           String
Library           StringFormat
Library           BuiltIn
Library           OperatingSystem

*** Keywords ***
Get list image
    ${param}=    create dictionary    is_library=0    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    create session    lolo    ${coreapi_url}
    ${resp1}=    get request    lolo    /v1/media    headers=${heades1}    params=${param}
    log    ${resp1.json()}
    Should be equal as strings    ${resp1.status_code}    200
    Return from keyword    ${resp1.json()}

search image with keyword
    [Arguments]    ${image_name}
    ${param}=    create dictionary    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media?keyword={0}&is_library=0    ${image_name}
    create session    lolo    ${coreapi_url}
    ${resp}=    get request    lolo    ${endpoint}    headers=${heades1}    params=${param}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200
    ${id}    Get Value From Json    ${resp.json()}    $.data.data[0].id
    ${id}=    Evaluate    $id[0] if $id else 0
    Log    ${id}
    Return From Keyword    ${id}

delete an image
    [Arguments]    ${image_id}
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media/remove/{0}    ${image_id}
    create session    lolo    ${coreapi_url}
    ${resp}=    post request    lolo    ${endpoint}    headers=${heades1}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200

Get image detail
    [Arguments]    ${image_id}
    ${headers1}=    Create Dictionary    store-id=${retailer_id}
    Create Session    ali    ${coreapi_url}    verify=True
    ${uri}    Format String    /v1/media/detail/{0}    ${image_id}
    ${resp}=    Get Request    ali    ${uri}    headers=${headers1}
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.json()}
    Return from keyword    ${resp.json()}

Update information image
    [Arguments]    ${image_id}    ${title_update}    ${alt_update}    ${caption_update}
    ${data}    Format String    {{"title": "{0}","alt": "{1}","caption": "{2}","file_name": "Nguyễn Thu.jpg" }}    ${title_update}    ${alt_update}    ${caption_update}
    ${endpoint}    Format String    v1/media/update/{0}    ${image_id}
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    create session    lolo    ${coreapi_url}
    ${resp1}=    Post request    lolo    ${endpoint}    data=${data}    headers=${heades1}
    Should Be Equal As Strings    ${resp1.status_code}    200
    Log    ${resp1.json()}

Get image detail that not exists
    [Arguments]    ${image_id}
    ${headers1}=    Create Dictionary    store-id=${retailer_id}
    Create Session    ali    ${coreapi_url}    verify=True
    ${uri}    Format String    /v1/media/detail/{0}    ${image_id}
    ${resp}=    Get Request    ali    ${uri}    headers=${headers1}
    Should Be Equal As Strings    ${resp.status_code}    422
    log    ${resp.json()}
    Return from keyword    ${resp.json()}

Update image title
    [Arguments]    ${image_id}    ${title_update}
    ${data}    Format String    {{"title":"{0}"}}    ${title_update}
    ${endpoint}    Format String    /v1/media/update/{0}    ${image_id}
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    create session    lolo    ${coreapi_url}
    ${resp1}=    Post request    lolo    ${endpoint}    data=${data}    headers=${heades1}
    Should Be Equal As Strings    ${resp1.status_code}    200
    Log    ${resp1.json()}

Get image title
    [Arguments]    ${image_id}
    ${resp.json()}    Get image detail    ${image_id}
    ${title}    Get Value From Json    ${resp.json()}    $.data.title
    ${title}=    Evaluate    $title[0] if $title else 0
    Log    ${title}
    Return From Keyword    ${title}

post an image
    ${file_data}=    Get File For Streaming Upload    E:/Robot Framework/atdd-mykiot/Actions/Share/image-comparison/d3.jpg
    ${data}    Create dictionary    name=file    filename=${file_data}
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=multipart/form-data
    create session    lolo    ${coreapi_url}
    ${resp1}=    Post request    lolo    /v1/media/upload    data=${data}    headers=${heades1}
    Should Be Equal As Strings    ${resp1.status_code}    200
    Log    ${resp1.json()}

Search library image with keyword
    [Arguments]    ${image_name}
    ${param}=    create dictionary    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media?keyword={0}&is_library=1    ${image_name}
    create session    lolo    ${coreapi_url}
    ${resp}=    get request    lolo    ${endpoint}    headers=${heades1}    params=${param}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200
    ${id}    Get Value From Json    ${resp.json()}    $.data.data[0].id
    ${id}=    Evaluate    $id[0] if $id else 0
    Log    ${id}
    Return From Keyword    ${id}

Get full_path from library image
    [Arguments]    ${image_name}
    ${param}=    create dictionary    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media?keyword={0}&is_library=1    ${image_name}
    create session    lolo    ${coreapi_url}
    ${resp}=    get request    lolo    ${endpoint}    headers=${heades1}    params=${param}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200
    ${full_path}    Get Value From Json    ${resp.json()}    $.data.data[0].full_path
    ${full_path}=    Evaluate    $full_path[0] if $full_path else 0
    Log    ${full_path}
    Return From Keyword    ${full_path}

get full_path from your data
    [Arguments]    ${image_name}
    ${param}=    create dictionary    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media?keyword={0}&is_library=0    ${image_name}
    create session    lolo    ${coreapi_url}
    ${resp}=    get request    lolo    ${endpoint}    headers=${heades1}    params=${param}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200
    ${full_path}    Get Value From Json    ${resp.json()}    $.data.data[0].full_path
    ${full_path}=    Evaluate    $full_path[0] if $full_path else 0
    Log    ${full_path}
    Return From Keyword    ${full_path}

Get thumbnail from library data
    [Arguments]    ${image_name}
    ${param}=    create dictionary    sort_name=created_at    sort_type=desc
    ${heades1}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${endpoint}    Format String    /v1/media?keyword={0}&is_library=1    ${image_name}
    create session    lolo    ${coreapi_url}
    ${resp}=    get request    lolo    ${endpoint}    headers=${heades1}    params=${param}
    log    ${resp.json()}
    Should be equal as strings    ${resp.status_code}    200
    ${thumbnail}    Get Value From Json    ${resp.json()}    $.data.data[0].thumbnail
    ${thumbnail}=    Evaluate    $thumbnail[0] if $thumbnail else 0
    Log    ${thumbnail}
    Return From Keyword    ${thumbnail}
