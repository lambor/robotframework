*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Library           StringFormat

*** Keywords ***
Sửa slug của danh mục
    [Arguments]    ${slug-moi}    ${category_id}
    ${data}    Format String    {{"slug":"phan-lan-{0}","page_title":"Phần Lan","page_keywords":"phan,lan","meta_description":"Phần Lan","status":true}}    ${slug-moi}
    ${uri}    Format string    categories/{0}    ${category_id}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Put request    ali    ${uri}    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}

Get category_id from category_name
    [Arguments]    ${category_name}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Get Request    mysession    categories/cms    headers=${headers}
    Log    ${resp.json()}
    ${json}=    To Json    ${resp.content}
    ${json_path}    Format String    $.data.data[?(@.name=="{0}")].id    ${category_name}
    Log    ${json_path}
    ${id_json}=    Get Value From Json    ${resp.json()}    ${json_path}
    ${id} =    Evaluate    $id_json[0] if $id_json else 0
    Log    ${id}
    Return From Keyword    ${id}

Get category detail
    [Arguments]    ${category_id}
    ${uri}    format string    /categories/{0}    ${category_id}
    ${headers}=    Create Dictionary    JWTToken=${jwttoken}
    Create Session    mysession    ${apiurl_pb}    verify=True
    ${resp} =    Get Request    mysession    ${uri}    headers=${headers}
    Log    ${resp.json()}
    return from keyword    ${resp.json()}

Get slug of category
    [Arguments]    ${category_id}
    ${resp.json()}    Get category detail    ${category_id}
    ${slug}=    Get Value From Json    ${resp.json()}    $.data.slug
    ${slug}=    Evaluate    $slug[0] if $slug else 0
    Log    ${slug}
    Return From Keyword    ${slug}

Sửa meta description của danh mục
    [Arguments]    ${meta-description-moi}    ${category_id}
    ${data}    Format String    {{"slug":"danh-muc-a","page_title":"Danh mục A","page_keywords":"danh,muc,a","meta_description":"Danh mục A giá rẻ {0}","status":true}}    ${meta-description-moi}
    ${uri}    Format string    categories/{0}    ${category_id}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Put request    ali    ${uri}    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}

Get meta description of category
    [Arguments]    ${category_id}
    ${resp.json()}    Get category detail    ${category_id}
    ${meta_description}    Get Value From Json    ${resp.json()}    $.data.meta_description
    ${meta_description}=    Evaluate    $meta_description[0] if $meta_description else 0
    Log    ${meta_description}
    Return From Keyword    ${meta_description}

Get category status
    [Arguments]    ${category_id}
    ${resp.json()}    Get category detail    ${category_id}
    ${status}    Get Value From Json    ${resp.json()}    $.data.status
    ${status}=    Evaluate    $status[0] if $status else 0
    Log    ${status}
    Return From Keyword    ${status}

switch status of category to 0
    [Arguments]    ${category_id}
    ${data}    Format String    {{"id":{0},"status":0}}    ${category_id}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Patch request    ali    /categories/cms/switch-status    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}

switch status of category to 1
    [Arguments]    ${category_id}
    ${data}    Format String    {{"id":{0},"status":1}}    ${category_id}
    ${headers1}=    Create Dictionary    JWTToken=${jwttoken}    Content-Type=application/json
    Create Session    ali    ${apiurl_pb}    verify=True
    ${resp}=    Patch request    ali    /categories/cms/switch-status    headers=${headers1}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log    ${resp.json()}

Get image of category
    [Arguments]    ${category_id}
    ${resp.json()}    Get category detail    ${category_id}
    ${image}=    Get Value From Json    ${resp.json()}    $.data.image
    ${image}=    Evaluate    $image[0] if $image else 0
    Log    ${image}
    Return From Keyword    ${image}
