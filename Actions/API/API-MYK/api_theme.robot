*** Settings ***
# Test Setup        Before Test
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Library           StringFormat
Resource          ../../../Locators/PageBuilder/Mytemplate.robot
Resource          ../../../Locators/PageBuilder/SidebarMenu.robot
Resource          ../../Share/Computation.robot
Resource          ../../Share/utils.robot
Resource          ../../Common.robot
Resource          ../../PageBuilder/TopbarMenu.robot
Resource          ../../../Config/envi.robot
Resource          ../../../Config/Theme/theme_config.robot
Resource          ../../../Locators/PageBuilder/TopbarMenu.robot

# *** Test cases ***
# Kiểm tra log
#     # Tạo theme mới theo config: vegan_config.json
#     # Update Giao diện mặc định theo config: embeau_config.json
#       Publish theme Giao diện mặc định theo config: grocerin_config.json

*** Keywords ***
Update Giao diện mặc định theo config: ${json_file}
    ${file_data}=   Get Binary File    ${EXECDIR}${/}Config${/}Theme${/}${json_file}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    version=1    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/update    headers=${headers}   data=${file_data}
    Log    ${resp.content}

Tạo theme mới theo config: ${json_file}
    # ${json}=    Convert String to JSON    ${theme}
    # Log    ${json}
    ${file_data}=   Get Binary File    ${EXECDIR}${/}Config${/}Theme${/}${json_file}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/create    headers=${headers}   data=${file_data}
    Log    ${resp.content}

Publish theme Giao diện mặc định theo config: ${json_file}
    ${file_data}=   Get Binary File    ${EXECDIR}${/}Config${/}Theme${/}${json_file}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/theme/publish    headers=${headers}   data=${file_data}
    Log    ${resp.content}
    Log    ${resp.request.body}
    Return From Keyword    ${resp.request.body}

Lấy config theme đang phát hành của gian hàng
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}
    ${resp} =    Get Request    mysession    api/v1/theme    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    ${published_theme_config}=    Set Variable    ${jsondata['data']['config']}
    ${str_config}=    Convert JSON To String    ${published_theme_config}
    [Return]    ${str_config}

Lấy version theme của ${template_name}
    Người dùng đang ở màn hình Templates
    ${sub_xpath1}=    Set Variable    //*[@id='my-templates']//*[contains(text(),'
    ${sub_xpath2}=    Set Variable    ')]//parent::*//parent::*//a[contains(@id,'open')]
    ${xpath}    Set Variable    ${sub_xpath1}${template_name}${sub_xpath2}
    ${id-version}=    Get Element Attribute    ${xpath}    id
    ${version}=    Get Substring    ${id-version}    5
    [Return]    ${version}

Lấy config theme của giao diện: ${tmpl_name}
    ${ver}=    Lấy version theme của ${tmpl_name}
    Log    ${ver}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    jwttoken=${jwttoken}
    ${params} =    Create Dictionary    version=${ver}
    ${resp} =    Get Request    mysession    api/v1/theme/page-builder    headers=${headers}    params=${params}
    ${jsondata}=    To Json    ${resp.content}
    ${detail_config}=    Set Variable    ${jsondata['data']['config']}
    ${str_config}=    Convert JSON To String    ${detail_config}
    # Config theme đang tùy biến
    #    Create Session    mysession    ${api_url}
    #    ${headers} =    Create Dictionary    store-id=${store_id}    auth-page-builder=${auth_page_builder}
    #    ${resp} =    Get Request    mysession    api/v1/theme/page-builder    headers=${headers}
    #    ${jsondata}=    To Json    ${resp.content}
    #    ${customized_config}=    Set Variable    ${jsondata['data']['config']}
    #    ${str_config}=    Convert JSON To String    ${customized_config}
    #    [return]    ${str_config}
    [Return]    ${str_config}

So sánh config của giao diện đang phát hành và ${tmpl-name} trong My Templates giống nhau
    ${published_theme_str}=    Lấy config theme đang phát hành của gian hàng
    ${compared_theme}    Lấy config theme của giao diện: ${tmpl-name}
    Log    ${published_theme_str}
    Log    ${compared_theme}
    Should Be Equal As Strings    ${published_theme_str}    ${compared_theme}

Giao diện ${theme} lưu thành công phông chữ ${font} trong config
    ${config_str}    Lấy config theme của giao diện: ${theme}
    Should Contain    ${config_str}    ${font}
