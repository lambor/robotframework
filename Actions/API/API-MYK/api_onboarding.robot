*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
Library           StringFormat
Resource          ../../Share/utils.robot
Resource          ../../Common.robot
Resource          ../../../Config/envi.robot



*** Keywords ***
Tạo gian hàng mykiot thành công
      ${data}=    create dictionary    token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6IjJYViJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxMTU0OTYsImlhdCI6MTYzMjM4MjA5NCwiZXhwIjoxNjM0OTc0MDk0LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbIlVzZXIiXSwia3ZzZXMiOiJmMjRjZjkyNjhhZmU0NDAxYjM1ZjY0M2E4N2ZlOGEzYyIsImt2dWlkIjoxMTU0OTYsImt2bGFuZyI6InZpLVZOIiwia3Z1dHlwZSI6MCwia3Z1bGltaXQiOiJGYWxzZSIsImt2dWFkbWluIjoiVHJ1ZSIsImt2dWFjdCI6IlRydWUiLCJrdnVsaW1pdHRyYW5zIjoiRmFsc2UiLCJrdnVzaG93c3VtIjoiVHJ1ZSIsImt2YmlkIjoxMjA3MSwia3ZyaW5kaWQiOjMsImt2cmNvZGUiOiJhbmhuazAzMDE4OSIsImt2cmlkIjo4MTIxODAsImt2dXJpZCI6ODEyMTgwLCJrdnJnaWQiOjEsInBlcm1zIjoiIn0.6FymE2M3F8s9fyQYkYuvj1SHGef0wyUa3Spgu6_WX8w   retailer_code=anhnk030189    retailer_id=812180
      ${headers}=   Create Dictionary     Content-Type=application/x-www-form-urlencoded
      Log    ${api_url}
      Create Session    mysession    ${api_url}
      ${resp} =    Post Request    mysession    /api/v1/create-store    headers=${headers}   data=${data}
      Log    ${resp.json()}
      Should be equal as strings    ${resp.status_code}    200
      ${jsondata}=    To Json    ${resp.content}
      ${data-auth-page-builder}=    Set Variable    ${jsondata['data']}
      Log    ${data-auth-page-builder}
      # Complete onboarding step
      ${headers1}=    Create Dictionary    store-id=812180    auth-page-builder=${data-auth-page-builder}   Content-Type=application/json;charset=UTF-8
      ${resp1} =    Post Request    mysession    /api/v1/sync/onboarding-step     headers=${headers1}   data={"step":"onboarding-completed"}
      Log    ${resp1.json()}
      Should be equal as strings    ${resp1.status_code}    200

Xóa gian hàng mykiot thành công
    ${data}=      Create Dictionary    email=${onboarding_gettoken_user}     password=${onboarding_gettoken_password}
    ${headers}=   Create Dictionary     Content-Type=application/x-www-form-urlencoded    Accept-Encoding=gzip, deflate, br
    Create Session    mysession    ${url}
    ${resp}=    Post On Session    mysession    /auth/login   headers=${headers}    data=${data}
    # ${resp}=    Post Request    mysession    /auth/login    headers=${headers}    data=${data}
    Log    ${resp.json()}
    ${jsondata}=    To Json    ${resp.content}
    ${token}=    Set Variable    ${jsondata['access_token']}
    ${str_token}=    Convert JSON To String    ${token}
    Log    ${str_token}
    ${authorization}=   Set Variable    Bearer ${token}
    Log    ${authorization}
    ${headers1}=    Create Dictionary    authorization=${authorization}
    ${resp1}=     Post Request    mysession    /api/store/delete/${onboarding_retailerID}    headers=${headers1}
    Log    ${resp1.json()}
    Should be equal as strings    ${resp.status_code}    200
