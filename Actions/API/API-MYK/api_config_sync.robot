*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Library           String
<<<<<<< Updated upstream
Library           StringFormat
=======
>>>>>>> Stashed changes
Resource          ../../Share/Computation.robot
Library           StringFormat

*** Keywords ***
Đồng bộ thủ công bằng API
    ${headers}=    Create Dictionary    cookie=${cookie_sync}
    Create Session    mysession    ${url}
    ${resp}=    Post Request    mysession    advance-setting/sync/syncCacheProduct    headers=${headers}
    log    ${resp.request.body}
    Log    ${resp.json()}

Reset thiết lập đồng bộ
    [Arguments]    ${CN_dbtonkho_Id}    ${CN_dbdonhang_Id}    ${banggiaban_Id}    ${thukhac_Id}    ${description_type}
    ${file_data}=    Format String    {{"data":{{"generalSetting":{{"description":"{4}"}},"branch":{{"branchInventory":[{0}],"branchOrder":{1}}},"surchargeIds":[{3}],"priceBook":{{"regularPrice":{2}}}}},"name":"storeConfig"}}    ${CN_dbtonkho_Id}    ${CN_dbdonhang_Id}    ${banggiaban_Id}    ${thukhac_Id}
    ...    ${description_type}
    log    ${file_data}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/configs    headers=${headers}    data=${file_data}
    log    ${resp.request.body}
    Log    ${resp.json()}

Lấy danh sách chi nhánh
    ${headers}=    Create Dictionary    store-id=${retailer_id}
    Create Session    mysession    ${api_url}
    ${resp}=    Get Request    mysession    api/v1/sync/branches    headers=${headers}
    ${list_name}    Get Value From Json    ${resp.json()}    $.[:].branchName
    Log    ${list_name}
    Return From Keyword    ${list_name}

Lấy tên chi nhánh qua id
    [Arguments]    ${branch_id}
    ${headers}=    Create Dictionary    store-id=${retailer_id}
    Create Session    mysession    ${api_url}
    ${resp}=    Get Request    mysession    api/v1/sync/branches    headers=${headers}
    ${branch_name}    Get Value From Json    ${resp.json()}    $.['?(@.id=="${branch_id}")'].branchName
    log    ${branch_name}
    return from keyword    ${branch_name}

Cập nhật thiết lập đồng bộ
    [Arguments]    ${CN_dbtonkho_Id}    ${CN_dbdonhang_Id}    ${banggiaban_Id}    ${banggiakm_Id}    ${thukhac_Id}    ${description_type}
    ${file_data}=    Format String    {{"data":{{"generalSetting":{{"description":"{5}"}},"branch":{{"branchInventory":[{0}],"branchOrder":{1}}},"surchargeIds":[{4}],"priceBook":{{"regularPrice":{2},"salePrice":{3}}}}},"name":"storeConfig"}}    ${CN_dbtonkho_Id}    ${CN_dbdonhang_Id}    ${banggiaban_Id}    ${banggiakm_Id}
    ...    ${thukhac_Id}    ${description_type}
    log    ${file_data}
    Create Session    mysession    ${api_url}
    ${headers} =    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    Content-Type=application/json
    ${resp} =    Post Request    mysession    api/v1/configs    headers=${headers}    data=${file_data}
    log    ${resp.request.body}
    Log    ${resp.json()}

Kiểm tra cấu hình đồng bộ chứa: ${jsonpath}
    Log    ${jsonpath}
    ${headers}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    create session    ss1    ${api_url}
    ${resp}=    get request    ss1    api/v1/configs    headers=${headers}
    Log    ${resp.json()}
    ${jsonvalue}=    JSONLibrary.Get Value From Json    ${resp.json()}    $.data.value
    ${jsonvalue}=    evaluate    $jsonvalue[0]
    log    ${jsonvalue}
    ${strdata}    convert to string    ${jsonvalue}
    ${strdata1}    Remove String    ${strdata}    \\
    Log    ${strdata1}
    #${data}=    Evaluate    json.loads($jsonvalue1)    json
    # ${jsondata}    JSONLibrary.Convert String To JSON    ${strdata1}
    # log    ${jsondata}
    # ${data}=    JSONLibrary.Get Value From Json    ${jsonvalue}    ${jsonpath}
    # ${key}=    Evaluate    $data[0]
    Should Contain    ${strdata1}    ${jsonpath}

Lấy tổng số sản phẩm hoặc mã sản phẩm đang hiển thị ở màn hình Thiết lập đồng bộ sản phẩm Mykiot
    [Arguments]    ${key}    ${category_id}    ${tradeMarkName}    ${imagefilter}    ${isSync}
    ${headers}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${params}=    Create Dictionary    key=${key}    page=1    limit=30    order=mostRelevant    categoryReferIds=${category_id}
    ...    ignoreStatus=1    ignoreCategoryStatus=1    tradeMarkName=${tradeMarkName}    imageFilter=${imagefilter}    filterIsSync=${isSync}    # ${params}=
    # ...    # Create Dictionary    key=${EMPTY}    page=1    limit=30    order=newest    categoryReferIds=${EMPTY}
    # ...    # ignoreStatus=1    ignoreCategoryStatus=1    tradeMarkName=${EMPTY}
    create session    ss1    ${api_url}
    ${resp}=    get request    ss1    api/v1/products/sync    headers=${headers}    params=${params}
    Log    ${resp.json()}
    ${jsondata}=    To Json    ${resp.content}
    ${total_product}=    Set Variable    ${jsondata['data']['total_products']}
    ${str_total_product}=    Convert JSON To String    ${total_product}
    IF    '${imageFilter}' != '${EMPTY}' or '${key}' != '${EMPTY}'
    ${product_id}=    Set Variable    ${jsondata['data']['products'][0]['code']}
    ${str_productID}=    Convert JSON To String    ${product_id}
    Log    ${str_productID}
    Return From Keyword    ${str_productID}
    END
    Log    ${str_total_product}
    Return From Keyword    ${str_total_product}

Đồng bộ một sản phẩm
    [Arguments]    ${sync-status}    ${productID}
    ${headers}=    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    content-type=application/json;charset=UTF-8
    ${data}=    Format String    {{"isSync":{0},"productId":{1}}}    ${sync-status}    ${productID}
    Create Session    sync    ${api_url}
    ${resp} =    Post Request    sync    api/v1/products/single/sync    headers=${headers}    data=${data}
    log    ${resp.request.body}
    #Log    ${resp.json()}

Tìm kiếm sản phẩm theo product_code
    [Arguments]    ${product_code}
    ${headers}=    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${params}=    Create Dictionary    key=${product_code}    page=1    limit=30    order=mostRelevant    categoryReferIds=${EMPTY}
    ...    tradeMarkName=${EMPTY}    ignoreStatus=1    ignoreCategoryStatus=1
    Create Session    sync    ${api_url}
    ${resp} =    Get Request    sync    api/v1/products/sync    headers=${headers}    params=${params}
    Log    ${resp.json()}
    ${is_sync}    Get Value From Json    ${resp.json()}    $.data.products[*].is_sync
    ${is_sync}=    Evaluate    $is_sync[0]
    Log    ${is_sync}
    Return From Keyword    ${is_sync}

Thiết lập đồng bộ bật/tắt cho Category Auto Test
    [Arguments]    ${is_sync}
    ${headers}=    Create Dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}    content-type=application/json;charset=UTF-8
    ${data}=    Format String    {{ \ \ "page": 1, \ \ "key": "", \ \ "categoryReferIds": [ \ \ \ {0} \ \ ], \ \ "limit": 50, \ \ "isSync": {1}, \ \ "ignoreStatus": 1, \ \ "ignoreCategoryStatus": 1, \ \ "tradeMarkName": [] }}    ${cateIDtestKV}    ${is_sync}
    Create Session    LH    ${api_url}
    ${resp} =    Post Request    LH    api/v1/products/mass/sync    headers=${headers}    data=${data}
    Log    ${resp.json()}
    ${categoryid}    Get value from json    ${resp.json()}    $.categoryReferIds
    Return From Keyword    ${categoryid}

Sản phẩm đồng bộ trong danh mục ON
    ${headers}=    create dictionary    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
    ${params}=    Create Dictionary    key=${EMPTY}    page=1    limit=30    order=newest    categoryReferIds=${cateIDtestKV}
    ...    ignoreStatus=1    ignoreCategoryStatus=1
    create session    ss1    ${api_url}
    ${resp}=    get request    ss1    api/v1/products/sync    headers=${headers}    params=${params}
    Log    ${resp.json()}
    ${product_id}    Get Value From Json    ${resp.json()}    $.data.products[0].id
    ${is_sync}    Get Value From Json    ${resp.json()}    $.data.products[0].is_sync
    ${is_sync}=    Evaluate    $is_sync[0]
    Return From Keyword    ${is_sync}

Tạo store
    ${file_data}    Format string    {{"retailer_code":"{0}","retailer_id":{1},"secret_key":"{2}","client_id":"{3}"}}    test190720212    812175    4AF3F1CC9F4485141AC0B727CD76B1E024F65B06    eeb024c2-f603-4c77-9e08-0299af06e545
    ${headers} =    Create Dictionary    Content-Type=application/json
    create session    mysession    ${api_url}
    ${resp} =    Post Request    mysession    api/v1/create-store    headers=${headers}    data=${file_data}
    log    ${resp.request.body}
    Log    ${resp.json()}
    #    store-id=${retailer_id}    auth-page-builder=${auth_page_builder}
