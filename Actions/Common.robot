*** Settings ***
Library           SeleniumLibrary
Library           RequestsLibrary
Library           Collections
Library           JSONLibrary
Library           OperatingSystem
Resource          ../Locators/CMS.robot
Resource          ../Actions/Share/Computation.robot
Resource          ../Config/envi.robot
Resource          PageBuilder/SyncCenter.robot
Resource          PageBuilder/ProductSetting.robot

*** Variables ***
${locator-onBoardModal}    //*[@id='onBoardModal']//*[@class='x-close']

*** Keywords ***
Get Browser Console Log Entries
    ${selenium}=    Get Library Instance    SeleniumLibrary
    ${webdriver}=    Set Variable    ${selenium._drivers.active_drivers}[0]
    ${log entries}=    Evaluate    $webdriver.get_log('browser')
    log    ${log entries}=
    [Return]    ${log entries}

Gian hàng "Testautomykiot" đăng nhập thành công
    Open Browser    ${access_url}    gc
    Maximize Browser Window
    Sleep    3s
    # Đóng onBoardModal
    #    Click To Element    //*[@id='onBoardModal']//*[@class='x-close']

Người dùng truy cập màn hình "Page Builder"
    Sleep    3
    # Click To Element    ${btn_tuybienhinhthuc}
    # Sleep    3

Người dùng truy cập màn hình Templates
    #Người dùng truy cập màn hình "Page Builder"
    Click To Element    ${btn_toggle_template}

Truy cập StoreFront
    Open Browser    ${storefront_url}    gc
    Maximize Browser Window
    Reload Page
    Sleep    1s

Người dùng truy cập màn hình "Tiện Ích"
    Gian hàng "Testautomykiot" đăng nhập thành công
    Người dùng truy cập màn hình "Page Builder"
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    ${locator-onBoardModal}
    Run Keyword If    ${present}    Click To Element    ${locator-onBoardModal}
    Click To Element    //*[@id='cube']

Truy cập Sản phẩm auto test ở màn hình StoreFront
    # Đảm bảo sản phẩm auto test được bật trên StoreFront
    Thiết lập đồng bộ ON cho sản phẩm test
    Có "Sản phẩm Auto Test" đang Bật hiển thị trong danh sách Sản phẩm của gian hàng
    #######
    ${url}    Set Variable    ${storefront_url}${productIDtesturl}
    Open Browser    ${url}    gc
    Maximize Browser Window
    Reload Page
    Sleep    1s

Truy cập Category auto test ở màn hình StoreFront
    ${url}    Set Variable    ${storefront_url}${categorytesturl}
    Open Browser    ${url}    gc
    Maximize Browser Window
    Reload Page
    Sleep    3s
    Reload Page
    Sleep    3s
    Reload Page
    Sleep    3s

Truy cập StoreFront ở màn hình Mobile
    Open Browser    ${storefront_url}    gc
    Set Window Size    375    812
    Reload Page
    Sleep    1s

Truy cập màn hình Quản lý KiotViet của gian hàng ${store-name} user=${user} password=${pass}
    ${url}=    Set Variable    https://${store-name}.kvpos.com:59903/
    ${url-live}=    Set Variable    https://${store-name}.kiotviet.vn/
    Log    ${url}
    Log    ${url-live}
    Run Keyword If    '${env}' != 'live'    Open Browser    ${url}    gc
    Run Keyword If    '${env}' == 'live'    Open Browser    ${url-live}    gc
    # Open Browser    ${url}    gc
    Maximize Browser Window
    Input Text    //*[@id='UserName']    ${user}
    Input Text    //*[@id='Password']    ${pass}
    Click Button    //*[@name='quan-ly']
    Sleep    3s

Kiểm tra MHQL Kiotviet chứa button dẫn sang trang quản trị/web Mykiot
    Bấm Tạo web trên MHQL KiotViet
    ${count-website}    Get Element Count    //*[@class='mk-btn-success' and contains(@href,'anhnk030189')]
    Should Be Equal As Integers    ${count-website}    1
    Click To Element    //*[@class='mk-btn-success' and contains(@href,'connect')]
    ${current-window}    Get Window Titles
    Should Contain    ${current-window}    Dashboard

Kiểm tra MHQL Kiotviet chứa button Kích hoạt Mykiot
    Bấm Tạo web trên MHQL KiotViet
    ${count}    Get Element Count    //*[@class='mk-btn-success' and contains(@href,'onboarding')]
    Should Be Equal As Integers    ${count}    1

Bấm Tạo web trên MHQL KiotViet
    Sleep    3s
    Reload Page
    Sleep    6s
    Click To Element    //*[@class='mk-create-btn']
