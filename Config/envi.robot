*** Settings ***
Library           SeleniumLibrary
Library           Collections
Library           OperatingSystem
Resource          ../Actions/API/API-KV/api_access_kv.robot
Resource          ../Actions/API/API-MYK/api_access_myk.robot
Resource          ../Actions/API/API-MYK/api_onboarding.robot

*** Keywords ***
fill env
    [Arguments]    ${env}
    Log    ${env}
    ${dic_product_test_id_myk}    Create Dictionary    stagingnew=5240    prelivenew=4702    live=28333809
    ${dic_product_test2_id_myk}    Create Dictionary    stagingnew=25009481    prelivenew=12679    live=11022358
    ${dic_id_horizontal_menu}    Create Dictionary    stagingnew=121433    prelivenew=326    live=64500
    ${dic_onboarding_retailerID}    Create Dictionary    live=164731    stagingnew=812180    prelivenew=812180
    ${dic_onboarding_retailer_name}    Create Dictionary    live=testz5    stagingnew=anhnk030189    prelivenew=anhnk030189
    ${dic_onboarding_email}    Create Dictionary    live=admin    stagingnew=admin    prelivenew=admin
    ${dic_onboarding_password}    Create Dictionary    live=123456    stagingnew=123    prelivenew=123
    ${dic_onboarding_gettoken_user}    Create Dictionary    live=system@citigo.com.vn    stagingnew=mykiot@citigo.com.vn    prelivenew=mykiot@citigo.com.vn
    ${dic_onboarding_gettoken_password}    Create Dictionary    live=k10Tvl3T    stagingnew=123456    prelivenew=123456
    ${dic_public_kvapi}    Create Dictionary    live=https://public.kiotapi.com    stagingnew=XXX    prelivenew=xxx
    ${dic_product_test_url}    Create Dictionary    stagingnew=/san-pham-auto-test-khong-xoa.p5240.html    prelivenew=/san-pham-auto-test-khong-xoa.p4702.html    live=/san-pham-auto-test-khong-xoa.p28333809.html
    ${dic_product_test_code}    Create Dictionary    stagingnew=SP001618    prelivenew=SP001618    live=SP001618
    ${dic_product_test_id_kv}    Create Dictionary    stagingnew=338062    prelivenew=338062    live=14352454
    ${dic_product_test2_id_kv}    Create Dictionary    stagingnew=354806    prelivenew=354806    live=1445556
    ${dic_category_test_id_kv}    Create Dictionary    stagingnew=27396    prelivenew=27396    live=70277
    ${dic_category_test_url}    Create Dictionary    stagingnew=/category-auto-test.c330.html    prelivenew=/category-auto-test.c108.html    live=/1momobile.c275626.html
    # ${dic_cookie_sync}    Create Dictionary    live=xxx    stagingnew=kvas-uuid=7eae2554-dae8-4b68-8f9b-a5060ed29c61; kvas-uuid-d=1627368115554; gkvas-uuid=d08392ca-c00c-4be6-89d8-d2d7a530c05e; gkvas-uuid-d=1627368115556; _ga=GA1.2.1675067360.1627368117; __zi=2000.SSZzejyD6zS_W_caXG0PpZFBlgBB3W6CRzcqlDbC4vfzrkgttm1QctFUwB7V2H6SVToZizmE4vfwswIp.1; _gid=GA1.2.15112005.1630290665; JWTToken=eyJpdiI6IndCYXQ0VldXXC9DTzUwSGg0S01XUUtBPT0iLCJ2YWx1ZSI6ImhXeDluelRvNFJNRHBEdXNxVVoyQWVxVndhY1J5VmxSc3diODdoNzgyQ3NwU3N1UHMzSlZCTkxXRGcwSDJ4bGJ3U1lFMUo4OFJmMzU2WENNZFwvd2hhN00rSlpHSXpsc3lwR3FiOEpFeSsxQ1FzOVFIdnhkRDVXU1JHdzJPb1A0TEJENUJjZUd3YjZYQ0syNUJuR2syYzYybGxTUDNZbUxVUzc5dkQ0MDJ3ZjJrdFd3MXdKSkY3NndWaEd3dUtWRVZlRlVYZ0tcL2dZUVRDMUdJQXlRQ25IXC9qSkF0VVBXVG1qVExKQVwvR1RUSVYxUkJZVW9la0JXN2NXNXhTVHl5V2NzRnJ3RXh2NWwycmQrdTRMeXYrXC9RSm1WYkFuenZrNzYxTThCRllOUFdcL3B4cmxLdnJHOGlSXC9zSHZpRkRoR1BOaE85elhnd2t1a1NkQnNiYzg0MUlNSFdsUm1ERVUzSTJNQnMyMXQwQ0VuREVhMmFmWENXRzRSZWZvMWtGSVgzXC9oY3ZXMTdcL3hiV24zUFN6SWQzekI3ek1hS3JWc0dyelhqXC85RzlWUDFQU09GelwvQ3Z6ZzZ6cnQ2UllcLzJFNUNWUzAiLCJtYWMiOiJjNGRlYmViZTM1YjBiZDIyZDg0Yzc0ZDkxZjk4ZTE4YTlhZWI5YjgwYmM5YTQ3OTdjOGEyYjM1Mjk3ZTUwM2QwIn0%3D; auth-page-builder=MnIwSXlKMDB3K21MOEZYLzlQZmNWZz09; kv-session=1c6e2f91-476e-47dd-b513-59cc1aa0467c; reset-theme=reset; kv-session-d=1630896399993; _gat_UA-149756813-1=1; laravel_session=eyJpdiI6IkJKcSs3eVQwU1hwTjFzeU1IbVNaVVE9PSIsInZhbHVlIjoid0xXNGNkQXpuZ2NLbW9LRkQ0TlNKMVE0MjNzWFRhbHB3dWtPWTk4OHp0XC81WFhTeVRkOUM0UVwvVXA3RFgyMDlDIiwibWFjIjoiZGVhODk1NTQzOGVhN2FhZTMwMTgzNWZlZDAwNTVmYzhhODQyOTI2MDcwMDEwMjVhYzE3OWVkZWVhOTkxOWMwMCJ9    prelivenew=auth-page-builder=MnIwSXlKMDB3K21MOEZYLzlQZmNWZz09; _ga=GA1.2.51535787.1630651293; _gid=GA1.2.115678748.1630651293; reset-theme=reset; _gat_UA-149756813-1=1; JWTToken=eyJpdiI6InU1ZTY4dW56eUxIV09obGtCcklZaWc9PSIsInZhbHVlIjoiQWRINU96QktvZ09NMmlhaWRPREZOSnNCYm5pZGxYZXVRbDVQUzEzUFAzSWhwSEhaY28rZmUra2JTR2pxUnVta1B2ODVJM1A0dFhNK0YzeDNlVlNXb21uMERFeUlIRVhBVlBVOG1cL0RoakhUa2h4a0tSNjVZRFFmT1kzeHJoN1wvSnVzYm90Y3JuUGk1emZYNEJqXC9jakd2cXgwN202M0dKdmkrdUNzOCtnMjh6TG91TlBUXC9NTTh3M2R2SXBueEttc3FPS21CblRxaHFoZENGTHJiSUczb1JwZTdRSXFqd1hCQkFMTUlLbWowVkljdmJZSFhBbjVxdjdEMkx4SUhsTytZTDdDSkpFTHZMVWdHcmNlbU5XNHYyRHlEVTY0ZFUzSnFOVW53ZzdqU1MwdHplcHg2YWZ1V0h5eTBDWHBqTlVUaFkybitWd2NJNzhPbmdqVDNrbU1iNGY4bUlZK0NCZEhMeXJNSkZmWTRBWEdRRnpiU2l5U1RzU0pYQkxhdEF0cVBJVEhhaVNNSHg1RU8yZk9tR0xUblROVURiS0ZxMHoyZVM5aDQwQldvUE1LZ2RIcjJUSmZsOUNTMlZsYjlkOW4iLCJtYWMiOiIzNmY5ZTgxZGE0N2U2NjFiNzE4ZjIzNDkxNGE0NzY3Y2EzM2U1NjBmOWIyZGQ3ZjViNDcwOTY1YmJkYTI4MDYxIn0%3D; kvas-uuid=3cc727f3-24ec-4549-b8f5-71c21d3713f8; kvas-uuid-d=1630658363299; gkvas-uuid=a0c02daa-94ed-4dca-adae-df301ade3471; gkvas-uuid-d=1630658363300; kv-session=40420cc3-6ddc-49b7-8883-3ce999eabebc; kv-session-d=1630658363303; laravel_session=eyJpdiI6Im5Hdjh5SzRcL0dKNVk1MGhtMm5wN1JBPT0iLCJ2YWx1ZSI6IlhYVWcwT1wvUWdJOW9OYnVPNWhcL2tJcjNmcDZla29kYjIxZlM1WGtic3VSeitWdU9KUlVva1l1VmIwamlsMWoxNiIsIm1hYyI6IjRmZTA0MzJmZjBmN2RjZGNmZmRmMDAwZWQ5M2Q5NzRiODkyNzc3Y2NiMTQ1ODM0ZTQxNzE3ZTRlZjFiYTRkOTQifQ%3D%3D
    ${dic_jwttoken}    Create Dictionary    live=eksyWTZ0S0lZbUhwRGxyQStJbjhiek0vTHI5SVRzdGZuaEFHMzN1bXRlY05LWmY4VktUSFJXQ0E4a3prRGlOTm1Oa3NyR012QkJ3SlhQVS9acjZneS91ZmRlL0I3bzUyWjQ3K09ZYnhIbm5ESGlPRkVJK25FYi9BOUE4Q1R1TjVsMHRVek4rMFBpb2NVbDhma0dPNWdoMUsxZXhwWW02anlxa3U0Y25lTzhJLzU0bjJSMHEzTWo4SjY5SEFtWDRPNkFmNlk5VHlyTDBoNzNOTGhPNVczUlZZRDgrRmZ4eDBySmRtZ0lyZXFEcjdiK1ZiSkh4d3hsYmNCbmRjUU04MWplU1NVaXFCcDNFWC9MNkFaYzFwQitWazFJMDBuUGRFQ1YwSk1pSk5UN3RyNGJORVVSa29iZzRvTVluWHA4aVVjcjNYUnpRdEw5NEFZcE80ZUx0dEJvRnltcGNhcCsrVE12empIZGFTaGFsNW0wNCt6dEx1bHpvWGhJd0drL0JNZkczMXZnaGRJcytlZDkzZ0ZnTHVVVDJDdTF0WFg2K21lWkw1aCtrMW1WcERYSVVjeVgvamY3N0hmb3hMYUk2TQ==    stagingnew=eksyWTZ0S0lZbUhwRGxyQStJbjhiek0vTHI5SVRzdGZuaEFHMzN1bXRlY05LWmY4VktUSFJXQ0E4a3prRGlOTkpMeTlrUzJlQkNGNWVNVis4NUpXaUhPZ0NjOGtiQzQrOFdtNWhZRWpGQVcyUDlKQ1NFVWNheUdJanoxSm5FT0J5SjR6RWhReHNvcTAyUnhkL2Jjd2FzeXlUbk9TbTNONUwvS1d4UVBmS3JLMXJ5UmpqMFp2SzJTNHJFNlBjdU5XTUNqWDJLaUNQY2tBM2grVWQ5VXpSZXNQU3hSZExQT25NV0lmSzRhQ3N2Mjh6YTlMSkRhdjZwdEppZnYxNmc3dmZyajJRaHN5TnA4WFpMNWM5dER5NFNQSndEbDR5QVNUR1ZZSHJHb3d0YklScVc2WkZpM21pTU9JRXlaeXBCaXdFRndvMWhzM3pzUnBuTW05eVU3OXI5WmhlTUlJZi9TVFV3dzJiMkZTSzJWN1VvS1kyMHIyWFg0SHdaNTN5ajFNQm9tSmFqRHFUUnAvUk8vejNoYzZxMHJiMWt5Vk1wVWVNQ2I1SWpLekJUb0piZ0QxU0c0SHlheC83L0J5V2RlNFVZYjhyajM3aG1HTjNSQ3RSWmdCc2c9PQ==    prelivenew=eksyWTZ0S0lZbUhwRGxyQStJbjhiek0vTHI5SVRzdGZuaEFHMzN1bXRlY05LWmY4VktUSFJXQ0E4a3prRGlOTlhpUmVGMi9qWG9NYmkzd2JrYi93Q25jN2V2cG9adGVCZ3dRWnFxdFZINFBjWUEvb3RaTWM0bXpQbnVPT0N2aExKZHVrVzBBY1RFN0pCcmNqOU54ZWQwTnFhdkNlaWJhT3d5Z3R6UXo3cld4QjYwSTAvL1c0bWhROHRiKzB5UERGWldTaDMxUU5sRjhBZmpuQ1dzRjJqUWZUSlZDRC9xQ0dNbVhaU0Z2enpKNjhDV2gwNjRUK0NxYjdUMDAzTU1wQ2JNUXJDVlN3NlhrNkpWcWM0anNSWFNWbnZFaVVXcCs0eDVSSENTQ2IxREtHM08rRDhhWGJqam13djN1dDJPY2tDVmFJWG5iUWd1Y3ROVG5DNDNpOFlSTXp5OEh5TW4xRDhURytiVEN0VmIzYkpYVE4zYjYzeGZmVm1pNGMyVWZVZkpFVVlJdHJ2Wk9pUi9lVnlqeHpmS1h3ajNNVEwwODBZK3Zac01Ya1NmejNaMTZDbGNsT3ZuTy9kdlJ6aW1aMEhUQ2lzM0ttandkS0xhNjJzSktudlE9PQ==
    ${dic_auth_page_builder}    Create Dictionary    live=a0RZRUZJVHRieC9yVk9ZdEZCQkJ0UT09    stagingnew=MnIwSXlKMDB3K21MOEZYLzlQZmNWZz09    prelivenew=MnIwSXlKMDB3K21MOEZYLzlQZmNWZz09
    ${dic_api_url}    Create Dictionary    live=https://api.mykiot.vn    stagingnew=https://api-staging.citigo.dev:40001    prelivenew=https://api-prelive.citigo.dev:40003
    ${dic_url}    Create Dictionary    live=https://admin.mykiot.vn    stagingnew=https://admin-staging.citigo.dev:40001    prelivenew=https://admin-prelive.citigo.dev:40003
    ${dic_clientid}    Create Dictionary    live=1f36fe67-a0f2-453b-bea5-9d778218279c    stagingnew=c8b67155-4f9a-4ebe-80ee-cac6e18e8d44    prelivenew=c8b67155-4f9a-4ebe-80ee-cac6e18e8d44
    ${dic_secretkey}    Create Dictionary    live=5F16AD6044795B917F2F9E640F572EF835B71254    stagingnew=109774688415EBDBBEBE55C3BFFCE082773CC863    prelivenew=109774688415EBDBBEBE55C3BFFCE082773CC863
    ${dic_retailercode}    Create Dictionary    live=testautomykiot    stagingnew=testautomykiot    prelivenew=testautomykiot
    ${dic_retailerid}    Create Dictionary    live=737280    stagingnew=810060    prelivenew=810060
    ${dic_storeid}    Create Dictionary    live=6565    stagingnew=811213    prelivenew=811213
    ${dic_accessurl}    Create Dictionary    live=https://api.mykiot.vn/login/connect?key=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IndYYSJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxNjc5OSwiaWF0IjoxNjM4OTU4NDM3LCJleHAiOjE2NDEzNzc2MzcsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIiwicm9sZXMiOlsiVXNlciJdLCJrdnNlcyI6IjFkZDZiOTQ0MjE0ZDQ0Mjg4OWMwNWQzN2M5NWMxZGExIiwia3Z1aWQiOjE2Nzk5LCJrdmxhbmciOiJ2aS1WTiIsImt2dXR5cGUiOjAsImt2dWxpbWl0IjoiRmFsc2UiLCJrdnVhZG1pbiI6IlRydWUiLCJrdnVhY3QiOiJUcnVlIiwia3Z1bGltaXR0cmFucyI6IkZhbHNlIiwia3Z1c2hvd3N1bSI6IlRydWUiLCJrdmJpZCI6OTExMywia3ZyaW5kaWQiOjksImt2cmNvZGUiOiJ0ZXN0YXV0b215a2lvdCIsImt2cmlkIjo3MzcyODAsImt2dXJpZCI6NzM3MjgwLCJrdnJnaWQiOjI2LCJwZXJtcyI6IiJ9.RA8_5hbYCuKRc8Ygps9JHUTJPPGPbI-s8rdjgrxh-vav8MtFyfsK3NrO7o2KpUf6UGfRi2fvCrXrAfr_Hd_9dPSfF4_H1F83kE0ceAvoHnLzTYVi8eak35l7XVqjdeaqspLTA9NpTaY0PZeELsCcZqA1Y5jsjxmucL4MSeIb7Yj7XOwTjibwAzadrLEXBWaqjUJAVruDruaEMLXZLZcXETsuL8l1CnfPxDPuRxb3FBaZvc1vUc8gaR_cVRBre644PbMuYkgV2ELG-hNC-veJ7OCogij-FKgTY5T0kQdiYO-XEu68yu5sgL-_xoAbwnRHBxk_382TDY47s37hc9x_Bg    stagingnew=https://api-staging.citigo.dev:40001/login/connect?key=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6IjJYViJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxMTUxMzMsImlhdCI6MTYzMjg3OTQyNSwiZXhwIjoxNjM1NDcxNDI1LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbIlVzZXIiXSwia3ZzZXMiOiI3ZWRkZjcwZmY0ZGI0NmU4YWVmMWE4NDcwYjVmODM5ZiIsImt2dWlkIjoxMTUxMzMsImt2dXR5cGUiOjAsImt2dWxpbWl0IjoiRmFsc2UiLCJrdnVhZG1pbiI6IlRydWUiLCJrdnVhY3QiOiJUcnVlIiwia3Z1bGltaXR0cmFucyI6IkZhbHNlIiwia3Z1c2hvd3N1bSI6IlRydWUiLCJrdmJpZCI6MTE3NDksImt2cmluZGlkIjoxLCJrdnJjb2RlIjoidGVzdGF1dG9teWtpb3QiLCJrdnJpZCI6ODEwMDYwLCJrdnVyaWQiOjgxMDA2MCwia3ZyZ2lkIjoxLCJwZXJtcyI6IiJ9.oxs5kcULaEPAbzYrxD3wN2lcLM89ql5RpSt7OeMoYZ4&retailer_id=810060&retailer_code=testautomykiot    prelivenew=https://api-prelive.citigo.dev:40003/login/connect?key=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6IjJYViJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxMTUxMzMsImlhdCI6MTYzMjczNjQ4OSwiZXhwIjoxNjM1MzI4NDg5LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbIlVzZXIiXSwia3ZzZXMiOiI2YjJlMWVhMThjYjU0ZTc5YTBiZjNiOGY2M2EwYjIxNiIsImt2dWlkIjoxMTUxMzMsImt2dXR5cGUiOjAsImt2dWxpbWl0IjoiRmFsc2UiLCJrdnVhZG1pbiI6IlRydWUiLCJrdnVhY3QiOiJUcnVlIiwia3Z1bGltaXR0cmFucyI6IkZhbHNlIiwia3Z1c2hvd3N1bSI6IlRydWUiLCJrdmJpZCI6MTE3NDksImt2cmluZGlkIjoxLCJrdnJjb2RlIjoidGVzdGF1dG9teWtpb3QiLCJrdnJpZCI6ODEwMDYwLCJrdnVyaWQiOjgxMDA2MCwia3ZyZ2lkIjoxLCJwZXJtcyI6IiJ9.OQsa_YoINqWz51GHS85SCrajbUxOFGJRIrgFP1JA8IM
    ${dic_credential_kv}    Create Dictionary    live=https://api-man.kiotviet.vn    stagingnew=XXX    prelivenew=XXX
    ${dic_storefronturl}    Create Dictionary    live=https://www.mykiot.vn/testautomykiot    stagingnew=https://fe-staging.citigo.dev:40001/testautomykiot    prelivenew=https://fe-prelive.citigo.dev:40003/testautomykiot
    ${dict_kvurl}    Create Dictionary    live=https://kiotviet.com    stagingnew=https://kvpos.com:59933    prelivenew=https://kvpos.com:59933
    ${dict_username}    Create Dictionary    live=admin    stagingnew=admin    prelivenew=admin
    ${dict_password}    Create Dictionary    live=123    stagingnew=123    prelivenew=123
    ${dic_kv_apiurl}    Create Dictionary    live=https://man.kiotapi.com/api    stagingnew=https://testautomykiot.kvpos.com:59933/api    prelivenew=https://testautomykiot.kvpos.com:59933/api
    ${dic_branchid1}    Create Dictionary    stagingnew=11749    prelivenew=11749    live=9113
    ${dic_branchid2}    Create Dictionary    stagingnew=12041    prelivenew=12041    live=146674
    ${dic_zone}    Create Dictionary    live=26    stagingnew=1    prelivenew=1
    ${dic_salekvapi}    Create Dictionary    live=https://sale.kiotapi.com/api    stagingnew=https://kvpos.com:59903/api    prelivenew=https://kvpos.com:59903/api
    ${dic_google_account}    Create Dictionary    live=testautomation113@gmail.com    stagingnew=testautomation113@gmail.com    prelivenew=testautomation113@gmail.com
    ${dic_google_pass}    Create Dictionary    live=test@123456    stagingnew=test@123456    prelivenew=test@123456
    ${dic_customer_code_google_account}    Create Dictionary    live=KH2B36D    stagingnew=KH123    prelivenew=KH123
    ${dic_surchageid1}    Create Dictionary    live=103181    stagingnew=7461    prelivenew=7461
    ${dic_surchageid2}    Create Dictionary    live=103182    stagingnew=7464    prelivenew=7464
    ${dic_sale_price_id}    Create Dictionary    live=110423    stagingnew=101621    prelivenew=101621
    ${dic_promote_price_id}    Create Dictionary    live=110435    stagingnew=101622    prelivenew=101622
    ${dic_coreapi_url}    Create Dictionary    live= https://api.mykiot.vn/api    stagingnew=https://api-staging.citigo.dev:40001/api    prelivenew=https://api-prelive.citigo.dev:40003/api
    ${dic_apiurl_pb}    Create Dictionary    live=https://cms-api.mykiot.vn/base/api/v1    stagingnew=https://cms-staging.citigo.dev:40001/base/api/v1    prelivenew=https://cms-prelive.citigo.dev:40003/base/api/v1
    ${dic_onboarding_link}    Create Dictionary    live=xxx    stagingnew=https://fe-staging.citigo.dev:40001/admin/onboarding.html?key=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6IjJYViJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxMTU0OTYsImlhdCI6MTYzOTExMDYzNiwiZXhwIjoxNjQxNzAyNjM2LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbIlVzZXIiXSwia3ZzZXMiOiIwNmQwNWZjNTQ1MTg0NmE0ODAzMmQ1ZWIyNDYzZDRlYSIsImt2dWlkIjoxMTU0OTYsImt2dXR5cGUiOjAsImt2dWxpbWl0IjoiRmFsc2UiLCJrdnVhZG1pbiI6IlRydWUiLCJrdnVhY3QiOiJUcnVlIiwia3Z1bGltaXR0cmFucyI6IkZhbHNlIiwia3Z1c2hvd3N1bSI6IlRydWUiLCJrdmJpZCI6MTIwNzEsImt2cmluZGlkIjozLCJrdnJjb2RlIjoiYW5obmswMzAxODkiLCJrdnJpZCI6ODEyMTgwLCJrdnVyaWQiOjgxMjE4MCwia3ZyZ2lkIjoxLCJwZXJtcyI6IiJ9.wRiiUxSnDjbAkrsVEpNTBwma3DK-HzP762xq_9CR1VY&retailer_id=812180&retailer_code=anhnk030189    prelivenew=https://fe-prelive.citigo.dev:40003/admin/onboarding.html?key=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImtpZCI6IjJYViJ9.eyJpc3MiOiJrdnNzand0Iiwic3ViIjoxMTU0OTYsImlhdCI6MTYzOTExMDYzNiwiZXhwIjoxNjQxNzAyNjM2LCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjpbIlVzZXIiXSwia3ZzZXMiOiIwNmQwNWZjNTQ1MTg0NmE0ODAzMmQ1ZWIyNDYzZDRlYSIsImt2dWlkIjoxMTU0OTYsImt2dXR5cGUiOjAsImt2dWxpbWl0IjoiRmFsc2UiLCJrdnVhZG1pbiI6IlRydWUiLCJrdnVhY3QiOiJUcnVlIiwia3Z1bGltaXR0cmFucyI6IkZhbHNlIiwia3Z1c2hvd3N1bSI6IlRydWUiLCJrdmJpZCI6MTIwNzEsImt2cmluZGlkIjozLCJrdnJjb2RlIjoiYW5obmswMzAxODkiLCJrdnJpZCI6ODEyMTgwLCJrdnVyaWQiOjgxMjE4MCwia3ZyZ2lkIjoxLCJwZXJtcyI6IiJ9.wRiiUxSnDjbAkrsVEpNTBwma3DK-HzP762xq_9CR1VY&retailer_id=812180&retailer_code=anhnk030189
    ${dic_onboarding_storefront_url}    Create Dictionary    live=xxx    stagingnew=https://fe-staging.citigo.dev:40001/anhnk030189    prelivenew=https://fe-prelive.citigo.dev:40003/anhnk030189
    ${product_test_id_myk}    Get From Dictionary    ${dic_product_test_id_myk}    ${env}
    ${product_test2_id_myk}    Get From Dictionary    ${dic_product_test2_id_myk}    ${env}
    ${product_test_id_myk}    Get From Dictionary    ${dic_product_test_id_myk}    ${env}
    ${product_test2_id_myk}    Get From Dictionary    ${dic_product_test2_id_myk}    ${env}
    ${dic_menungang_new}    Create Dictionary    live=64500    stagingnew=121433    prelivenew=326
    ${credential_kv}    Get From Dictionary    ${dic_credential_kv}    ${env}
    ${id_horizontal_menu}    Get From Dictionary    ${dic_id_horizontal_menu}    ${env}
    ${id_menungang_new}    Get From Dictionary    ${dic_menungang_new}    ${env}
    ${onboarding_gettoken_user}    Get From Dictionary    ${dic_onboarding_gettoken_user}    ${env}
    ${onboarding_gettoken_password}    Get From Dictionary    ${dic_onboarding_gettoken_password}    ${env}
    ${onboarding_retailer_name}    Get From Dictionary    ${dic_onboarding_retailer_name}    ${env}
    ${onboarding_email}    Get From Dictionary    ${dic_onboarding_email}    ${env}
    ${onboarding_password}    Get From Dictionary    ${dic_onboarding_password}    ${env}
    ${onboarding_retailerID}    Get From Dictionary    ${dic_onboarding_retailerID}    ${env}
    ${public_kvapi}    Get From Dictionary    ${dic_public_kvapi}    ${env}
    ${categorytesturl}    Get From Dictionary    ${dic_category_test_url}    ${env}
    ${cateIDtestKV}    Get From Dictionary    ${dic_category_test_id_kv}    ${env}
    ${productIDtesturl}    Get From Dictionary    ${dic_product_test_url}    ${env}
    ${productIDtestkv}    Get From Dictionary    ${dic_product_test_id_kv}    ${env}
    ${productIDtestKV2}    Get From Dictionary    ${dic_product_test2_id_kv}    ${env}
    ${product_code}    Get From Dictionary    ${dic_product_test_code}    ${env}
    # ${cookie_sync}    Get From Dictionary    ${dic_cookie_sync}    ${env}
    ${jwttoken}    Get From Dictionary    ${dic_jwttoken}    ${env}
    ${auth_page_builder}    Get From Dictionary    ${dic_auth_page_builder}    ${env}
    ${store_id}    Get From Dictionary    ${dic_storeid}    ${env}
    ${api_url}    Get From Dictionary    ${dic_api_url}    ${env}
    ${url}    Get From Dictionary    ${dic_url}    ${env}
    ${client_id}    Get From Dictionary    ${dic_clientid}    ${env}
    ${secret_key}    Get From Dictionary    ${dic_secretkey}    ${env}
    ${retailer_code}    Get From Dictionary    ${dic_retailercode}    ${env}
    ${retailer_id}    Get From Dictionary    ${dic_retailerid}    ${env}
    ${access_url}    Get From Dictionary    ${dic_accessurl}    ${env}
    ${storefront_url}    Get From Dictionary    ${dic_storefronturl}    ${env}
    ${kv_url}    Get From Dictionary    ${dict_kvurl}    ${env}
    ${user_name}    Get From Dictionary    ${dict_username}    ${env}
    ${password}    Get From Dictionary    ${dict_password}    ${env}
    ${kvapi_url}    Get From Dictionary    ${dic_kv_apiurl}    ${env}
    ${branch_id1}    Get From Dictionary    ${dic_branchid1}    ${env}
    ${branch_id2}    Get From Dictionary    ${dic_branchid2}    ${env}
    ${zone}    Get From Dictionary    ${dic_zone}    ${env}
    ${salekvapi}    Get From Dictionary    ${dic_salekvapi}    ${env}
    ${google_account}    Get From Dictionary    ${dic_google_account}    ${env}
    ${google_pass}    Get From Dictionary    ${dic_google_pass}    ${env}
    ${customer_code_google_account}    Get From Dictionary    ${dic_customer_code_google_account}    ${env}
    ${surchage_id1}    Get From Dictionary    ${dic_surchageid1}    ${env}
    ${surchage_id2}    Get From Dictionary    ${dic_surchageid2}    ${env}
    ${sale_price_id}    Get From Dictionary    ${dic_sale_price_id}    ${env}
    ${promote_price_id}    Get From Dictionary    ${dic_promote_price_id}    ${env}
    ${coreapi_url}    Get From Dictionary    ${dic_coreapi_url}    ${env}
    ${apiurl_pb}    Get From Dictionary    ${dic_apiurl_pb}    ${env}
    ${onboarding_link}    Get From Dictionary    ${dic_onboarding_link}    ${env}
    ${onboarding_storefront_url}    Get From Dictionary    ${dic_onboarding_storefront_url}    ${env}
    Set Global Variable    \${credential_kv}    ${credential_kv}
    Set Global Variable    \${product_test_id_myk}    ${product_test_id_myk}
    Set Global Variable    \${product_test2_id_myk}    ${product_test2_id_myk}
    Set Global Variable    \${id_menungang_new}    ${id_menungang_new}
    Set Global Variable    \${id_horizontal_menu}    ${id_horizontal_menu}
    Set Global Variable    \${onboarding_gettoken_user}    ${onboarding_gettoken_user}
    Set Global Variable    \${onboarding_gettoken_password}    ${onboarding_gettoken_password}
    Set Global Variable    \${onboarding_retailer_name}    ${onboarding_retailer_name}
    Set Global Variable    \${onboarding_email}    ${onboarding_email}
    Set Global Variable    \${onboarding_password}    ${onboarding_password}
    Set Global Variable    \${onboarding_retailerID}    ${onboarding_retailerID}
    Set Global Variable    \${public_kvapi}    ${public_kvapi}
    Set Global Variable    \${cateIDtestKV}    ${cateIDtestKV}
    Set Global Variable    \${categorytesturl}    ${categorytesturl}
    Set Global Variable    \${productIDtestkv}    ${productIDtestkv}
    Set Global Variable    \${productIDtestKV2}    ${productIDtestKV2}
    Set Global Variable    \${productIDtesturl}    ${productIDtesturl}
    Set Global Variable    \${product_code}    ${product_code}
    # Set Global Variable    \${cookie_sync}    ${cookie_sync}
    Set Global Variable    \${jwttoken}    ${jwttoken}
    Set Global Variable    \${auth_page_builder}    ${auth_page_builder}
    Set Global Variable    \${api_url}    ${api_url}
    Set Global Variable    \${url}    ${url}
    Set Global Variable    \${client_id}    ${client_id}
    Set Global Variable    \${secret_key}    ${secret_key}
    Set Global Variable    \${retailer_code}    ${retailer_code}
    Set Global Variable    \${retailer_id}    ${retailer_id}
    Set Global Variable    \${access_url}    ${access_url}
    Set Global Variable    \${storefront_url}    ${storefront_url}
    Set Global Variable    \${kv_url}    ${kv_url}
    Set Global Variable    \${user_name}    ${user_name}
    Set Global Variable    \${password}    ${password}
    Set Global Variable    \${kvapi_url}    ${kvapi_url}
    Set Global Variable    \${branch_id1}    ${branch_id1}
    Set Global Variable    \${branch_id2}    ${branch_id2}
    Set Global Variable    \${zone}    ${zone}
    Set Global Variable    \${salekvapi}    ${salekvapi}
    Set Global Variable    \${google_account}    ${google_account}
    Set Global Variable    \${google_pass}    ${google_pass}
    Set Global Variable    \${customer_code_google_account}    ${customer_code_google_account}
    Set Global Variable    \${store_id}    ${store_id}
    Set Global Variable    \${surchage_id1}    ${surchage_id1}
    Set Global Variable    \${surchage_id2}    ${surchage_id2}
    Set Global Variable    \${env}    ${env}
    Set Global Variable    \${sale_price_id}    ${sale_price_id}
    Set Global Variable    \${promote_price_id}    ${promote_price_id}
    Set Global Variable    \${coreapi_url}    ${coreapi_url}
    Set Global Variable    \${apiurl_pb}    ${apiurl_pb}
    Set Global Variable    \${onboarding_link}    ${onboarding_link}
    Set Global Variable    \${onboarding_storefront_url}    ${onboarding_storefront_url}

init test env
    [Arguments]    ${env}
    fill env    ${env}
    # ${token_value}    get access token from api
    # Set global variable    \${bearer_token_mykiot}    ${token_value}
    # Append To Environment Variable    PATH    ${EXECDIR}${/}Drivers
    Set Screenshot Directory    ${EXECDIR}${/}Out${/}Failures
    Set Selenium Speed    0.6s

init test env sync
    [Arguments]    ${env}
    fill env    ${env}
    IF    '${env}' == 'live'
    ${token_value_public_api}    Get BearerToken from Public API
    Set Global Variable    \${bearertoken}    ${token_value_public_api}
    # Log    ${bearertoken}
    # Set global variable    \${resp.cookies}    ${resp.cookies}
    ELSE
    ${token_value}    Get BearerToken from api
    Set global variable    \${bearertoken}    ${token_value}
    END
    # Log    ${bearertoken}
    # Set global variable    \${resp.cookies}    ${resp.cookies}
    # Append To Environment Variable    PATH    ${EXECDIR}${/}Drivers
    # Set Screenshot Directory    ${EXECDIR}${/}Out${/}Failures
    Set Selenium Speed    0.6s

init test core api
    [Arguments]    ${env}
    fill env    ${env}
    ${token_value}    Login customer get token from api    ${retailer_id}
    Set global variable    \${mykiot_token}    ${token_value}
    Append To Environment Variable    PATH    ${EXECDIR}${/}Drivers
    Set Screenshot Directory    ${EXECDIR}${/}Out${/}Failures
    Set Selenium Speed    0.1s

init test storefront
    [Arguments]    ${env}
    fill env    ${env}
    ${token_value}    ${resp.cookies}    Get BearerToken from api
    Set global variable    \${bearertoken}    ${token_value}
    Set global variable    \${resp.cookies}    ${resp.cookies}
    ${token_value1}    Login customer get token from api    ${retailer_id}
    Set global variable    \${mykiot_token}    ${token_value1}
    Append To Environment Variable    PATH    ${EXECDIR}${/}Drivers
    Set Screenshot Directory    ${EXECDIR}${/}Out${/}Failures
    set selenium implicit wait    30s
    Set Selenium Speed    0.3s
    log    ${bearertoken}
    log    ${mykiot_token}
